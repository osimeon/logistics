<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Pickup extends CI_Controller{
    function Pickup(){
		parent::__construct();
	 	$this->load->database();
	 	$this->load->model("pickup_model"); 	
        $this->load->model("customer_model");	  
     	$this->load->library('form_validation');
         
         /*cache control*/
		$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
        $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");

        check_login_customer();
    }

    function index(){
        $data['pickups'] = $this->pickup_model->location_list(userdata_customer('id'));
        $data['contacts'] = $this->customer_model->get_location_contacts(userdata_customer('id'));
        $this->load->view('header');
        $this->load->view('pickup/pickup_list', $data);
        $this->load->view('footer');
    }

    function get_pickups_ajax(){
        $items = $this->pickup_model->location_list(userdata_customer('id'));
        
        $response = array();
        $response["items"] = array();

        foreach($items as $item){
            $status = ($item->status == 1) ? 'checked' : '';
            $edit = site_url('customer/pickup/update/').'/'.$item->loc_id;

            $con = $this->pickup_model->get_loc_contact($item->contact_person);

            $tmp = array(
                'name' => $item->name,
                'opening' => $item->opening,
                'closing' => $item->closing,
                'contacts' => $con->name,
                'address' => $item->address,
                'options' => "<a href=\"$edit\" class=\"edit btn btn-sm btn-default dlt_sm_table\"><i class=\"icon-note\"></i></a><a class=\"delete btn btn-sm btn-danger dlt_sm_table\"><i class=\"glyphicon glyphicon-trash\" onclick=\"delete_loc($item->loc_id)\"></i></a>"
            );

            array_push($response["items"], $tmp);
        }

        echo json_encode($response);
    }

    function add(){ 
        $this->load->view('header');
        $data['contacts'] = $this->customer_model->get_location_contacts(userdata_customer('id'));
        $this->load->view('pickup/add_pickup', $data);
        $this->load->view('footer');
    }

    function add_process(){
        if($this->form_validation->run('add_pickup') == FALSE){
            echo '<div class="alert error"><ul>' . validation_errors('<li style="color:red">','</li>') . '</ul></div>';
        }
        else{
            if($this->pickup_model->add_loc()){
                echo '<div class="alert alert-success">Location was added succesfully</div>';
            }
            else{
                echo $this->lang->line('technical_problem');
            }
        }
    }

    function add_process_ajax(){
        if($this->form_validation->run('add_pickup') == FALSE){
            echo '<div class="alert error"><ul>' . validation_errors('<li style="color:red">','</li>') . '</ul></div>';
        }
        else{
            if($this->pickup_model->add_loc()){
                $pickup_id = $this->db->insert_id();
                $data['pickup'] = $this->pickup_model->get_loc($pickup_id);

                $details = array();
                $details['pi_id'] = $pickup_id;
                $details['pi_name'] = $data['pickup']->name; 
                
                echo json_encode($details);
            }
            else{
                echo $this->lang->line('technical_problem');
            }
        }
    }

    function add_delivery_ajax(){
        if($this->form_validation->run('add_pickup') == FALSE){
            echo '<div class="alert error"><ul>' . validation_errors('<li style="color:red">','</li>') . '</ul></div>';
        }
        else{
            if($this->pickup_model->add_loc_del()){
                $pickup_id = $this->db->insert_id();
                $data['pickup'] = $this->pickup_model->get_loc($pickup_id);

                $details = array();
                $details['pi_id'] = $pickup_id;
                $details['pi_name'] = $data['pickup']->name; 
                
                echo json_encode($details);
            }
            else{
                echo $this->lang->line('technical_problem');
            }
        }
    }

    /*
     * Displays member update form
     */
    function update($loc_id){
        $data['location'] = $this->pickup_model->get_loc($loc_id);  
        $data['contacts'] = $this->customer_model->get_location_contacts(userdata_customer('id'));
        $this->load->view('header');
        $this->load->view('pickup/update_pickup',$data);
        $this->load->view('footer');
    }

    function update_process(){
        if($this->form_validation->run('add_pickup') == FALSE){
            echo '<div class="alert error"><ul>' . validation_errors('<li>','</li>') . '</ul></div>';
        }
        else{
            if($this->pickup_model->update_staff()){
                echo '<div class="alert alert-success">update_succesful</div>';
            }
            else{
                echo 'technical_problem';
            }
        }
    }

    function delete($loc_id){
        if($this->pickup_model->delete($loc_id)){
            echo 'deleted';
        }   
    }
}
?>