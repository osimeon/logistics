<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Package extends CI_Controller{
	function Package(){
		parent::__construct();
		$this->load->database();

		$this->load->model("packaging_model"); 		  
     	$this->load->library('form_validation');
         
         /*cache control*/
		$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
        $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");

        check_login_customer();
	}

	function index(){
		$data['packages'] = $this->packaging_model->getCustomerPackaging(userdata_customer('id'));
		$this->load->view('header');
		$this->load->view('package/package_list', $data);
		$this->load->view('footer');
	}

	function get_packages_ajax(){
		$items = $this->packaging_model->getCustomerPackaging(userdata_customer('id'));
        
        $response = array();
        $response["items"] = array();

        foreach($items->result() as $item){
            $status = ($item->status == 1) ? 'checked' : '';
            $edit = site_url('customer/package/update').'/'.$item->id;

            $tmp = array(
                'type' => $item->type,
                'description' => $item->description,
                'options' => "<a href=\"$edit\" class=\"edit btn btn-sm btn-default dlt_sm_table\"><i class=\"icon-note\"></i></a><a class=\"delete btn btn-sm btn-danger dlt_sm_table\"><i class=\"glyphicon glyphicon-trash\" onclick=\"delete_package($item->id)\"></i></a>"
            );

            array_push($response["items"], $tmp);
        }

        echo json_encode($response);
	}

	function add(){
		$this->load->view('header');
		$this->load->view('package/add', $data);
		$this->load->view('footer');
	}

	function add_process(){
		if($this->form_validation->run('add_package') == FALSE){
        	echo '<div class="alert error"><ul>' . validation_errors('<li style="color:red">','</li>') . '</ul></div>';
        }
        else{
        	if($this->packaging_model->addPackage()){
        		echo '<div class="alert alert-success">'.$this->lang->line('create_succesful').'</div>';
            }
            else{
                echo $this->lang->line('technical_problem');
            }
        }
	}

	function add_process_ajax(){
		if($this->form_validation->run('add_package') == FALSE){
        	echo '<div class="alert error"><ul>' . validation_errors('<li style="color:red">','</li>') . '</ul></div>';
        }
        else{
        	if($this->packaging_model->addPackage()){
        		$package_id = $this->db->insert_id();
            	$data['package'] = $this->packaging_model->get_package($package_id);

            	$details = array();
				$details['pa_id'] = $package_id;
				$details['pa_name'] = $data['package']->type; 
				
				echo json_encode($details);
        	}
        	else{
                echo $this->lang->line('technical_problem');
            }
        }
	}

	function delete($package){
		if($this->packaging_model->deletePackage($package)){
			echo 'deleted';
		}
	}

	function update($package_id){
		$data['package'] = $this->packaging_model->get_package($package_id);
		$this->load->view('header');
		$this->load->view('package/update', $data);
		$this->load->view('footer');
	}

	function update_process(){
		if($this->form_validation->run('add_package') == FALSE){
            echo '<div class="alert error"><ul>' . validation_errors('<li>','</li>') . '</ul></div>';
        }
        else{
            if($this->packaging_model->updatePackage()){
                echo '<div class="alert alert-success">update_succesful</div>';
            }
            else{
                echo 'technical_problem';
            }
        }
	}
}
?>