<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Customer extends CI_Controller {
    function Customer(){
		parent::__construct();
	 	$this->load->database();
	 	$this->load->model("customer_model"); 
     	$this->load->library('form_validation');
    }
     
	function index(){ 
		redirect(site_url('customer/login'),'refresh');
	}

    function new_contact(){ 
        $this->load->view('header');
        $this->load->view('pickup/add_contact');
        $this->load->view('footer');
    }

    function contacts(){
        $data['contacts'] = $this->customer_model->get_location_contacts(userdata_customer('id'));
        $this->load->view('header');
        $this->load->view('pickup/contacts', $data);
        $this->load->view('footer');
    }

    function get_contacts_ajax(){
        $items = $this->customer_model->get_location_contacts(userdata_customer('id'));
        
        $response = array();
        $response["items"] = array();

        foreach($items->result() as $item){
            $edit = site_url('customer/customer/update_contact/').'/'.$item->id;

            $tmp = array(
                'name' => $item->name,
                'email' => $item->email,
                'position' => $item->position,
                'options' => "<a href=\"$edit\" class=\"edit btn btn-sm btn-default dlt_sm_table\"><i class=\"icon-note\"></i></a><a class=\"delete btn btn-sm btn-danger dlt_sm_table\"><i class=\"glyphicon glyphicon-trash\" onclick=\"delete_loc($item->id)\"></i></a>"
            );

            array_push($response["items"], $tmp);
        }

        echo json_encode($response);
    }
	 
	/*
  	 * Displays form for new member
     */
    function create(){
		if(is_login_customer() == 1){
			redirect(site_url('customer/dashboard'),'refresh'); 
		}
       
        $this->load->view('user/new_company');
    }

    function update_contact($contact){
        $data['contact'] = $this->customer_model->get_contact($contact);       
        $this->load->view('header');
        $this->load->view('pickup/edit_contact',$data);
        $this->load->view('footer');
    }

    function activate($code){
    	$status = $this->customer_model->verify_code($code);

    	if($status){
    		redirect(site_url('customer/login'),'refresh');
    	}
    	else{
    		redirect(site_url('customer/create'),'refresh');
    	}
    }

    function deletecontact($contact){
        if($this->customer_model->deletecontact($contact)){
            echo 'deleted';
        } 
    }

    function add_contact_process(){
        if($this->form_validation->run('create_contact_person') == FALSE){
            echo '<div class="alert error"><ul>' . validation_errors('<li>','</li>') . '</ul></div>';
        }
        elseif( $this->customer_model->exists_contact_email( $this->input->post('contact_email') ) > 0){
            echo '<div class="alert error">'.$this->lang->line('already_account').'</div>';
        }
        else{
            if($this->customer_model->create_contact()){
                echo '<div class="alert error">contact added succesfully</div>';
            }
            else{
                echo '<div class="alert error">' .$this->lang->line('technical_problem'). '</div>';
            }
        }
    }

    function update_contact_process(){
        if($this->customer_model->update_contact()){
            echo '<div class="alert alert-success">update_succesful</div>';
        }
        else{
            echo 'technical_problem';
        }
    }

    function create_company(){
    	$email = $this->input->post('company_email');
    	$password = $this->input->post('pass1');
    	$company = $this->input->post('company_name');

    	if($this->form_validation->run('create_customers') == FALSE){
            echo '<div class="alert error"><ul>' . validation_errors('<li>','</li>') . '</ul></div>';
        }
        elseif( $this->customer_model->exists_company_email( $this->input->post('company_email') ) > 0){
            echo '<div class="alert error">'.$this->lang->line('already_account').'</div>';
        }
        else{
        	if( $this->customer_model->create_company()){
                //Send email
                $email_body = '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">';
                $email_body .= '<title>Logistics & Energy Africa</title><style type="text/css">';
                $email_body .= 'a{color: #4A72AF;}body, #header h1, #header h2, p {margin: 0;padding: 0;}';
                $email_body .= '#main {border: 1px solid #cfcece;}img {display: block;}';
                $email_body .= '#top-message p, #bottom-message p {color: #3f4042;font-size: 12px;font-family: Arial, Helvetica, sans-serif;}';
                $email_body .= '#header h1{color: #ffffff !important;font-family: "Lucida Grande", "Lucida Sans", "Lucida Sans Unicode", sans-serif;font-size: 24px; margin-bottom: 0!important; padding-bottom: 0;}';
                $email_body .= '#header h2 {color: #ffffff !important;font-family: Arial, Helvetica, sans-serif;font-size: 24px; margin-bottom: 0 !important; padding-bottom: 0;}';
                $email_body .= '#header p {color: #ffffff !important;font-family: "Lucida Grande", "Lucida Sans", "Lucida Sans Unicode", sans-serif;font-size: 12px;}';
                $email_body .= 'h1, h2, h3, h4, h5, h6 {margin: 0 0 0.8em 0;}h3{font-size: 28px;color: #444444 !important;font-family: Arial, Helvetica, sans-serif;}h4 {font-size: 22px;color: #4A72AF !important;font-family: Arial, Helvetica, sans-serif;}';
                $email_body .= 'h5{font-size: 18px;color: #444444 !important;font-family: Arial, Helvetica, sans-serif;}';
                $email_body .= 'p{font-size: 12px;color: #444444 !important;font-family: "Lucida Grande", "Lucida Sans", "Lucida Sans Unicode", sans-serif; line-height: 1.5;}';
                $email_body .= '</style></head><body><table width="100%" cellpadding="0" cellspacing="0" bgcolor="e4e4e4"><tr>
                                <td><table id="top-message" cellpadding="20" cellspacing="0" width="600" align="center"><tr>
                                <td align="center"><p style="display:none;">Trouble viewing this email? <a href="#">View in Browser</a></p>
                                </td></tr></table><!-- top message -->
                                <table id="main" width="600" align="center" cellpadding="0" cellspacing="15" bgcolor="ffffff">
                                <tr><td><table id="header" cellpadding="10" cellspacing="0" align="center" bgcolor="8fb3e9">
                                <tr><td width="570" bgcolor="7aa7e9"><h1>Logistics &amp; Energy Africa</h1></td>
                                </tr></table><!-- header -->
                                </td></tr><!-- header -->
                                <tr><td></td></tr><tr><td><table id="content-1" cellpadding="0" cellspacing="0" align="center">
                                <tr><td width="375" valign="top" colspan="3"><h3>Welcome to Logistics &amp; Energy Africa</h3>
                                <h4>Going Beyond | Going Places</h4></td></tr>
                                </table><!-- content 1 -->
                                </td></tr><!-- content 1 -->
                                <tr><td><table id="content-2" cellpadding="0" cellspacing="0" align="center"><tr>
                                <td width="570" align="center"><p>';
                $email_body .= 'Hello '.$company.', <br/><br/>
                                Your account at Logistics & Energy Africa Ltd is ready,<br/>
                                Below are your account login details: 
                                </p><br/><p>
                                <b>Email: </b>'.$email.'<br/>
                                <b>Password: </b>'.$password.'<br/><br/>
                                <b>Please <a href="'.site_url('customer/activate').'/'.md5($password).'">click here</a> for activation </b></p>';
                $email_body .= '</td></tr></table><!-- content-2 --></td></tr><!-- content-2 --><tr>
                                <td align="center"><table id="content-6" cellpadding="0" cellspacing="0" align="center">
                                <p align="center">You may copy paste this link in your browser:</p><p align="center"><a href="http://logistics.mymarket.co.ke/index.php/customer">http://logistics.mymarket.co.ke/index.php/customer</a></p>
                                </table></td></tr></table><!-- main -->
                                <table id="bottom-message" cellpadding="20" cellspacing="0" width="600" align="center"><tr>
                                <td align="center"><p>You are receiving this email because you signed up for Logistics and Energy Africa Ltd customer portal</p>
                                </td></tr></table><!-- top message --></td>
                                </tr></table><!-- wrapper --></body></html>';
                
                $subject = 'Customer login details';
                //$message = 'Hello '.$company.',  <br><br>Your account at Logistics & Energy Africa Ltd is ready, <br>Below are the details that you used to when creating the account: <br><b>Email:</b> <br><br>';
                //$message .= $email;
                //$message .= '. <br> <b>Password:</b> ';
                //$message .= $password;
                //$message .= ' <br>Please <a href="';
                //$message .= site_url('customer/activate').'/'.md5($password);
                //$message .= '">click here</a> for activation';
            	send_notice($this->input->post('company_email'), $subject, $email_body);

            	echo '<div class="alert error">' .$this->lang->line('register_succes_msg1'). '</div>';
            }
            else{
                echo '<div class="alert error">' .$this->lang->line('technical_problem'). '</div>';
            }
        }
    }

    function test_email(){
    	$subject = 'Customer login details, Test func';
        $message = 'Hello,  <br><br><b>Email:</b> simeon.obwogo79@gmail.com <br> <b>Password:</b> obsiha2013 <br>Please <a href="'.site_url('customer/login').'">click here</a> for login';
    	send_notice('simeon.obwogo79@gmail.com', $subject, $message);
    }

    /*
     * Makes controls for new member.
     */
    function create_process(){
        
        if($this->form_validation->run('create_customers') == FALSE){
            echo '<div class="alert error"><ul>' . validation_errors('<li>','</li>') . '</ul></div>';
        }
        elseif( $this->customer_model->exists_email( $this->input->post('email') ) > 0){
            echo '<div class="alert error">'.$this->lang->line('already_account').'</div>';
        }
        else{
            if( $this->customer_model->create_customers()){
                echo '<div class="alert error">' .$this->lang->line('register_succes_msg1'). '</div>';
            }
            else{
                echo '<div class="alert error">' .$this->lang->line('technical_problem'). '</div>';
            }
        }
    }
	
	/*
     * Displays the login form
     */
    function login(){
		if(is_login_customer() == 1 ){ 
			redirect(site_url('customer/dashboard'),'refresh'); 
		}
        
        $this->load->view('user/login_company');
    }
    
    function login_process(){
        if($this->form_validation->run('login_customer') == FALSE){
			echo '<div class="alert error"><ul>' . validation_errors('<li>','</li>') . '</ul></div>';
        }
        elseif($this->customer_model->exists_email( $this->input->post('email') ) == 0){
            echo '<div class="alert error">'.$this->lang->line('you_must_create_account').'</div>';
        }
        elseif($this->customer_model->check_user_detail() == FALSE){
            echo '<div class="alert error">'.$this->lang->line('invalid_email_or_pass').'</div>';
        }
        else{
            $userdata = $this->customer_model->user_data( $this->input->post('email') );
            $session_data = array("customername"   => $userdata->email, "customerhash"   => md5( $userdata->password.$this->config->item('password_hash')));
            $this->session->set_userdata($session_data);
            echo '<script>location.href="'.site_url('customer/dashboard').'";</script>';
        }
    }

    function login_process_company(){
        if($this->form_validation->run('login_customer') == FALSE){
            echo '<div class="alert error"><ul>' . validation_errors('<li>','</li>') . '</ul></div>';
        }
        elseif($this->customer_model->exists_company_email( $this->input->post('email') ) == 0){
            echo '<div class="alert error">'.$this->lang->line('you_must_create_account').'</div>';
        }
        elseif($this->customer_model->check_company_detail() == FALSE){
            echo '<div class="alert error">'.$this->lang->line('invalid_email_or_pass').'</div>';
        }
        else{
            $userdata = $this->customer_model->company_data( $this->input->post('email') );
            $session_data = array("customername"   => $userdata->email, "customerhash"   => md5( $userdata->password.$this->config->item('password_hash')));
            $this->session->set_userdata($session_data);
            echo '<script>location.href="'.site_url('customer/dashboard').'";</script>';
        }
    }
 
	/*
     * Logout function
     */
    function logout(){
        $session_items = array('customername' => '', 'customerhash'     => '', 'logged_in'=> FALSE);
		$this->session->unset_userdata($session_items);
		redirect(site_url('customer/login'),"refresh");
    }
    
    /*
     * Sends request for password reset
     */
	function lostpassword_process(){
		if($this->form_validation->run('customer_lostpassword') == FALSE){
			echo '<div class="alert error">'.validation_errors().'</div>';
		}
		elseif( $this->customer_model->exists_email( $this->input->post('email') ) == 0 ){
			echo '<div class="alert error">'.$this->lang->line('you_must_create_account').'</div>';
		}
		else{
			
			$lostpw_code = $this->customer_model->create_lostpw_code();
			
			$subject = 'Reset password request';
            $message = 'Hello, <br> To reset your password please follow the link below: <br> <a href="'.site_url('customer/check_code/'.$this->input->post('email').'/'.$lostpw_code).'">'.site_url('customer/check_code/'.$this->input->post('email').'/'.$lostpw_code).'</a>';
            	
			send_notice($this->input->post('email'),$subject,$message);
			  
			echo '<script>location.href="'.site_url('customer/lostpw_succes').'";</script>';
		}
	}
	
	function lostpw_succes(){
    	$this->load->view('user/lostpw_succes');
    }
	
	/*
     * Checks the reset password code
     */
	function check_code($email, $code){
		$status = $this->customer_model->check_code( $email,$code );
		
		if($status == 0){
			$this->session->set_flashdata('message','<div class="alert error">'.$this->lang->line('invalid_reset_code').'</div>');
			redirect(site_url('customer/login'),'refresh');
		}
		else{
			$new_password = $this->customer_model->create_new_password( $email );
			$subject = 'New Password';
            $message = 'Hello, <br><br> New password is <b>'.$new_password.'</b>. Please <a href="'.site_url('customer/login').'">click here</a> for login';
			send_notice($email,$subject,$message);
			$this->session->set_flashdata('message','<div class="alert ok">'.$this->lang->line('new_pass_sent').'</div>');
			redirect(site_url('customer/login'),'refresh');
		}
	}
	
	/*
     * Displays the settings
     */
    function settings(){
    	check_login_customer();
    	$data['customer'] = $this->customer_model->user_data( userdata_customer('email') );
			
		$this->load->view('header');
		$this->load->view('user/user_settings',$data);
		$this->load->view('footer');
	}
	
	function change_profile(){
		check_login_customer();
		
		if($this->form_validation->run('customer_change_profile') == FALSE){
			echo '<div class="alert error"><ul>'.validation_errors('<li>','</li>').'</ul></div>';
		}
		else{
			$session_data = array("customername"   => $this->input->post('email'), "customerhash"   => md5( userdata_customer('password').$this->config->item('password_hash')));
										  
			if($this->customer_model->change_profile()){
				$this->session->set_userdata($session_data);
				echo '<div class="alert ok">'.$this->lang->line('update_succesful').'</div>';
			}
			else{
				echo '<div class="alert error">'.$this->lang->line('technical_problem').'</div>';
			}
		}
	}
	
	function change_password(){
		check_login_customer();
		
		if($this->form_validation->run('customer_change_password') == FALSE){
			echo '<div class="alert error"><ul>'.validation_errors('<li>','</li>').'</ul></div>';
		}
		elseif($this->customer_model->check_password() == 0){
			echo '<div class="alert error">'.$this->lang->line('invalid_pass').'</div>';
		}
		else{
			if($this->customer_model->password_update()){
				$session_data = array("customername"   => userdata_customer('email'), "customerhash" => md5(userdata_customer('password').$this->config->item('password_hash')));
				$this->session->set_userdata($session_data);
				echo '<div class="alert ok">'.$this->lang->line('update_succesful').'</div>';
			}
			else{
				echo '<div class="alert error">'.$this->lang->line('technical_problem').'</div>';
			}
		}
	}
}