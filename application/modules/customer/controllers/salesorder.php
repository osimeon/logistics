<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Salesorder extends CI_Controller {
    function Salesorder(){
     	parent::__construct();
	 	$this->load->database();		 
	 	$this->load->model('staff_model');
	 	$this->load->model('pickup_model');
	 	$this->load->model('site_model');
        $this->load->model('salesorder_model');
        $this->load->model("packaging_model"); 
        $this->load->model("customer_model"); 
	 	$this->load->library('form_validation');
        $this->load->library('zend');
        $this->zend->load('Zend/Barcode');
        $this->load->helper('pdf_helper'); 

        $this->load->model("driver_model");
        $this->load->model("resources_model");
        $this->load->model("security_model");
        $this->load->model("quotations_model");

        check_login_customer();
    }

    // get order items
    function get_airwaybill_details_ajax($airwaybill){
        $items = $this->salesorder_model->get_airway_items($airwaybill);

        $response = array();
        $response["items"] = array();

        $stop = 1;

        foreach($items->result() as $item){
            $tmp = array(
                'stop' => 'Stop '.$stop,
                'contact' => $this->salesorder_model->getcontactnames($item->contact_person),
                'pickup' => $this->salesorder_model->getlocations($item->pickup_loc).' - '.$item->pickup_date.' '.$item->pickup_time,
                'delivery' => $this->salesorder_model->getlocations($item->del_loc).' - '.$item->del_date.' '.$item->del_time,
                'quantity' => $item->item_quantity.' '.$item->unit,
                'package' => $this->salesorder_model->packagenames($item->package_type),
                'options' => '<a href="javascript:void(0)" class="delete btn btn-sm btn-danger dlt_sm_table" data-toggle="modal" data-target="#modal-basic4" onclick="delete_airway_item('.$item->item_id.')"><i class="glyphicon glyphicon-trash"></i></a>'
            );

            array_push($response["items"], $tmp);

            $stop++;
        }

        echo json_encode($response);
    }

    function accept_order_quote($order){
        $transporter_mail = '<html><head> <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> <title>Logistics and Energy Africa</title> <style type="text/css"> a{color: #4A72AF;}body, #header h1, #header h2, p{margin: 0; padding: 0;}#main{border: 1px solid #cfcece;}img{display: block;}#top-message p, #bottom-message p{color: #3f4042; font-size: 12px; font-family: Arial, Helvetica, sans-serif;}#header h1{color: #ffffff !important; font-family: "Lucida Grande", "Lucida Sans", "Lucida Sans Unicode", sans-serif; font-size: 24px; margin-bottom: 0!important; padding-bottom: 0;}#header h2{color: #ffffff !important; font-family: Arial, Helvetica, sans-serif; font-size: 24px; margin-bottom: 0 !important; padding-bottom: 0;}#header p{color: #ffffff !important; font-family: "Lucida Grande", "Lucida Sans", "Lucida Sans Unicode", sans-serif; font-size: 12px;}h1, h2, h3, h4, h5, h6{margin: 0 0 0.8em 0;}h3{font-size: 28px; color: #444444 !important; font-family: Arial, Helvetica, sans-serif;}h4{font-size: 22px; color: #4A72AF !important; font-family: Arial, Helvetica, sans-serif;}h5{font-size: 18px; color: #444444 !important; font-family: Arial, Helvetica, sans-serif;}p{font-size: 12px; color: #444444 !important; font-family: "Lucida Grande", "Lucida Sans", "Lucida Sans Unicode", sans-serif; line-height: 1.5;}.call_to_action{text-decoration: none;display: block; height: 50px; width: 300px; background: #34696f; border: 2px solid rgba(33, 68, 72, 0.59); color: rgb(255, 255, 255); text-align: center;font: bold 2.2em/50px "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;background: -webkit-linear-gradient(top, #34696f, #2f5f63);background: -moz-linear-gradient(top, #34696f, #2f5f63);background: -o-linear-gradient(top, #34696f, #2f5f63);background: -ms-linear-gradient(top, #34696f, #2f5f63);background: linear-gradient(top, #34696f, #2f5f63);-webkit-border-radius: 50px;-khtml-border-radius: 50px;-moz-border-radius: 50px;border-radius: 50px;-webkit-box-shadow: 0 8px 0 #1b383b;-moz-box-shadow: 0 4px 0 #1b383b;box-shadow: 0 4px 0 #1b383b;text-shadow: 0 2px 2px rgba(255, 255, 255, 0.2);}</style></head><body><table width="100%" cellpadding="0" cellspacing="0" bgcolor="e4e4e4"><tr><td><table id="top-message" cellpadding="20" cellspacing="0" width="600" align="center"><tr><td align="center"><p style="display:none;">Trouble viewing this email? <a href="#">View in Browser</a></p></td></tr></table><table id="main" width="600" align="center" cellpadding="0" cellspacing="15" bgcolor="ffffff"><tr><td><table id="header" cellpadding="10" cellspacing="0" align="center" bgcolor="8fb3e9"><tr><td width="570" bgcolor="#013f45"><h1>Logistics &amp; Africa Energy</h1></td></tr></table></td></tr><tr><td></td></tr><tr><td><table id="content-1" cellpadding="0" cellspacing="0" align="center"><tr><td width="375" valign="top" colspan="3"><center><img src="http://logistics.devshop.co.ke/uploads/site/logo.png"/></center></td></tr></table></td></tr><tr><td><table id="content-2" cellpadding="0" cellspacing="0" align="center"><tr><td width="570" align="center"><p>Hello __TRANSPORTER__, <br/><br/>A quotation you sent to __CUSTOMER__, regarding order __ORDER_NO__,<br/>placed on the __DATE__ has been accepted. <br/><br/>Please login to your account to see the order details.</p><br/></td></tr></table></td></tr><br/><!--<tr><td align="center"><table id="content-6" cellpadding="0" cellspacing="0" align="center"><p align="center">You may copy paste this link in your browser:</p><br/><p align="center"><a href="R_ORDER_URL" class="call_to_action">R_ORDER_URL</a></p></table></td></tr>--></table><table id="bottom-message" cellpadding="20" cellspacing="0" width="600" align="center"><tr><td align="center"><p>You are receiving this email because you signed up for Logistics and Energy Africa Ltd customer portal</p></td></tr></table></td></tr></table></body></html>';

        $order_details = $this->salesorder_model->getorderdetails($order);
        $name = $this->salesorder_model->getcompanyname($order_details->customer_customer_id);
        $orderno = $order_details->airwaybill_no;
        $date = date('m/d/Y H:i', $order_details->order_date);
        $email = $this->salesorder_model->getcompanyemail($order_details->customer_customer_id);
        $order_url = site_url('customer/salesorder/need_action').'/'.$order;
        $subject = 'Quotation for order: '.$orderno.' accepted by customer';

        if($this->salesorder_model->acceptquote($order)){
            $transporter_mail = str_replace('__CUSTOMER__', $name, $transporter_mail);
            $transporter_mail = str_replace('__ORDER_NO__', $orderno, $transporter_mail);
            $transporter_mail = str_replace('__DATE__', $date, $transporter_mail);
            $transporter_mail = str_replace('__TRANSPORTER__', config('site_name'), $transporter_mail);

            // get transporters
            $transporters = $this->salesorder_model->gettransporters();

            foreach($transporters->result() as $transporter){
                send_notice($transporter->email, $subject, $transporter_mail);
            }

            echo 'sent';
        }
        else{
            echo 'not sent';
        }
    }

    // getting resource details for ajax call
    function get_resource_details_ajax($order){
        $quotation = $this->quotations_model->getquote($order);

        $driver = $quotation->driver;
        $vehicle = $quotation->vehicle;
        $security = $quotation->security;

        // get driver details
        $driver_detail = $this->driver_model->getdetails($driver);

        // get vehicle details
        $vehicle_detail = $this->resources_model->getvehicle($vehicle);

        // get security details
        $security_detail = $this->security_model->getdetails($security);

        if(!$driver_detail && !$vehicle_detail && !$security_detail){
            $response['resources'] = array();
            die(json_encode($response));
        }

        // response output
        $response['resources'] = array();
        $driver_avatar = base_url('uploads').'/'.$driver_detail->avatar;

        $driver_arr = array(
            'name' => $driver_detail->lname.' '.$driver_detail->fname,
            'desc' => $driver_detail->job,
            'qua' => 1,
            'avatar' => '<img src="'.$driver_avatar.'" alt="user image" style="height: 100px;width: 100px;">',
            'options' => '<a href="javascript:void(0)" class="delete btn btn-sm btn-danger dlt_sm_table" data-toggle="modal" data-target="#modal-basic4" onclick="delete_driver_resource('.$quotation->id.')"><i class="glyphicon glyphicon-trash"></i></a>'
        );

        $vehicle_avatar = base_url('uploads/resources').'/'.$vehicle_detail->avatar;
        $vehicle_arr = array(
            'name' => $vehicle_detail->name,
            'desc' => $vehicle_detail->description,
            'qua' => 1,
            'avatar' => '<img src="'.$vehicle_avatar.'" alt="user image" style="height: 100px;width: 100px;">',
            'options' => '<a href="javascript:void(0)" class="delete btn btn-sm btn-danger dlt_sm_table" data-toggle="modal" data-target="#modal-basic4" onclick="delete_vehicle_resource('.$quotation->id.')"><i class="glyphicon glyphicon-trash"></i></a>'
        );

        $security_avatar = base_url('uploads').'/'.$security_detail->avatar;
        $security_arr = array(
            'name' => $security_detail->lname.' '.$security_detail->fname,
            'desc' => $security_detail->job,
            'qua' => 1,
            'avatar' => '<img src="'.$security_avatar.'" alt="user image" style="height: 100px;width: 100px;">',
            'options' => '<a href="javascript:void(0)" class="delete btn btn-sm btn-danger dlt_sm_table" data-toggle="modal" data-target="#modal-basic4" onclick="delete_security_resource('.$quotation->id.')"><i class="glyphicon glyphicon-trash"></i></a>'
        );

        if($driver_detail){
            array_push($response['resources'], $driver_arr);
        }

        if($vehicle_detail){
            array_push($response['resources'], $vehicle_arr);
        }

        if($security_detail){
            array_push($response['resources'], $security_arr);
        }
        
        echo json_encode($response);
    }

    function generate_bar_code($airwaybill){
        echo Zend_Barcode::render('code39', 'image', array('text' => $airwaybill), array());
    }

    function order_details($order){
        $data['orderno'] = $order;
        $data["drivers"] = $this->driver_model->getdrivers();
        $data["vehicles"] = $this->resources_model->getresources();
        $data["securities"] = $this->security_model->getsecurity();

        $data['order'] = $this->salesorder_model->getorderdetails($order);

        

        $this->load->view('header');
        $this->load->view('salesorder/order_details', $data);
        $this->load->view('footer');
    }

    function add(){
    	$data['contactpersons'] = $this->staff_model->staff_list(userdata_customer('id'));
        $data['contacts'] = $this->customer_model->get_location_contacts(userdata_customer('id'));
    	$data['pickups'] = $this->pickup_model->location_list(userdata_customer('id'));
        $data['deliveries'] = $this->pickup_model->location_list(userdata_customer('id'));
        $data['airwaybill'] = date('d').date('m').date('Y').userdata_customer('id').date('H').date('i').rand(1111, 9999);
        $data['packages'] = $this->packaging_model->getCustomerPackaging(userdata_customer('id'));
    	$this->load->view('header');
		$this->load->view('salesorder/new_order', $data);
		$this->load->view('footer');
    }

    function add_order_process(){
        if($this->form_validation->run('add_customer_main_order') == FALSE){
            echo '<div class="alert error"><ul>' . validation_errors('<li style="color:red">','</li>') . '</ul></div>';
        }
        else{
            if($this->site_model->add_order()){
                $billno = $this->input->post('airway_bill_no');

                echo '<div class="alert alert-success">order was placed succesfully</div>';
                // $this->sendordermail($billno);
            }
            else{
                echo $this->lang->line('technical_problem');
            }
        }
    }

    function add_process(){
    	if($this->form_validation->run('add_customer_order') == FALSE){
            echo '<div class="alert error"><ul>' . validation_errors('<li style="color:red">','</li>') . '</ul></div>';
        }
        else{
        	if($this->site_model->add_new_order()){
                //Generate PDF first
                // $billno = $this->input->post('airway_bill_no_order_form');
                // $data['airwaybill'] = $this->input->post('airway_bill_no_order_form');

                //Get contact names
                /*$data['ordercontact'] = implode(",", $this->input->post('contact_person'));
                $data['pickup'] = $this->input->post('pickup_location');
                $data['delivery'] = implode(",", $this->input->post('delivery_location'));
                $data['quantity'] = $this->input->post('item_quantity');
                $data['package'] = implode(",", $this->input->post('packaging_type'));
                $data['notes'] = $this->input->post('comments');
                $data['vehicle'] = 'NA';
                $data['pdate'] = $this->input->post('pickup_date');
                $data['ptime'] = $this->input->post('pickup_time');
                $data['ddate'] = $this->input->post('delivery_date');
                $data['dtime'] = $this->input->post('delivery_time');

                $html = $this->load->view('salesorder/createpdf', $data, true);
                $filename = $this->input->post('airway_bill_no_order_form');

                $pdfFilePath = FCPATH."/pdfs/".$filename.".pdf";
                $mpdf = new mPDF('c','A4','','',20,15,48,25,10,10); 
                $mpdf->SetProtection(array('print'));
                $mpdf->SetTitle("Order Request Summary Sheet");
                $mpdf->SetAuthor("Logistics & Energy Africa");

                $mpdf->watermark_font = 'DejaVuSansCondensed';
                $mpdf->watermarkTextAlpha = 0.1;
                $mpdf->SetDisplayMode('fullpage');       
                $mpdf->WriteHTML($html);
                $mpdf->Output($pdfFilePath, 'F');*/
        
                //echo base_url()."pdfs/".$filename.".pdf";
                //End PDF generate

                echo '<div class="alert alert-success">stop added successfully</div>';

                // we are not sending email at this point, but
                // until we save the order
                // $this->sendordermail($billno);
            }
            else{
                echo $this->lang->line('technical_problem');
            }
        }
    }

    function testpdf(){
        //Get contact names
        $data['airwaybill'] = '04042016116209660';
        $data['ordercontact'] = implode(",", array('4', '4'));
        $data['pickup'] = 1;
        $data['delivery'] = implode(",", array('6', '5'));
        $data['quantity'] = 12;
        $data['package'] = implode(",", array('6', '11', '13'));
        $data['notes'] = 'Blah blah blah blah blah';
        $data['vehicle'] = 'lorry';

        $html = $this->load->view('salesorder/createpdf', $data, true);
        $filename = '04042016116209663';

        $pdfFilePath = FCPATH."/pdfs/".$filename.".pdf";
        $mpdf = new mPDF('c','A4','','',20,15,48,25,10,10); 
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle("Order Request Summary Sheet");
        $mpdf->SetAuthor("Logistics & Energy Africa");

        $mpdf->watermark_font = 'DejaVuSansCondensed';
        $mpdf->watermarkTextAlpha = 0.1;
        $mpdf->SetDisplayMode('fullpage');       
        $mpdf->WriteHTML($html);

        sleep(5);
        $mpdf->Output($pdfFilePath, 'F');

        echo base_url()."pdfs/".$filename.".pdf";
    }

    function pending(){
        $this->load->view('header');
        $data['pending_orders'] = $this->salesorder_model->getPendingOrders(userdata_customer('id'));
        $this->load->view('salesorder/pending', $data);
        $this->load->view('footer');
    }

    function pending_ajax(){
        $items = $this->salesorder_model->getPendingOrders(userdata_customer('id'));

        $response = array();
        $response["items"] = array();

        foreach($items->result() as $item){
            $status = $item->customer_status;
            $m_status = '';

            if($status == 2){
                $m_status = '<span class="btn-sm" style="background-color: #ffc000; color:#ffffff; ">PENDING</span>';
            }
            elseif($status == 3){
                $m_status = '<span class="btn-sm" style="background-color: #ff99cc; color:#ffffff; ">ACTION NEEDED</span>';
            }
            elseif($status == 4){
                $m_status = '<span class="btn-sm" style="background-color: #00b0f0; color:#ffffff; ">LIVE</span>';
            }
            elseif($status == 5){
                $m_status = '<span class="btn-sm" style="background-color: #008000; color:#ffffff; ">CLOSED</span>';
            }

            $tmp = array(
                'airway' => $item->airwaybill_no,
                'transporter' => config('site_name'),
                'customer' => $this->salesorder_model->getcompanyname($item->customer_customer_id),
                'status' => $m_status,
                'comments' => $item->comment,
                'date' => date('d F Y g:i a', $item->order_date)
            );

            array_push($response["items"], $tmp);
        }

        echo json_encode($response);
    }

    function need_action(){
        $this->load->view('header');
        $data['orders'] = $this->salesorder_model->getActionsNeeded(userdata_customer('id'));
        $this->load->view('salesorder/action_needed', $data);
        $this->load->view('footer');
    }

    function need_action_ajax(){
        $items = $this->salesorder_model->getActionsNeeded(userdata_customer('id'));

        $response = array();
        $response["items"] = array();

        foreach($items->result() as $item){
            $status = $item->customer_status;
            $m_status = '';

            if($status == 2){
                $m_status = '<span class="btn-sm" style="background-color: #ffc000; color:#ffffff; ">PENDING</span>';
            }
            elseif($status == 3){
                $m_status = '<span class="btn-sm" style="background-color: #ff99cc; color:#ffffff; ">ACTION NEEDED</span>';
            }
            elseif($status == 4){
                $m_status = '<span class="btn-sm" style="background-color: #00b0f0; color:#ffffff; ">LIVE</span>';
            }
            elseif($status == 5){
                $m_status = '<span class="btn-sm" style="background-color: #008000; color:#ffffff; ">CLOSED</span>';
            }

            $tmp = array(
                'airway' => $item->airwaybill_no,
                'transporter' => config('site_name'),
                'customer' => $this->salesorder_model->getcompanyname($item->customer_customer_id),
                'status' => $m_status,
                'comments' => $item->comment,
                'date' => date('d F Y g:i a', $item->order_date)
            );

            array_push($response["items"], $tmp);
        }

        echo json_encode($response);
    }

    function live(){
        $this->load->view('header');
        $this->load->view('salesorder/live');
        $this->load->view('footer');
    }

    function live_ajax(){
        $items = $this->salesorder_model->getLiveOrders(userdata_customer('id'));

        $response = array();
        $response["items"] = array();

        foreach($items->result() as $item){
            $status = $item->customer_status;
            $m_status = '';

            if($status == 2){
                $m_status = '<span class="btn-sm" style="background-color: #ffc000; color:#ffffff; ">PENDING</span>';
            }
            elseif($status == 3){
                $m_status = '<span class="btn-sm" style="background-color: #ff99cc; color:#ffffff; ">ACTION NEEDED</span>';
            }
            elseif($status == 4){
                $m_status = '<span class="btn-sm" style="background-color: #00b0f0; color:#ffffff; ">LIVE</span>';
            }
            elseif($status == 5){
                $m_status = '<span class="btn-sm" style="background-color: #008000; color:#ffffff; ">CLOSED</span>';
            }

            $tmp = array(
                'airway' => $item->airwaybill_no,
                'transporter' => config('site_name'),
                'customer' => $this->salesorder_model->getcompanyname($item->customer_customer_id),
                'status' => $m_status,
                'comments' => $item->comment,
                'date' => date('d F Y g:i a', $item->order_date)
            );

            array_push($response["items"], $tmp);
        }

        echo json_encode($response);
    }

    function closed(){
        $this->load->view('header');
        $this->load->view('salesorder/closed');
        $this->load->view('footer');
    }

    function closed_ajax(){
        $items = $this->salesorder_model->getClosedOrders(userdata_customer('id'));

        $response = array();
        $response["items"] = array();

        foreach($items->result() as $item){
            $status = $item->customer_status;
            $m_status = '';

            if($status == 2){
                $m_status = '<span class="btn-sm" style="background-color: #ffc000; color:#ffffff; ">PENDING</span>';
            }
            elseif($status == 3){
                $m_status = '<span class="btn-sm" style="background-color: #ff99cc; color:#ffffff; ">ACTION NEEDED</span>';
            }
            elseif($status == 4){
                $m_status = '<span class="btn-sm" style="background-color: #00b0f0; color:#ffffff; ">LIVE</span>';
            }
            elseif($status == 5){
                $m_status = '<span class="btn-sm" style="background-color: #008000; color:#ffffff; ">CLOSED</span>';
            }

            $tmp = array(
                'airway' => $item->airwaybill_no,
                'transporter' => config('site_name'),
                'customer' => $this->salesorder_model->getcompanyname($item->customer_customer_id),
                'status' => $m_status,
                'comments' => $item->comment,
                'date' => date('d F Y g:i a', $item->order_date)
            );

            array_push($response["items"], $tmp);
        }

        echo json_encode($response);
    }

    function all(){
        $this->load->view('header');
        $data['all_orders'] = $this->salesorder_model->getPendingOrders(userdata_customer('id'));
        $this->load->view('salesorder/all_orders', $data);
        $this->load->view('footer');
    }

    function all_ajax(){
        $items = $this->salesorder_model->getAllOrders(userdata_customer('id'));

        $response = array();
        $response["items"] = array();

        foreach($items->result() as $item){
            $status = $item->customer_status;
            $m_status = '';

            if($status == 2){
                $m_status = '<span class="btn-sm" style="background-color: #ffc000; color:#ffffff; ">PENDING</span>';
            }
            elseif($status == 3){
                $m_status = '<span class="btn-sm" style="background-color: #ff99cc; color:#ffffff; ">ACTION NEEDED</span>';
            }
            elseif($status == 4){
                $m_status = '<span class="btn-sm" style="background-color: #00b0f0; color:#ffffff; ">LIVE</span>';
            }
            elseif($status == 5){
                $m_status = '<span class="btn-sm" style="background-color: #008000; color:#ffffff; ">CLOSED</span>';
            }

            $tmp = array(
                'airway' => $item->airwaybill_no,
                'transporter' => config('site_name'),
                'customer' => $this->salesorder_model->getcompanyname($item->customer_customer_id),
                'status' => $m_status,
                'comments' => $item->comment,
                'date' => date('d F Y g:i a', $item->order_date)
            );

            array_push($response["items"], $tmp);
        }

        echo json_encode($response);
    }

    function sendordermail($airwaybill){

        if(!empty($airwaybill)){
            $orderdetails = $this->site_model->getorderdetails($airwaybill);

            //prepare ccs
            /*$contacts = $orderdetails->contact_person;
            $ccs = array();

            $set = $this->site_model->getcontactnames($contacts);
            foreach($set->result() as $s){
                $ccs[] = $s->email;
            }

            $m_ccs = implode(",", $ccs);*/

            if($orderdetails){
                $mailstr = '<html><head> <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> <title>Logistics and Energy Africa</title> <style type="text/css"> a{color: #4A72AF;}body, #header h1, #header h2, p{margin: 0; padding: 0;}#main{border: 1px solid #cfcece;}img{display: block;}#top-message p, #bottom-message p{color: #3f4042; font-size: 12px; font-family: Arial, Helvetica, sans-serif;}#header h1{color: #ffffff !important; font-family: "Lucida Grande", "Lucida Sans", "Lucida Sans Unicode", sans-serif; font-size: 24px; margin-bottom: 0!important; padding-bottom: 0;}#header h2{color: #ffffff !important; font-family: Arial, Helvetica, sans-serif; font-size: 24px; margin-bottom: 0 !important; padding-bottom: 0;}#header p{color: #ffffff !important; font-family: "Lucida Grande", "Lucida Sans", "Lucida Sans Unicode", sans-serif; font-size: 12px;}h1, h2, h3, h4, h5, h6{margin: 0 0 0.8em 0;}h3{font-size: 28px; color: #444444 !important; font-family: Arial, Helvetica, sans-serif;}h4{font-size: 22px; color: #4A72AF !important; font-family: Arial, Helvetica, sans-serif;}h5{font-size: 18px; color: #444444 !important; font-family: Arial, Helvetica, sans-serif;}p{font-size: 12px; color: #444444 !important; font-family: "Lucida Grande", "Lucida Sans", "Lucida Sans Unicode", sans-serif; line-height: 1.5;}</style></head><body><table width="100%" cellpadding="0" cellspacing="0" bgcolor="e4e4e4"><tr><td><table id="header" cellpadding="14" cellspacing="0" align="center" bgcolor="8fb3e9"><tr><td width="570" bgcolor="#013f45"><h1></h1></td></tr><tr><td width="570" bgcolor="#ffc000"><center><h1>LOGISTICS ORDER INFORMATION</h1></center></td></tr></table><table id="main" width="600" align="center" cellpadding="0" cellspacing="15" bgcolor="ffffff"><tr><td></td></tr><tr><td><table id="content-1" cellpadding="0" cellspacing="0"><tr><td width="375" valign="top" colspan="3"><h5 style="color: #0572c1!important;">ORDER# ';
                $mailstr .= $airwaybill;
                $mailstr .= '</h5></td><td width="375" valign="top" colspan="3"><h5 style="color: #0572c1!important;">';
                $mailstr .= $this->site_model->getcompanyname($orderdetails->customer_customer_id);
                $mailstr .= '</h5></td></tr><tr><td width="375" valign="top" colspan="3"><h5 style="color: #0572c1!important;">Delivery Date :</h5></td><td width="375" valign="top" colspan="3"><h5 style="color: #0572c1!important;">';
                $mailstr .= ''; //$orderdetails->del_date.' '.$orderdetails->del_time;
                $mailstr .= '</h5></td></tr><tr style="margin-top: 100px;"><td width="375" valign="top" colspan="3"><h5 style="color: #0572c1!important;">Cut off time :</h5></td><td width="375" valign="top" colspan="3"><h5 style="color: #0572c1!important;">';
                $mailstr .= '10/19/2016 09:02:00 CST</h5></td></tr><tr style="margin-top: 100px;"><td width="375" valign="top" colspan="3"><h5 style="color: #0572c1!important;">System Order No :</h5></td><td width="375" valign="top" colspan="3"><h5 style="color: #0572c1!important;">';
                $mailstr .= $airwaybill;
                $mailstr .= '</h5></td></tr><tr style="margin-top: 100px;"><td width="375" valign="top" colspan="3"><h5 style="color: #0572c1!important;">Ordered By :';
                $mailstr .= $this->site_model->getcompanyname($orderdetails->customer_customer_id);
                $mailstr .= '</h5></td></tr></table></td></tr><tr><td><table id="content-2" cellpadding="0" cellspacing="0" align="center"><tr><td width="570" align="center"><p>Hello, Below are the details for your order request, find attached the order summary sheet. Thank you</p></td></tr></table></td></tr><tr><td align="center"><table id="content-6" cellpadding="0" cellspacing="0" align="center"><p align="center">Click the link below for more information about your order:</p><p align="center"><a href="'.$file = base_url()."pdfs/".$airwaybill.".pdf".'">Order Summary PDF File</a></p></table></td></tr></table><table id="header" cellpadding="14" cellspacing="0" align="center" bgcolor="8fb3e9"><tr><td width="570" bgcolor="#ffc000"><center><h1><br/><br/></h1></center></td></tr></table></td></tr></table></body></html>';
                $subject = 'Order Summary Sheet';

                send_order_summary(userdata_customer('email'), $subject, $mailstr, $airwaybill, $m_ccs);

                echo 'email sent';
            }
        }
    }

    function ajax(){
        //All Orders
        $all_obj = $this->salesorder_model->getAllOrders(userdata_customer('id'));
        $all_count = ($all_obj->num_rows() > 0) ? $all_obj->num_rows() : 0;

        //Pending Orders
        $pending_obj = $this->salesorder_model->getPendingOrders(userdata_customer('id'));
        $pending_count = ($pending_obj->num_rows() > 0) ? $pending_obj->num_rows() : 0;

        //Action needed Orders
        $actions_obj = $this->salesorder_model->getActionsNeeded(userdata_customer('id'));
        $actions_count = ($actions_obj->num_rows() > 0) ? $actions_obj->num_rows() : 0;

        //Live Orders
        $live_obj = $this->salesorder_model->getLiveOrders(userdata_customer('id'));
        $live_count = ($live_obj->num_rows() > 0) ? $live_obj->num_rows() : 0;

        //Closed Orders
        $closed_obj = $this->salesorder_model->getClosedOrders(userdata_customer('id'));
        $closed_count = ($closed_obj->num_rows() > 0) ? $closed_obj->num_rows() : 0;

        $response = array(
            'all' => $all_count,
            'pending' => $pending_count,
            'action_needed' => $actions_count,
            'live' => $live_count,
            'closed' => $closed_count
        );
        echo json_encode($response);
    }

    function delete_airway_item($airway){
        if($this->salesorder_model->delete_airway_item($airway)){
            echo 'deleted';
        } 
        else{
            echo 'delete failed';
        }
    }
}