<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Site_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }    
    
    function get_settings(){
        return $this->db->get_where('settings',array('id' => 1))->row();
    } 
    
    function general_settings(){
		$data = array(
            'site_name' => $this->input->post('site_name'),		
            'site_email' => $this->input->post('site_email')        
        );
        
		$condition = array('id' => 1);
		
		return $this->db->update('settings',$data,$condition);
	}
	
    function upload_settings(){
		$data = array(
            'allowed_extensions' => $this->input->post('allowed_extensions'),
            'max_upload_files' => $this->input->post('max_upload_files'),
            'max_upload_file_size' => $this->input->post('max_upload_file_size')
        );
        
		$condition = array('id' => 1);
		return $this->db->update('settings',$data,$condition);
	} 

    function getcontactnames($ids){
        return $this->db->query("SELECT * FROM logistics_cust_staff WHERE staff_id IN ($ids)");
    }

    function packages($package){
        return $this->db->query("SELECT * FROM logistics_packaging_type WHERE id IN ($package)");
    }

    function getorderdetails($airwaybill){
        return $this->db->get_where('logistics_order', array('airwaybill_no' => $airwaybill))->row();
    }

    function getcompanyname($customer){
        $company = '';

        $results = $this->db->get_where('company', array('id' => $customer));

        if($results){
            $row = $results->row();
            $company = $row->name;
        }

        return $company;
    }

    function getpickup($id){
        $pickup = '';

        $result = $this->db->get_where('logistics_pickup_loc', array('loc_id' => $id));

        $row = $result->row();

        if($row){
            $pickup = $row->name; 
        }

        return $pickup;
    }

    function getStops($stops){
        return $this->db->query("SELECT name FROM logistics_pickup_loc WHERE loc_id IN ($stops)");
    }

    function add_new_order(){
        return $this->db->insert('logistics_order_items', array(
            'airwaybill_no' => $this->input->post('airway_bill_no_order_form'),
            'contact_person' => implode(",", $this->input->post('stop_contact_person')),
            'pickup_loc' => $this->input->post('pickup_location'),
            'pickup_date' => $this->input->post('pickup_date'),
            'del_loc' => implode(",", $this->input->post('delivery_location')),
            'del_date' => $this->input->post('delivery_date'),
            'item_quantity' => $this->input->post('item_quantity'),
            'total_weight' => $this->input->post('total_weight'),
            'package_type' => implode(",", $this->input->post('packaging_type')),
            'unit' => $this->input->post('measure'),
            'del_time' => $this->input->post('delivery_time'),
            'pickup_time' => $this->input->post('pickup_time'),
            'item_date' => time()
        ));
    }

    function add_order(){
        $number_of_files = sizeof($_FILES['order_attachments']['tmp_name']);
        $files = $_FILES['order_attachments'];
        $errors = array();
        $filenames = array();

        for($i = 0; $i < $number_of_files; $i++){
            if($_FILES['order_attachments']['error'][$i] != 0){
                $errors[$i][] = 'Couldn\'t upload file '.$_FILES['order_attachments']['name'][$i];
            }
        }

        if(sizeof($errors) == 0){
            $this->load->library('upload');
            $config['upload_path'] = './uploads/products/';
            $config['allowed_types'] = 'gif|jpg|png|pdf|doc|docx|xls|xlsx';
            $config['encrypt_name'] = TRUE;

            for($i = 0; $i < $number_of_files; $i++){
                $_FILES['order_attachments']['name'] = $files['name'][$i];
                $_FILES['order_attachments']['type'] = $files['type'][$i];
                $_FILES['order_attachments']['tmp_name'] = $files['tmp_name'][$i];
                $_FILES['order_attachments']['error'] = $files['error'][$i];
                $_FILES['order_attachments']['size'] = $files['size'][$i];

                $this->upload->initialize($config);

                if($this->upload->do_upload('order_attachments')){
                    $fdata = $this->upload->data();
                    $data['uploads'][$i] = $this->upload->data();
                    $filenames[] = $fdata['file_name'];
                }
                else{
                    $data['upload_errors'][$i] = $this->upload->display_errors();
                }
            }
        }

        return $this->db->insert('logistics_order', array(
            'airwaybill_no' => $this->input->post('airway_bill_no'),
            'comment' => $this->input->post('comments'),
            'order_document' => implode(",", $filenames),
            'order_date' => time(),
            'customer_customer_id' => userdata_customer(),
            'order_status' => 1,
            'customer_status' => 2
        ));
    }
}
?>