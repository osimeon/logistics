<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Calendar_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }

	function order_list(){
		$this->db->order_by("DATE(pickup_date)", "DESC");		
        $this->db->from('logistics_order');
         
        return $this->db->get()->result();
	}
}
?>