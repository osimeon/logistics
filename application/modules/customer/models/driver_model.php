<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Driver_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function getdetails($driver){
    	return $this->db->get_where('logistics_trans_driver', array('staff_id' => $driver))->row();
    }

    function getdrivers(){
    	return $this->db->get('logistics_trans_driver');
    }

    function exists_email($email){
		$email_count = $this->db->get_where('logistics_trans_driver', array('email' => $email))->num_rows();
		return $email_count;
    }

    function updatedriver(){
    	if($this->input->post('pass1') != ""){
    		$member_details = array(
    			'fname' => $this->input->post('first_name'),
    			'lname' => $this->input->post('last_name'),
    			'email' => $this->input->post('email'),
    			'job' => $this->input->post('jobtitle'),
    			'phone' => $this->input->post('phone_number'),
    			'status' => $this->input->post('status'),
    			'password' => md5($this->input->post('pass1'))
    		);
		}
		else{
			$member_details = array(
				'fname' => $this->input->post('first_name'),
    			'lname' => $this->input->post('last_name'),
    			'email' => $this->input->post('email'),
    			'job' => $this->input->post('jobtitle'),
    			'phone' => $this->input->post('phone_number'),
    			'status' => $this->input->post('status')
    		);
		}

		return $this->db->update('logistics_trans_driver', $member_details, array('staff_id' => $this->input->post('user_id')));
    }

    function adddriver(){
		if(empty($_FILES['user_avatar']['name'])){	
			$staff_details = array(
                'fname' => $this->input->post('first_name'),
	            'lname' => $this->input->post('last_name'),
	            'email' => $this->input->post('email'),
	            'password' => md5( $this->input->post('pass1') ),
	            'transporter_id' => userdata('id'),
	            'register_time' => strtotime( date('d F Y g:i a') ),
	            'ip_address' => $this->input->server('REMOTE_ADDR'),
	            'role' => $this->input->post('account_role_id'),
	            'status' => $this->input->post('status'),
	            'phone' => $this->input->post('phone_number'),
	            'job' => $this->input->post('jobtitle')
            );
		}
		else{
			$config['upload_path'] = './uploads/';
			$config['allowed_types'] = config('allowed_extensions');
			$config['max_size']	= config('max_upload_file_size');
			$config['encrypt_name']	= TRUE;
			
			$this->load->library('upload', $config);
			
			if(!$this->upload->do_upload('user_avatar')){
				echo $this->upload->display_errors();
			}
			else{ 
				$img_data  = $this->upload->data();
				$staff_details = array(
                    'fname' => $this->input->post('first_name'),
		            'lname' => $this->input->post('last_name'),
		            'email' => $this->input->post('email'),
		            'password' => md5( $this->input->post('pass1') ),
		            'transporter_id' => userdata('id'),
		            'register_time' => strtotime( date('d F Y g:i a') ),
		            'ip_address' => $this->input->server('REMOTE_ADDR'),
		            'role' => $this->input->post('account_role_id'),
		            'status' => $this->input->post('status'),
		            'phone' => $this->input->post('phone_number'),
		            'job' => $this->input->post('jobtitle'),
                    'avatar' => $img_data['file_name']
                );
			}
		}

		return $this->db->insert('logistics_trans_driver', $staff_details);
	}

	function delete($staff_id){
		if($this->db->delete('logistics_trans_driver', array('staff_id' => $staff_id))){  
			return true;
		}
	}
}

?>