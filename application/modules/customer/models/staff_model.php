<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Staff_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }
	
	function exists_email($email){
		$email_count = $this->db->get_where('logistics_cust_staff',array('email' => $email))->num_rows();
		return $email_count;
    }
	
	function staff_list($customer){
		$this->db->order_by("staff_id", "desc");
        return $this->db->get_where('logistics_cust_staff', array('customer_customer_id' => $customer));
	}
	
	function get_user($staff_id){
		return $this->db->get_where('logistics_cust_staff', array('staff_id' => $staff_id))->row();
	}
	
	function add_staff(){
		return $this->db->insert('logistics_cust_staff', 
			$staff_details = array(
				'fname' => $this->input->post('first_name'),
	            'lname' => $this->input->post('last_name'),
	            'email' => $this->input->post('email'),
	            'password' => md5( $this->input->post('pass1') ),
	            'customer_customer_id' => userdata_customer(),
	            'register_time' => strtotime( date('d F Y g:i a') ),
	            'ip_address' => $this->input->server('REMOTE_ADDR'),
	            'role' => $this->input->post('account_role_id'),
	            'status' => $this->input->post('status'),
	            'phone' => $this->input->post('phone_number'),
	            'job' => $this->input->post('jobtitle')
        	));
	}
	
	function update_staff(){
		if($this->input->post('pass1') != ""){
			$member_details = array(
				'fname' => $this->input->post('first_name'),
				'lname' => $this->input->post('last_name'),
				'email' => $this->input->post('email'),
				'password' => md5( $this->input->post('pass1') ),
				'phone' => $this->input->post('phone_number'),
				'role' => $this->input->post('account_role_id'),
				'status' => $this->input->post('status')
			);	
		}	
		else{
			$member_details = array(
				'fname' => $this->input->post('first_name'),
				'lname' => $this->input->post('last_name'),
				'email' => $this->input->post('email'),					
				'phone' => $this->input->post('phone_number'),
				'role' => $this->input->post('account_role_id'),
				'status' => $this->input->post('status')
			);
		}
			
		return $this->db->update('logistics_cust_staff', $member_details, array('staff_id' => $this->input->post('user_id')));
    }
    
    function delete($staff_id){
		if($this->db->delete('logistics_cust_staff', array('staff_id' => $staff_id))){  
			return true;
		}
	}
	
	function get_user_fullname($staff_id){
		$this->db->where('staff_id', $staff_id);
		$q = $this->db->get('logistics_cust_staff');
		$data = $q->result_array();
		return $data[0]['fname'].' '.$data[0]['lname'];
	}
	
	function get_staff_user_image($staff_id){
		$this->db->where('staff_id', $staff_id);
		$q = $this->db->get('logistics_cust_staff');
		$data = $q->result_array();
		return $data[0]['user_avatar'];
	}
}
?>