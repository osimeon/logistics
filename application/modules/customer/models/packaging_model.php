<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Packaging_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }
	
	function getCustomerPackaging($customer){
		return $this->db->get_where('logistics_packaging_type', array('customer' => $customer));
	}

	function addPackage(){
		$package = array(
			'type' => $this->input->post('package_name'),
            'description' => $this->input->post('package_desc'),
            'customer' => userdata_customer(),
            'createdon' => strtotime( date('d F Y g:i a') ),
            'createdby' => userdata_customer()
       	);
		
		return $this->db->insert('logistics_packaging_type', $package);
	}

	function get_package($package){
		return $this->db->get_where('logistics_packaging_type', array('id' => $package))->row();
	}

	function updatePackage(){
		$package = array(
			'type' => $this->input->post('package_name'),
            'description' => $this->input->post('package_desc')
       	);

       	return $this->db->update('logistics_packaging_type', $package, array('id' => $this->input->post('package_id')));
	}

	function deletePackage($package){
		if($this->db->delete('logistics_packaging_type', array('id' => $package))){  
			return true;
		}
	}
}
?>