<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Pickup_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function location_list($customer){
        $this->db->order_by("name", "asc");
        $this->db->where('customer_customer_id', $customer);
        $this->db->select('loc_id, name, address, customer_customer_id, opening, closing, contact_person');
        $this->db->from('logistics_pickup_loc');
        return $this->db->get()->result(); 
    }

    function add_loc_del(){
        return $this->db->insert('logistics_pickup_loc', 
            array(
                'name' => $this->input->post('d_pickup_name'),
                'address' => $this->input->post('d_pickup_address'), 
                'customer_customer_id' => userdata_customer(),
                'opening' => $this->input->post('d_opening_hour'),
                'closing' => $this->input->post('d_closing_hour'),
                'contact_person' => $this->input->post('d_contact_person')
            ));
    }

    function add_loc(){
    	return $this->db->insert('logistics_pickup_loc', 
            array(
                'name' => $this->input->post('pickup_name'),
                'address' => $this->input->post('pickup_address'), 
                'customer_customer_id' => userdata_customer(),
                'opening' => $this->input->post('opening_hour'),
                'closing' => $this->input->post('closing_hour'),
                'contact_person' => $this->input->post('contact_person')
            ));
    }

    function get_loc_contact($contact){
        return $this->db->get_where('logistics_contacts', array('id' => $contact))->row();
    }

    function get_loc($loc_id){
        return $this->db->get_where('logistics_pickup_loc', array('loc_id' => $loc_id))->row();
    }

    function update_staff(){
        return $this->db->update('logistics_pickup_loc', 
            array(
                'name' => $this->input->post('pickup_name'), 
                'address' => $this->input->post('pickup_address'), 
                'opening' => $this->input->post('opening_hour'),
                'closing' => $this->input->post('closing_hour'),
                'contact_person' => $this->input->post('contact_person')
                ), 
            array('loc_id' => $this->input->post('loc_id')));
    }

    function delete($loc_id){
        if($this->db->delete('logistics_pickup_loc', array('loc_id' => $loc_id))){  
            return true;
        }
    }

    function package_types(){
        $this->db->order_by('type', 'asc');
        $this->db->select('id, type');
        $this->db->from('logistics_packaging_type');
        return $this->db->get()->result(); 
    }
}
?>