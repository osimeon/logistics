<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Resources_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function getvehicle($vehicle){
    	return $this->db->get_where('logistics_trans_resources', array('id' => $vehicle))->row();
    }

    function getresources(){
    	return $this->db->get('logistics_trans_resources');
    }

    function updatevehicle(){
    	if(empty($_FILES['resource_avatar']['name'])){	
			$resource_details = array(
                'name' => $this->input->post('vehicle_name'),
                'registration' => $this->input->post('vehicle_registration'),
	            'description' => $this->input->post('vehicle_description'),
	            'type' => $this->input->post('vehicle_type'),
	            'status' => $this->input->post('status')
            );
		}
		else{
			$config['upload_path'] = './uploads/resources';
			$config['allowed_types'] = config('allowed_extensions');
			$config['max_size']	= config('max_upload_file_size');
			$config['encrypt_name']	= TRUE;
			
			$this->load->library('upload', $config);
			
			if(!$this->upload->do_upload('resource_avatar')){
				echo $this->upload->display_errors();
			}
			else{
				$img_data  = $this->upload->data();

				$resource_details = array(
                    'name' => $this->input->post('vehicle_name'),
                    'registration' => $this->input->post('vehicle_registration'),
		            'description' => $this->input->post('vehicle_description'),
		            'type' => $this->input->post('vehicle_type'),
		            'status' => $this->input->post('status'),
                    'avatar' => $img_data['file_name']
                );
			}
		}

		return $this->db->update('logistics_trans_resources', $resource_details, array('id' => $this->input->post('vehicle_id')));
    }

    function addresource(){
    	if(empty($_FILES['resource_avatar']['name'])){	
			$resource_details = array(
                'name' => $this->input->post('resource_name'),
                'registration' => $this->input->post('resource_regno'),
	            'description' => $this->input->post('resource_description'),
	            'class' => 'road transport',
	            'type' => $this->input->post('resource_type'),
	            'registered' => strtotime( date('d F Y g:i a')),
	            'status' => $this->input->post('status'),
	            'transporter_id' => userdata('id')
            );
		}
		else{
			$config['upload_path'] = './uploads/resources';
			$config['allowed_types'] = config('allowed_extensions');
			$config['max_size']	= config('max_upload_file_size');
			$config['encrypt_name']	= TRUE;
			
			$this->load->library('upload', $config);
			
			if(!$this->upload->do_upload('resource_avatar')){
				echo $this->upload->display_errors();
			}
			else{ 
				$img_data  = $this->upload->data();

				$resource_details = array(
                    'name' => $this->input->post('resource_name'),
                    'registration' => $this->input->post('resource_regno'),
		            'description' => $this->input->post('resource_description'),
		            'class' => 'road transport',
		            'type' => $this->input->post('resource_type'),
		            'registered' => strtotime( date('d F Y g:i a')),
		            'status' => $this->input->post('status'),
		            'transporter_id' => userdata('id'),
                    'avatar' => $img_data['file_name']
                );
			}
		}

		return $this->db->insert('logistics_trans_resources', $resource_details);
    }
}

?>