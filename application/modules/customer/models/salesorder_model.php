<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Salesorder_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function get_airway_items($airway){
        return $this->db->get_where('logistics_order_items', array('airwaybill_no' => $airway));
    }

    function acceptquote($order){
    	return $this->db->update('logistics_order', array('customer_status' => 4, 'order_status' => 4), array('order_id' => $order));
    }

    function getorderdetails($order){
        return $this->db->get_where('logistics_order', array('order_id' => $order))->row();
    }

    function getcompanyname($customer){
        $company = '';

        $results = $this->db->get_where('company', array('id' => $customer));

        if($results){
            $row = $results->row();
            $company = $row->name;
        }

        return $company;
    }

    function gettransporters(){
    	return $this->db->get('users');
    }

    function getcompanyaddress($customer){
        $address = '';

        $results = $this->db->get_where('company', array('id' => $customer));

        if($results){
            $row = $results->row();
            $address = $row->address;
        }

        return $address;
    }

    function getcompanyemail($customer){
        $email = '';

        $results = $this->db->get_where('company', array('id' => $customer));

        if($results){
            $row = $results->row();
            $email = $row->email;
        }

        return $email;
    }

    function getcontactnames($ids){
    	$names = array();

        $clients = $this->db->query("SELECT * FROM logistics_cust_staff WHERE staff_id IN ($ids)");
        
        foreach($clients->result() as $client){
        	$names[] = $client->lname .' '.$client->fname;
        }

        return implode(',', $names);
    }

    function getlocations($stops){
    	$locations = array();

        $locs = $this->db->query("SELECT name FROM logistics_pickup_loc WHERE loc_id IN ($stops)");

        foreach($locs->result() as $loc){
        	$locations[] = $loc->name;
        }

        return implode(',', $locations);
    }

    function packages($package){
        return $this->db->query("SELECT * FROM logistics_packaging_type WHERE id IN ($package)");
    }

    function packagenames($package){
        $packages = array();

        $pckgs = $this->db->query("SELECT * FROM logistics_packaging_type WHERE id IN ($package)");

        foreach($pckgs->result() as $package){
            $packages[] = $package->type;
        }

        return implode(',', $packages);
    }
    
	function salesorder_list($customer_id){
		$this->db->where(array('quot_or_order' => 'o','customer_id' => $customer_id) );
		$this->db->order_by("id", "desc");		
        $this->db->from('quotations_salesorder');
        return $this->db->get()->result();
	} 
	
    function get_salesorder($quotation_id,$customer_id){
		return $this->db->get_where('quotations_salesorder', array('id' => $quotation_id,'customer_id' => $customer_id,'quot_or_order' => 'o'))->row();
	}
	  
	function quot_order_products($qo_id){		    
		$this->db->where(array('quotation_order_id' => $qo_id) );
		$this->db->order_by("id", "desc");		
        $this->db->from('quotations_salesorder_products');
         
        return $this->db->get()->result();
	}

    function getCalendarOrders($customer){
        return $this->db->get_where('logistics_order', array('customer_customer_id' => $customer))->result();
    }

	function getAllOrders($customer){
		return $this->db->get_where('logistics_order', array('customer_customer_id' => $customer));
	}

	function getPendingOrders($customer){
		return $this->db->get_where('logistics_order', array('customer_customer_id' => $customer, 'customer_status' => 2));
	}

	function getActionsNeeded($customer){
		return $this->db->get_where('logistics_order', array('customer_customer_id' => $customer, 'customer_status' => 3));
	}

	function getLiveOrders($customer){
		return $this->db->get_where('logistics_order', array('customer_customer_id' => $customer, 'customer_status' => 4));
	}

	function getClosedOrders($customer){
		return $this->db->get_where('logistics_order', array('customer_customer_id' => $customer, 'customer_status' => 5));
	}

    function delete_airway_item($airway){
        if($this->db->delete('logistics_order_items', array('item_id' => $airway))){  
            return true;
        }
        else{
            return false;
        }
    }
}
?>