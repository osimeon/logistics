<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/1.12.0/semantic.min.css">
<script>
function delete_user(staff_id){
    var result = confirm("are you sure you want to delete staff?");
    
    if(result){
      $.ajax({
        type: "GET",
        url: "<?php echo site_url('customer/staff/delete' ); ?>/" + staff_id,
        success: function(msg){
    			if(msg == 'deleted'){
            $('#staff_id_' + staff_id).fadeOut('normal');
          }
        }
      });
    }
 }
</script>

<script>
  $(document).ready(function() {
    $("form[name='add_staff']").submit(function(e) {
      //Firstname
      if(!$('#first_name').val()){
        if ($("#first_name").parent().next(".validation").length == 0){
          $("#first_name").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter first name</div>");
        }

        e.preventDefault();
        return;
      } 
      else {
        $("#first_name").parent().next(".validation").remove();
      }

      //Lastname
      if(!$('#last_name').val()){
        if ($("#last_name").parent().next(".validation").length == 0){
          $("#last_name").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter last name</div>");
        }

        e.preventDefault();
        return;
      } 
      else {
        $("#last_name").parent().next(".validation").remove();
      }

      //Phone number
      if(!$('#phone_number').val()){
        if ($("#phone_number").parent().next(".validation").length == 0){
          $("#phone_number").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter phone number</div>");
        }

        e.preventDefault();
        return;
      } 
      else {
        $("#phone_number").parent().next(".validation").remove();
      }

      //Email address
      if(!$('#email').val()){
        if ($("#email").parent().next(".validation").length == 0){
          $("#email").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter email address</div>");
        }

        e.preventDefault();
        return;
      } 
      else {
        $("#email").parent().next(".validation").remove();
      }

      //Job title
      if(!$('#jobtitle').val()){
        if ($("#jobtitle").parent().next(".validation").length == 0){
          $("#jobtitle").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter job title</div>");
        }

        e.preventDefault();
        return;
      } 
      else {
        $("#jobtitle").parent().next(".validation").remove();
      }

      //Password 1
      if(!$('#password1').val()){
        if ($("#password1").parent().next(".validation").length == 0){
          $("#password1").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter password</div>");
        }

        e.preventDefault();
        return;
      } 
      else {
        $("#password1").parent().next(".validation").remove();
      }

      //Password 2
      if(!$('#password2').val()){
        if ($("#password2").parent().next(".validation").length == 0){
          $("#password2").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter confirmation password</div>");
        }

        e.preventDefault();
        return;
      } 
      else {
        $("#password2").parent().next(".validation").remove();
      }

      var formData = new FormData($(this)[0]);
      $.ajax({
          url: "<?php echo site_url('customer/staff/add_process'); ?>",
          type: "POST",
          data: formData,
          async: false,
          beforeSend : function(msg){ $("#submitbutton").html('<img src="<?php echo base_url('public/images/loading.gif'); ?>" />'); },
          success: function (msg) {
            $('#new_staff').animate({ scrollTop: 0 }, 200);
            $("#staff_ajax").html(msg); 
            $("#staff_ajax").delay(500).fadeOut();
            $("#submitbutton").html('<button type="submit" class="btn btn-embossed btn-primary">Save</button>');
            $("#add_staff").trigger("reset");
          },
          cache: false,
          contentType: false,
          processData: false
      });

      e.preventDefault();
    });
  });
 </script>

<!-- BEGIN PAGE CONTENT -->
<div class="page-content page-thin">
  <div class="header">
    <h2><strong>Staff</strong></h2>            
  </div>
  <div class="row">
 	  <div class="col-lg-12">
      <div class="panel">
        <div class="panel-header">
          <h3><i class="fa fa-table"></i> <strong>Manage </strong> Staff</h3>
        </div>
        <div class="panel-content"> 
          <div class="m-b-20">
            <div class="btn-group">
              <!--<a href="<?php echo site_url('customer/staff/add'); ?>" class="btn btn-sm btn-dark"><i class="fa fa-plus"></i> Add New</a>-->
              <a href="javascript:void(0)" class="btn btn-embossed btn-primary" data-toggle="modal" data-target="#new_staff"><i class="fa fa-plus"></i> Add New</a>
            </div>
            <div> 
              <?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');}?>         
            </div>
          </div>

          <div class="panel-content pagination2 table-responsive">
            <table class="table table-hover table-dynamic filter-between_date" id="customer_staff">
              <thead>
                <tr>                        
                  <th><?php echo $this->lang->line('name'); ?></th>
                  <th><?php echo $this->lang->line('email'); ?></th>
                  <th>JOB POSITION</th>
                  <th>ACTIVE</th>
                  <th><?php echo $this->lang->line('register_time'); ?></th>
                  <th><?php echo $this->lang->line('options'); ?></th>    
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
				</div>
      </div>
    </div>
  </div>
</div>    
<div class="modal fade" id="new_staff" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
        <h4 class="modal-title"><strong>Add Staff</strong></h4>
      </div>
      <div id="staff_ajax"> 
        <?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');}?>         
      </div>
      <form id="add_staff" name="add_staff" class="form-validation" accept-charset="utf-8" enctype="multipart/form-data" method="post">
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">First Name</label>
                <div class="append-icon">
                  <input type="text" name="first_name" value="" class="form-control" id="first_name">
                  <i class="icon-user"></i>
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Last Name</label>
                <div class="append-icon">
                  <input type="text" name="last_name" value="" class="form-control" id="last_name">
                  <i class="icon-user"></i>
                </div>
              </div>
            </div>
          </div>
          <div class="row">                                
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Phone Number</label>
                <div class="append-icon">
                  <input type="text" name="phone_number" value="" class="form-control" id="phone_number">
                  <i class="icon-screen-smartphone"></i>
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Email Address</label>
                <div class="append-icon">
                  <input type="email" name="email" value="" class="form-control" id="email">
                  <i class="icon-envelope"></i>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                  <label class="control-label">Job Title</label>
                  <div class="append-icon">
                    <input type="jobtitle" name="jobtitle" value="" class="form-control" id="jobtitle">
                    <i class="icon-envelope"></i>
                  </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Active</label>
                <div class="append-icon">
                  <?php $status = ($staff->status == 1) ? 'checked' : ''; ?>
                  <input type="checkbox" name="status" value="1" <?php echo $status; ?> data-checkbox="icheckbox_square-blue"/> 
                   
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Password</label>
                  <div class="append-icon">
                    <input type="password" name="pass1" id="password1" value="" class="form-control">
                    <i class="icon-lock"></i>
                  </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Repeat Password</label>
                <div class="append-icon">
                  <input type="password" name="pass2" id="password2" value="" class="form-control">
                  <i class="icon-lock"></i>
                </div>
              </div>
            </div>
          </div>
          <div class="text-left  m-t-20">
            <div id="submitbutton"><button type="submit" class="btn btn-embossed btn-primary">Create</button></div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>