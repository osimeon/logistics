<div class="sidebar">
  <div class="logopanel">
    <a href="<?php echo site_url('customer/dashboard/'); ?>"><img src="<?php echo base_url('uploads/site').'/'.config('site_logo'); ?>" alt="company logo" class="" style="height: 30px;"></a> 
  </div>
        
  <div class="sidebar-inner">
    <div class="menu-title">
      Navigation 
    </div>
    <ul class="nav nav-sidebar">
      <li class=" nav-active <?php echo is_active_menu('dashboard'); ?>">
        <a href="<?php echo site_url('customer/dashboard'); ?>">
          <i class="icon-home"></i><span>Dashboard</span>
        </a>
      </li>
  		
      <li class="nav-parent <?php echo is_active_menu('salesorder'); ?>">
        <a href="#"><i class="fa fa-shopping-cart"></i><span>My Orders</span> <span class="fa arrow"></span></a>
        <ul class="children collapse">
          <li><a href="<?php echo site_url('customer/salesorder/all'); ?>" style="color: #444444;"><b>All (<span id="cust_all_orders"><?php echo 0; ?></span>)</b></a></li>
          <li><a href="<?php echo site_url('customer/salesorder/pending'); ?>" style="color: #ffc000;"><b>Pending (<span id="cust_pending_orders"><?php echo 0; ?></span>)</b></a></li>
          <li><a href="<?php echo site_url('customer/salesorder/need_action'); ?>" style="color: #ff99cc;"><b>Action Needed (<span id="cust_action_orders"><?php echo 0; ?></span>)</b></a></li>
          <li><a href="<?php echo site_url('customer/salesorder/live'); ?>" style="color: #00b0f0;"><b>Live (<span id="cust_live_orders"><?php echo 0; ?></span>)</b></a></li>
          <li><a href="<?php echo site_url('customer/salesorder/closed'); ?>" style="color: #008000;"><b>Closed (<span id="cust_closed_orders"><?php echo 0; ?></span>)</b></a></li>
        </ul>
      </li>    
      
      <li class=" nav-active <?php echo is_active_menu('staff'); ?>"><a href="<?php echo site_url('customer/staff'); ?>"><i class="icon-users"></i><span>Staff</span></a></li>
      
      <li class="nav-parent nav-active <?php echo is_active_menu('pickup'); ?>">
        <a href="#"><i class="glyphicon glyphicon-map-marker"></i><span>Locations</span> <span class="fa arrow"></span></a>
        <ul class="children collapse">
          <li><a href="<?php echo site_url('customer/contacts'); ?>">Contacts</a></li>
          <li><a href="<?php echo site_url('customer/pickup'); ?>">Locations</a></li>
        </ul>
      </li>
      <li class=" nav-active <?php echo is_active_menu('packaging'); ?>"><a href="<?php echo site_url('customer/package'); ?>"><i class="fa fa-cubes"></i><span>Packaging</span></a></li>
    </ul>
    </ul>
  </div>
</div>