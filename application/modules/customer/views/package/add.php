<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<style type="text/css">
	.validation{
      color: red;
      margin-bottom: 20px;
    }
</style>
<script>
	$(document).ready(function() {
		$("form[name='new_package']").submit(function(e) {
			//Name
			if(!$('#package_name').val()){
        		if ($("#package_name").parent().next(".validation").length == 0){
            		$("#package_name").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter name</div>");
    			}
    			e.preventDefault();
        		return;
    		} 
    		else {
        		$("#package_name").parent().next(".validation").remove();
    		}

    		//Description
    		if(!$('#package_desc').val()){
        		if ($("#package_desc").parent().next(".validation").length == 0){
            		$("#package_desc").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter description</div>");
    			}
    			e.preventDefault();
        		return;
    		} 
    		else {
        		$("#package_desc").parent().next(".validation").remove();
    		}

	        var formData = new FormData($(this)[0]);

	        $.ajax({
	            url: "<?php echo site_url('customer/package/add_process'); ?>",
	            type: "POST",
	            data: formData,
	            async: false,
	            success: function (msg) {
					$('body,html').animate({ scrollTop: 0 }, 200);
	            	$("#order_ajax").html(msg); 
					$("#order_submitbutton").html('<button type="submit" class="btn btn-embossed btn-primary">Save</button>');
				
					$("form[name='new_package']").find("input[type=text], textarea").val("");
	        	},

	            cache: false,
	            contentType: false,
	            processData: false
	        });

	        e.preventDefault();
	    });
	});
 </script>
 <!-- BEGIN PAGE CONTENT -->
<div class="page-content">
	<div class="header">
        <h2><strong>New Package Type</strong></h2>            
  	</div>
   	
   	<div class="row">
   		<div class="col-md-12">
      		<div class="panel">
         		<div class="panel-content">
					<div id="order_ajax"> 
            			<?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');}?>         
            		</div>
		            
		            <form id="new_package" name="new_package" class="form-validation" accept-charset="utf-8" method="post">
			      		<div class="row">
        					<div class="col-sm-12">
								<div class="form-group">
									<label class="control-label">Name</label>
									<div class="append-icon">
								  		<input type="text" name="package_name" value="" class="form-control" id="package_name">
									</div>
								</div>
                  			</div>
                  			<div class="col-sm-12">
                      			<div class="form-group">
                      				<label class="control-label">Description</label>
		                        	<div class="append-icon">
		                        		<textarea name="package_desc" rows="4" class="form-control" id="package_desc"></textarea>
		                      		</div>
                      			</div>
                  			</div>
                		</div>      
        				<div class="text-left  m-t-20">
     				 		<div id="order_submitbutton"><button type="submit" class="btn btn-embossed btn-primary">Add Package Type</button></div>
                        </div>
                  	</form>             
              	</div>
          	</div>
        </div>
   	</div>
</div>   
<!-- END PAGE CONTENT -->