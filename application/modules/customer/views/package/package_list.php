<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js">
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $("form[name='new_package']").submit(function(e) {
    //Name
    if(!$('#package_name').val()){
      if($("#package_name").parent().next(".validation").length == 0){
        $("#package_name").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter name</div>");
      }
      e.preventDefault();
      return;
    } 
    else {
      $("#package_name").parent().next(".validation").remove();
    }

    //Description
    if(!$('#package_desc').val()){
      if($("#package_desc").parent().next(".validation").length == 0){
        $("#package_desc").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter description</div>");
      }
      e.preventDefault();
      return;
    } 
    else {
      $("#package_desc").parent().next(".validation").remove();
    }

    var formData = new FormData($(this)[0]);
    $.ajax({
        url: "<?php echo site_url('customer/package/add_process'); ?>",
        type: "POST",
        data: formData,
        beforeSend : function(msg){ $("#package_submitbutton").html('<img src="<?php echo base_url('public/images/loading.gif'); ?>" />'); },
        async: false,
        success: function (msg) {
          $('body,html').animate({ scrollTop: 0 }, 200);
          $("#pickup_ajax").html(msg); 
          $("#pickup_ajax").delay(500).fadeOut();
          $("#package_submitbutton").html('<button type="submit" class="btn btn-embossed btn-primary">Save</button>');
          $("#new_package").trigger("reset");
        },
        cache: false,
        contentType: false,
        processData: false
    });

    e.preventDefault();
});
  });
</script>

<script>
function delete_package(package_id){
    var result = confirm("are you sure you want to delete package?");
    
    if(result){
      $.ajax({
        type: "GET",
        url: "<?php echo site_url('customer/package/delete' ); ?>/" + package_id,
        success: function(msg){
    			if(msg == 'deleted'){
            $('#package_id_' + package_id).fadeOut('normal');
          }
        }
      });
    }
 }

 </script>

<!-- BEGIN PAGE CONTENT -->
<div class="page-content page-thin">
  <div class="header">
    <h2><strong>Packaging</strong> Types</h2>            
  </div>
  <div class="row">
 	  <div class="col-lg-12">
      <div class="panel">
        <div class="panel-header">
          <h3><i class="fa fa-table"></i> <strong>Manage </strong> Packaging Types</h3>
        </div>
        <div class="panel-content"> 
          <div class="m-b-20">
            <div class="btn-group">
              <!--<a href="<?php echo site_url('customer/package/add'); ?>" class="btn btn-sm btn-dark"><i class="fa fa-plus"></i> Add New</a>-->
              <a href="javascript:void(0)" class="btn btn-primary" data-toggle="modal" data-target="#pop_new_package"><i class="fa fa-plus"></i> Add New</a>
            </div>
            <div> 
              <?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');}?>         
            </div>
          </div>

          <div class="panel-content pagination2 table-responsive">
            <table class="table table-hover table-dynamic filter-between_date" id="packages_list">
              <thead>
                <tr>                        
                  <th><?php echo $this->lang->line('name'); ?></th>
                  <th>DESCRIPTION</th> 
                  <th>OPTIONS</th>   
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
				</div>
      </div>
    </div>
  </div>
</div> 

<div class="modal" id="pop_new_package" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
        <h4 class="modal-title"><strong>New Package Type</strong></h4>
      </div>
      <div id="pickup_ajax" style="color:red;margin-left:15px;"> 
        <?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');}?>         
      </div>

      <form id="new_package" name="new_package" class="form-validation" accept-charset="utf-8" enctype="multipart/form-data" method="post">
        <div class="modal-body">
          <div class="row">
              <div class="col-md-12">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group">
                      <label class="control-label">Name</label>
                      <div class="append-icon">
                        <input type="text" name="package_name" value="" class="form-control" id="package_name">
                        <i class="icon-pencil"></i>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group">
                      <label class="control-label">Description</label>
                      <div class="append-icon">
                        <textarea name="package_desc" rows="4" class="form-control" id="package_desc"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
        <div id="package_submitbutton" class="modal-footer text-left"><button type="submit" class="btn btn-primary btn-embossed bnt-square">Add Package Type</button></div>
      </form>
    </div>
  </div>
</div>       