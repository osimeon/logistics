<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>
	$(document).ready(function() {
		$("form[name='update_package']").submit(function(e) {
			//Name
			if(!$('#package_name').val()){
        		if ($("#package_name").parent().next(".validation").length == 0){
            		$("#package_name").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter name</div>");
    			}
    			e.preventDefault();
        		return;
    		} 
    		else {
        		$("#package_name").parent().next(".validation").remove();
    		}

    		//Description
    		if(!$('#package_desc').val()){
        		if ($("#package_desc").parent().next(".validation").length == 0){
            		$("#package_desc").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter description</div>");
    			}
    			e.preventDefault();
        		return;
    		} 
    		else {
        		$("#package_desc").parent().next(".validation").remove();
    		}
    		
	        var formData = new FormData($(this)[0]);

	        $.ajax({
	            url: "<?php echo site_url('customer/package/update_process'); ?>",
	            type: "POST",
	            data: formData,
	            async: false,
	            success: function (msg) {
				$('body,html').animate({ scrollTop: 0 }, 200);
	            $("#profile_ajax").html(msg); 
				$("#profile_submitbutton").html('<button type="submit" class="btn btn-embossed btn-primary">Save</button>');
	        },
	            cache: false,
	            contentType: false,
	            processData: false
	        });

	        e.preventDefault();
	    });
	});
 </script>
 <!-- BEGIN PAGE CONTENT -->
<div class="page-content page-thin">
	<div class="header">
    	<h2>Update Package</h2>            
  	</div>
   	
   	<div class="row">
   		<div class="col-md-12">
      		<div class="panel">
         		<div class="panel-content">
   					<div id="profile_ajax">                      	                       
                    	<?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');}?>                     	
                  	</div>
  				    
  				    <form id="update_package" name="update_package" class="form-validation" accept-charset="utf-8" method="post">
    					<input type="hidden" name="package_id" value="<?php echo $package->id; ?>" />
        				<div class="row">
          					<div class="col-sm-12">
	                            <div class="form-group">
                              		<label class="control-label">Name</label>
                              		<div class="append-icon">
	                                	<input type="text" name="package_name" value="<?php echo $package->type; ?>" class="form-control" id="package_name">
	                                	<i class="icon-user"></i>
	                              	</div>
	                            </div>
                          	</div>
                          	<div class="col-sm-12">
                            	<div class="form-group">
                              		<label class="control-label">Description</label>
                              		<div class="append-icon">
	                                	<textarea name="package_desc" rows="4" class="form-control" id="package_desc"><?php echo $package->description; ?></textarea>
	                              	</div>
	                            </div>
                          	</div>
                        </div>	
        				<div class="text-left  m-t-20">
 				 			<div id="profile_submitbutton"><button type="submit" class="btn btn-embossed btn-primary">Update</button></div>
                		</div>
                  	</form>
		 		</div>
      		</div>
    	</div>
	</div>
</div>   
<!-- END PAGE CONTENT -->	