<html>
	<head>
	   <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	   <title>Logistics and Energy Africa</title>
	   <style type="text/css">
	   		a {
	   			color: #4A72AF;
	   		}
			body, #header h1, #header h2, p {
				margin: 0; 
				padding: 0;
			}

			#main {
				border: 1px solid #cfcece;
			}
			img {
				display: block;
			}
			
			#top-message p, #bottom-message p {
				color: #3f4042; 
				font-size: 12px; 
				font-family: Arial, Helvetica, sans-serif; 
			}
			
			#header h1 {
				color: #ffffff !important; 
				font-family: "Lucida Grande", "Lucida Sans", "Lucida Sans Unicode", sans-serif; 
				font-size: 24px; margin-bottom: 0!important; padding-bottom: 0; 
			}

			#header h2 {
				color: #ffffff !important; 
				font-family: Arial, Helvetica, sans-serif; 
				font-size: 24px; margin-bottom: 0 !important; padding-bottom: 0; 
			}
			#header p {
				color: #ffffff !important; 
				font-family: "Lucida Grande", "Lucida Sans", "Lucida Sans Unicode", sans-serif; 
				font-size: 12px;  
			}
			h1, h2, h3, h4, h5, h6 {
				margin: 0 0 0.8em 0;
			}
			h3 {
				font-size: 28px; 
				color: #444444 !important; 
				font-family: Arial, Helvetica, sans-serif; 
			}
			h4 {
				font-size: 22px; 
				color: #4A72AF !important; 
				font-family: Arial, Helvetica, sans-serif; 
			}
			h5 {
				font-size: 18px; 
				color: #444444 !important; 
				font-family: Arial, Helvetica, sans-serif; 
			}
			p {font-size: 12px; 
				color: #444444 !important; 
				font-family: "Lucida Grande", "Lucida Sans", "Lucida Sans Unicode", sans-serif; line-height: 1.5;
			}
	   </style>
	</head>
	<body>
		<table width="100%" cellpadding="0" cellspacing="0" bgcolor="e4e4e4">
			<tr>
				<td>
					<table id="top-message" cellpadding="20" cellspacing="0" width="600" align="center">
						<tr>
							<td align="center">
								<p style="display:none;">Trouble viewing this email? <a href="#">View in Browser</a></p>
							</td>
						</tr>
					</table><!-- top message -->
					
					<table id="main" width="600" align="center" cellpadding="0" cellspacing="15" bgcolor="ffffff">
						<tr>
							<td>
								<table id="header" cellpadding="10" cellspacing="0" align="center" bgcolor="8fb3e9">
									<tr>
										<td width="570" bgcolor="7aa7e9"><h1>Logistics &amp; Africa Energy</h1></td>
									</tr>
								</table><!-- header -->
							</td>
						</tr><!-- header -->
						<tr>
							<td></td>
						</tr>
						<tr>
							<td>
								<table id="content-1" cellpadding="0" cellspacing="0" align="center">
									<tr>
										<td width="375" valign="top" colspan="3">
											<h3>Welcome to Logistics &amp; Africa Energy</h3>
											<h4>Going Beyond | Going Places</h4>
										</td>
									</tr>
								</table><!-- content 1 -->
							</td>
						</tr><!-- content 1 -->
						<tr>
							<td>
								<table id="content-2" cellpadding="0" cellspacing="0" align="center">
									<tr>
										<td width="570" align="center">
											<p>
												Hello Mobidawa Ltd, <br/><br/>
												Your account at Logistics & Energy Africa Ltd is ready,<br/>
												Below are the details that you used to when creating the account: 
											</p>
											<br/>
											<p>
												<b>Email: </b>simeon.obwogo79@gmail.com<br/>
												<b>Password: </b>obsiha2013<br/><br/>
												<b>Please click here for activation: </b>
											</p>
										</td>
									</tr>
								</table><!-- content-2 -->
							</td>
						</tr><!-- content-2 -->
						<tr>
							<td align="center">
								<table id="content-6" cellpadding="0" cellspacing="0" align="center">
									<p align="center">You may copy paste this link in your browser:</p>
									<p align="center"><a href="http://logistics.mymarket.co.ke/index.php/customer">http://logistics.mymarket.co.ke/index.php/customer</a></p>
								</table>
							</td>
						</tr>
					</table><!-- main -->
					<table id="bottom-message" cellpadding="20" cellspacing="0" width="600" align="center">
						<tr>
							<td align="center">
								<p>You are receiving this email because you signed up for Logistics and Energy Africa Ltd customer portal</p>
							</td>
						</tr>
					</table><!-- top message -->
				</td>
			</tr>
		</table><!-- wrapper -->
	</body>
</html>