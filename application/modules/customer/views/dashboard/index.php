<!-- BEGIN PAGE CONTENT -->
<div class="page-content page-thin">
    <div class="header">
        <h2><strong><?php echo userdata_customer('name'); ?></strong> - Customer Dashboard</h2>

    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel" id="dashboard_welcome">
                <div class="panel-header">
                    <button type="button" class="close" data-target="#dashboard_welcome" data-dismiss="alert" align="left">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button> 
                    <h4>Welcome to Logistics & Energy Africa</h4>
                    <h5>We've assembled some links to get you started</h5>
                </div>
                <div class="panel-content pagination2 table-responsive">
                    <div class="row">
                        <div class="col-lg-4">
                            <h5><strong>Next steps</strong></h5>
                            <h6><a href="<?php echo site_url('customer/staff/add'); ?>"><i class="icon-users"></i> <span>Add staff</span></a></h6>
                            <h6><a href="<?php echo site_url('customer/pickup/add'); ?>"><i class="glyphicon glyphicon-map-marker"></i> <span>Add location</span></a></h6>
                            <h6><a href="<?php echo site_url('customer/customer/new_contact'); ?>"><i class="icon-user"></i> <span>Add contact Person</span></a></h6>
                        </div>
                        <div class="col-lg-4">
                            <h5><strong>Place order</strong></h5>
                            <a href="<?php echo site_url('customer/salesorder/add'); ?>" class="btn btn-primary">New Order</a>
                        </div>
                        <div class="col-lg-4">
                            <h5><strong>More Actions</strong></h5>
                            <h6><a href="<?php echo site_url('customer/settings'); ?>"><i class="icon-settings"></i> <span>Update profile</span></a></h6>
                            <h6><a href="<?php echo site_url('customer/staff'); ?>"><i class="icon-users"></i> <span>Manage staff</span></a></h6>
                            <h6><a href="<?php echo site_url('customer/package/add'); ?>"><i class="fa fa-cubes"></i> <span>Add package types</span></a></h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="panel">
                <div class="panel-header">
                    <h4><strong>At a Glance</strong></h4>
                    <hr/>
                </div>
                <div class="panel-content pagination2 table-responsive">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="txt">Total Deliveries</div>
                            <br/>
                            <p class="btn" style="background-color: #00b0f0;">
                            0
                            </p>
                        </div>
                        <div class="col-md-4">
                            <div class="txt">On Time</div>
                            <br/>
                            <p class="btn" style="background-color: #00b050;">
                            0
                            </p>
                        </div>
                        <div class="col-md-4">
                            <div class="txt">Delays</div>
                            <br/>
                            <p class="btn" style="background-color: #ff0000;">
                            0
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="txt">On Time %</div>
                        </div>
                        <div class="col-md-4">
                            <p class="btn">
                            0%
                            </p>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <a href="<?php echo site_url('customer/salesorder/all'); ?>"><span id="glance_all" style="color: #444444;">All (0)</span></a> &nbsp; | &nbsp;
                    <a href="<?php echo site_url('customer/salesorder/pending'); ?>"><span id="glance_pending" style="color: #ffc000;">Pending (0) </span></a> &nbsp; | &nbsp; 
                    <a href="<?php echo site_url('customer/salesorder/need_action'); ?>"<span id="glance_action_needed" style="color: #ff99cc;">Action Needed(0) </span></a> &nbsp; | &nbsp;
                    <a href="<?php echo site_url('customer/salesorder/live'); ?>"><span id="glance_live" style="color: #00b0f0;">Live (0) </span></a> &nbsp; | &nbsp;
                    <a href="<?php echo site_url('customer/salesorder/closed'); ?>"><span id="glance_closed" style="color: #008000;">Closed (0) </span></a>
                </div>
            </div>
            <div class="panel">
                <div class="panel-header">
                    <h4><strong>Activity</strong></h4>
                    <hr/>
                </div>
                <div class="panel-content pagination2 table-responsive">

                </div>
                <div class="panel-footer">
                
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="panel">
                <div class="panel-header">
                    <h4><strong>News Feed</strong></h4>
                    <hr/>
                </div>
                <div class="panel-content pagination2 table-responsive">

                </div>
                <div class="panel-footer">

                </div>
            </div>
        </div>
    </div>
    
</div>
<!-- END PAGE CONTENT -->