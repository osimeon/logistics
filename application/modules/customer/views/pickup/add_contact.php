<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>
	$(document).ready(function() {
		$("form[name='add_contact']").submit(function(e) {
          //Name
          if(!$('#contact_name').val()){
            if ($("#contact_name").parent().next(".validation").length == 0){
                $("#contact_name").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter name</div>");
            }
            e.preventDefault();
            return;
          } 
          else {
              $("#contact_name").parent().next(".validation").remove();
          }

          //Email
          if(!$('#contact_email').val()){
            if ($("#contact_email").parent().next(".validation").length == 0){
                $("#contact_email").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter email address</div>");
            }
            e.preventDefault();
            return;
          } 
          else {
              $("#contact_email").parent().next(".validation").remove();
          }

          //Position
          if(!$('#contact_position').val()){
            if ($("#contact_position").parent().next(".validation").length == 0){
                $("#contact_position").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter position</div>");
            }
            e.preventDefault();
            return;
          } 
          else {
              $("#contact_position").parent().next(".validation").remove();
          }

          //Password
          if(!$('#password').val()){
            if ($("#password").parent().next(".validation").length == 0){
                $("#password").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter password</div>");
            }
            e.preventDefault();
            return;
          } 
          else {
              $("#password").parent().next(".validation").remove();
          }

          //Password 2
          if(!$('#password2').val()){
            if ($("#password2").parent().next(".validation").length == 0){
                $("#password2").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter confirmation password</div>");
            }
            e.preventDefault();
            return;
          } 
          else {
              $("#password2").parent().next(".validation").remove();
          }

	        var formData = new FormData($(this)[0]);
	        $.ajax({
	            url: "<?php echo site_url('customer/customer/add_contact_process'); ?>",
	            type: "POST",
	            data: formData,
	            async: false,
	            success: function (msg) {
				        $('body,html').animate({ scrollTop: 0 }, 200);
                $("#pickup_ajax").html(msg); 
				        $("#picku_submitbutton").html('<button type="submit" class="btn btn-embossed btn-primary">Save</button>');
                $("form[name='add_contact']").find("input[type=text], input[type=checkbox], input[type=password], input[type=email]").val("");
              },
	            cache: false,
	            contentType: false,
	            processData: false
	        });

	        e.preventDefault();
	    });
	});
 </script>
 
 <!-- BEGIN PAGE CONTENT -->
<div class="page-content page-thin">
	<div class="header">
        <h2>Add Contact Person</h2>            
	</div>
	
  <div class="row">
    <div class="col-md-12">
      <div class="panel">
        <div class="panel-content">
				  <div id="pickup_ajax">                      	                       
          	<?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');}?>                     	
        	</div>
  				    
			    <form id="add_contact" name="add_contact" class="form-validation" accept-charset="utf-8" enctype="multipart/form-data" method="post">
  				  <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                	<label class="control-label">Name</label>
                	<div class="append-icon">
                  	<input type="text" name="contact_name" value="" class="form-control" id="contact_name">
                  	<i class="icon-pencil"></i>
                	</div>
                </div>
            	</div>
            	<div class="col-sm-12">
                <div class="form-group">
            		  <label class="control-label">Email</label>
                	<div class="append-icon">
                  	<input type="email" name="contact_email" value="" class="form-control" id="contact_email">
                  	<i class="icon-pencil"></i>
                	</div>
                </div>
            	</div>
              <div class="col-sm-12">
                <div class="form-group">
                  <label class="control-label">Postion</label>
                  <div class="append-icon">
                    <input type="text" name="contact_position" value="" class="form-control" id="contact_position">
                    <i class="icon-pencil"></i>
                  </div>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group">
                  <label class="control-label">Password</label>
                  <div class="append-icon">
                    <input type="password" name="pass1" id="password" class="form-control form-white password" placeholder="Password" required>
                    <i class="icon-lock"></i>
                  </div>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group">
                  <label class="control-label">Repeat Password</label>
                  <div class="append-icon">
                    <input type="password" name="pass2" id="password2" class="form-control form-white password2" placeholder="Repeat Password" required>
                    <i class="icon-lock"></i>
                  </div>
                </div>
              </div>
          	</div>
            <div class="text-left  m-t-20">
     					<div id="picku_submitbutton"><button type="submit" class="btn btn-embossed btn-primary">CREATE CONTACT</button></div>
            </div>
        	</form>
				</div>
    	</div>
    </div>
 	</div>
</div>   
<!-- END PAGE CONTENT -->