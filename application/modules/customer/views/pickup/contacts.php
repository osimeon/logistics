<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/1.12.0/semantic.min.css">
<script>
function delete_loc(contact_id){
  var result = confirm("are you sure you want to contact?");
  
  if(result){
    $.ajax({
      type: "GET",
      url: "<?php echo site_url('customer/customer/deletecontact' ); ?>/" + contact_id,
      success: function(msg){
  			if(msg == 'deleted'){
          $('#contact_id_' + contact_id).fadeOut('normal');
        }
      }
    });
  }
 }
 </script>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>
  $(document).ready(function() {
    $("form[name='add_contact']").submit(function(e) {
          //Name
          if(!$('#contact_name').val()){
            if ($("#contact_name").parent().next(".validation").length == 0){
                $("#contact_name").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter name</div>");
            }
            e.preventDefault();
            return;
          } 
          else {
              $("#contact_name").parent().next(".validation").remove();
          }

          //Email
          if(!$('#contact_email').val()){
            if ($("#contact_email").parent().next(".validation").length == 0){
                $("#contact_email").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter email address</div>");
            }
            e.preventDefault();
            return;
          } 
          else {
              $("#contact_email").parent().next(".validation").remove();
          }

          //Position
          if(!$('#contact_position').val()){
            if ($("#contact_position").parent().next(".validation").length == 0){
                $("#contact_position").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter position</div>");
            }
            e.preventDefault();
            return;
          } 
          else {
              $("#contact_position").parent().next(".validation").remove();
          }

          //Password
          if(!$('#password').val()){
            if ($("#password").parent().next(".validation").length == 0){
                $("#password").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter password</div>");
            }
            e.preventDefault();
            return;
          } 
          else {
              $("#password").parent().next(".validation").remove();
          }

          //Password 2
          if(!$('#password2').val()){
            if ($("#password2").parent().next(".validation").length == 0){
                $("#password2").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter confirmation password</div>");
            }
            e.preventDefault();
            return;
          } 
          else {
              $("#password2").parent().next(".validation").remove();
          }

          var formData = new FormData($(this)[0]);
          $.ajax({
              url: "<?php echo site_url('customer/customer/add_contact_process'); ?>",
              type: "POST",
              data: formData,
              async: false,
              success: function (msg) {
                $('#new_contact').animate({ scrollTop: 0 }, 200);
                $("#contact_ajax").html(msg); 
                $("#contact_ajax").delay(500).fadeOut();
                $("#add_contact").trigger("reset");
              },
              cache: false,
              contentType: false,
              processData: false
          });

          e.preventDefault();
      });
  });
 </script>

<!-- BEGIN PAGE CONTENT -->
<div class="page-content page-thin">
  <div class="header">
    <h2><strong>Locations Contacts</strong></h2>            
  </div>
  <div class="row">
 	  <div class="col-lg-12">
      <div class="panel">
        <div class="panel-header">
          <h3><i class="fa fa-table"></i> <strong>Manage </strong> Location Contacts</h3>
        </div>
        <div class="panel-content"> 
          <div class="m-b-20">
            <div class="btn-group">
              <!--<a href="<?php echo site_url('customer/customer/new_contact'); ?>" class="btn btn-sm btn-dark"><i class="fa fa-plus"></i> Add New</a>-->
              <a href="javascript:void(0)" class="btn btn-embossed btn-primary" data-toggle="modal" data-target="#new_contact"><i class="fa fa-plus"></i> Add New</a>
            </div>
            <div> 
              <?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');}?>         
            </div>
          </div>

          <div class="panel-content pagination2 table-responsive">
            <table class="table table-hover table-dynamic filter-between_date" id="location_contacts">
              <thead>
                <tr>                        
                  <th><?php echo $this->lang->line('name'); ?></th>
                  <th>EMAIL</th>   
                  <th>POSITION</th>
                  <th>OPTIONS</th>    
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
				</div>
      </div>
    </div>
  </div>
</div>  
<div class="modal fade" id="new_contact" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
        <h4 class="modal-title"><strong>Add Contact Person</strong></h4>
      </div>
      <div id="contact_ajax"> 
        <?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');}?>         
      </div>
      <form id="add_contact" name="add_contact" class="form-validation" accept-charset="utf-8" enctype="multipart/form-data" method="post">
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Name</label>
                <div class="append-icon">
                  <input type="text" name="contact_name" value="" class="form-control" id="contact_name">
                  <i class="icon-pencil"></i>
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Email</label>
                <div class="append-icon">
                  <input type="email" name="contact_email" value="" class="form-control" id="contact_email">
                  <i class="icon-pencil"></i>
                </div>
              </div>
            </div>
          </div>
          <div class="row">    
            <div class="col-sm-12">
              <div class="form-group">
                <label class="control-label">Postion</label>
                <div class="append-icon">
                  <input type="text" name="contact_position" value="" class="form-control" id="contact_position">
                  <i class="icon-pencil"></i>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Password</label>
                <div class="append-icon">
                  <input type="password" name="pass1" id="password" class="form-control form-white password" placeholder="Password" required>
                  <i class="icon-lock"></i>
                </div>
              </div>
            </div>

            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Repeat Password</label>
                <div class="append-icon">
                  <input type="password" name="pass2" id="password2" class="form-control form-white password2" placeholder="Repeat Password" required>
                  <i class="icon-lock"></i>
                </div>
              </div>
            </div>
          </div>
          <div class="text-left  m-t-20">
            <div id="picku_submitbutton"><button type="submit" class="btn btn-embossed btn-primary">CREATE CONTACT</button></div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>     