<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>
	$(document).ready(function() {
		$("form[name='add_pickup']").submit(function(e) {
          //Name
          if(!$('#pickup_name').val()){
            if ($("#pickup_name").parent().next(".validation").length == 0){
              $("#pickup_name").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter name</div>");
            }
            e.preventDefault();
            return;
          } 
          else {
            $("#pickup_name").parent().next(".validation").remove();
          }

          //Location address
          if(!$('#pickup_address').val()){
            if ($("#pickup_address").parent().next(".validation").length == 0){
              $("#pickup_address").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter address</div>");
            }
            e.preventDefault();
            return;
          } 
          else {
            $("#pickup_address").parent().next(".validation").remove();
          }

          //Contact person
          if(!$('#contact_person').val()){
            if ($("#contact_person").parent().next(".validation").length == 0){
              $("#contact_person").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please select contact person</div>");
            }
            e.preventDefault();
            return;
          } 
          else {
            $("#contact_person").parent().next(".validation").remove();
          }

          //Openning hours
          if(!$('#opening_hour').val()){
            if ($("#opening_hour").parent().next(".validation").length == 0){
              $("#opening_hour").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter openning hour</div>");
            }
            e.preventDefault();
            return;
          } 
          else {
            $("#opening_hour").parent().next(".validation").remove();
          }

          //Closing hours
          if(!$('#closing_hour').val()){
            if ($("#closing_hour").parent().next(".validation").length == 0){
              $("#closing_hour").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter closing hour</div>");
            }
            e.preventDefault();
            return;
          } 
          else {
            $("#closing_hour").parent().next(".validation").remove();
          }

	        var formData = new FormData($(this)[0]);
	        $.ajax({
	            url: "<?php echo site_url('customer/pickup/add_process'); ?>",
	            type: "POST",
	            data: formData,
	            async: false,
	            success: function (msg) {
				        $('body,html').animate({ scrollTop: 0 }, 200);
                $("#pickup_ajax").html(msg); 
				        $("#picku_submitbutton").html('<button type="submit" class="btn btn-embossed btn-primary">Save</button>');
                $("form[name='add_pickup']").find("input[type=text], input[type=checkbox]").val("");
              },
	            cache: false,
	            contentType: false,
	            processData: false
	        });

	        e.preventDefault();
	    });
	});
 </script>
 
 <!-- BEGIN PAGE CONTENT -->
<div class="page-content page-thin">
	<div class="header">
        <h2>Add Location</h2>            
	</div>
	
  <div class="row">
    <div class="col-md-12">
      <div class="panel">
        <div class="panel-content">
				  <div id="pickup_ajax">                      	                       
          	<?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');}?>                     	
        	</div>
  				    
			    <form id="add_pickup" name="add_pickup" class="form-validation" accept-charset="utf-8" enctype="multipart/form-data" method="post">
  				  <div class="row">
              <div class="col-md-7">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group">
                    	<label class="control-label">Location Name</label>
                    	<div class="append-icon">
                      	<input type="text" name="pickup_name" value="" class="form-control" id="pickup_name">
                      	<i class="icon-pencil"></i>
                    	</div>
                    </div>
                	</div>
                	<div class="col-sm-12">
                    <div class="form-group">
                		  <label class="control-label">Location Address</label>
                    	<div class="append-icon">
                      	<input type="text" name="pickup_address" value="" class="form-control" id="pickup_address" placeholder="Enter pickup address">
                      	<i class="icon-pencil"></i>
                    	</div>
                    </div>
                	</div>
                  <div class="col-sm-12">
                    <div class="form-group">
                      <label class="control-label">Contact Person</label>
                      <div class="append-icon">
                        <select name="contact_person" id="contact_person" class="form-control" data-search="true">
                          <option value=""></option>
                          <?php foreach($contacts->result() as $contact){ ?>
                          <option value="<?php echo $contact->id;?>"><?php echo $contact->name;?></option>
                          <?php }?> 
                        </select>
                      </div>
                    </div>
                  </div>
              	</div>
                <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label class="control-label">Openning &amp; Closing Hours</label>
                        <div class="row">
                          <div class="append-icon">
                            <div class="col-sm-6">
                              <input type="text" id="p_opening_hour" name="p_opening_hour" class="form-control">
                              <i class="glyphicon glyphicon-time"></i>
                            </div>
                            <div class="col-sm-6">
                              <input type="text" id="p_closing_hour" name="p_closing_hour" class="form-control">
                              <i class="glyphicon glyphicon-time"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
              <div class="col-md-5">
                <div class="row">
                  <br/>
                  <div class="form-group">
                    <div class="append-icon">
                      <div id="map-canvas" style="width:100%; height:300px;">
                        <script type="text/javascript">
                          var map = new google.maps.Map(document.getElementById('map-canvas'), {
                            zoom: 9,
                            center: new google.maps.LatLng(37.4419, -122.1419),
                            mapTypeId: google.maps.MapTypeId.ROADMAP,
                            mapTypeControl: false,
                            streetViewControl: false,
                            panControl: false,
                            scrollwheel: false,
                            scaleControl: true,
                            zoomControlOptions: {
                              position: google.maps.ControlPosition.LEFT_BOTTOM
                            }
                          });

                          function showKenya(){
                            var geocoder = new google.maps.Geocoder();

                            geocoder.geocode({'address': 'KE'}, function (results, status) {
                              var ne = results[0].geometry.viewport.getNorthEast();
                              var sw = results[0].geometry.viewport.getSouthWest();

                              map.fitBounds(results[0].geometry.viewport); 
                              map.setZoom(5);               
                            }); 
                          }
                          showKenya();
                        </script>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="text-left  m-t-20">
     					<div id="picku_submitbutton"><button type="submit" class="btn btn-embossed btn-primary">Create</button></div>
            </div>
        	</form>
				</div>
    	</div>
    </div>
 	</div>
</div>   
<!-- END PAGE CONTENT -->
<script type="text/javascript">
  //Pickup location
  var pickuploc = document.getElementById('pickup_address');
  var pickupsearch = new google.maps.places.SearchBox(pickuploc);

  var markers = [];

  pickupsearch.addListener('places_changed', function() {
      var places = pickupsearch.getPlaces();
      if(places.length == 0){
          return;
      }

      // Clear out the old markers.
      markers.forEach(function(marker) {
        marker.setMap(null);
      });
      
      markers = [];

      var bounds = new google.maps.LatLngBounds();
      places.forEach(function(place) {
        var icon = {
          url: place.icon,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(25, 25)
        };

        markers.push(new google.maps.Marker({
          map: map,
          icon: icon,
          title: place.name,
          position: place.geometry.location
        }));

        if(place.geometry.viewport){
          bounds.union(place.geometry.viewport);
        } 
        else {
          bounds.extend(place.geometry.location);
        }
      });

      map.fitBounds(bounds);
  });
</script>