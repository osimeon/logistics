<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="<?php echo base_url('public/logistics/js/jquery.timepicker.js'); ?>"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDWEyNzk8ze3TGjyVWsdhzjnB_H1PtzVF4&libraries=places,geometry&signed_in=true&v=3.exp"></script>

<script>
function delete_loc(loc_id){
  var result = confirm("are you sure you want to delete location?");
    
    if(result){
      $.ajax({
        type: "GET",
        url: "<?php echo site_url('customer/pickup/delete' ); ?>/" + loc_id,
        success: function(msg){
    			if(msg == 'deleted'){
            $('#pickup_id_' + loc_id).fadeOut('normal');
          }
        }
      });
    }
 }

$(document).ready(function() {
  $("#add_new_location").click(function(){
    $('#new_location').on('shown.bs.modal', function(e){
      console.log("dialog is now open");

      var mapOptions = {
        center: new google.maps.LatLng(-1.2833, 36.8167),
        zoom: 18,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        streetViewControl: false,
        panControl: false,
        scrollwheel: false,
        scaleControl: true,
        zoomControlOptions: {
          position: google.maps.ControlPosition.LEFT_BOTTOM
        }
      }
        
      var map = new google.maps.Map($("#map-canvas")[0], mapOptions);
      var pickuploc = document.getElementById('pickup_address');
      var pickupsearch = new google.maps.places.SearchBox(pickuploc);

      var markers = [];

      pickupsearch.addListener('places_changed', function(){
        var places = pickupsearch.getPlaces();

        if(places.length == 0){
          return;
        }

        // Clear out the old markers.
        markers.forEach(function(marker) {
          marker.setMap(null);
        });
        
        markers = [];

        var bounds = new google.maps.LatLngBounds();
        places.forEach(function(place) {
          var icon = {
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(25, 25)
          };

          markers.push(new google.maps.Marker({
            map: map,
            icon: icon,
            title: place.name,
            position: place.geometry.location
          }));

          if(place.geometry.viewport){
            bounds.union(place.geometry.viewport);
          } 
          else {
            bounds.extend(place.geometry.location);
          }
        });

        map.fitBounds(bounds);
      });
    });

    $('#new_location').modal('show').trigger('shown');
  });

  $("form[name='add_pickup']").submit(function(e) {
    //Name
    if(!$('#pickup_name').val()){
      if ($("#pickup_name").parent().next(".validation").length == 0){
        $("#pickup_name").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter name</div>");
      }
      e.preventDefault();
      return;
    } 
    else {
      $("#pickup_name").parent().next(".validation").remove();
    }

    //Location address
    if(!$('#pickup_address').val()){
      if ($("#pickup_address").parent().next(".validation").length == 0){
        $("#pickup_address").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter address</div>");
      }
      e.preventDefault();
      return;
    } 
    else {
      $("#pickup_address").parent().next(".validation").remove();
    }

    //Contact person
    if(!$('#contact_person').val()){
      if ($("#contact_person").parent().next(".validation").length == 0){
        $("#contact_person").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please select contact person</div>");
      }
      e.preventDefault();
      return;
    } 
    else {
      $("#contact_person").parent().next(".validation").remove();
    }

    //Openning hours
    if(!$('#s_opening_hour').val()){
      if ($("#s_opening_hour").parent().next(".validation").length == 0){
        $("#s_opening_hour").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter openning hour</div>");
      }
      e.preventDefault();
      return;
    } 
    else {
      $("#s_opening_hour").parent().next(".validation").remove();
    }

    //Closing hours
    if(!$('#s_closing_hour').val()){
      if ($("#s_closing_hour").parent().next(".validation").length == 0){
        $("#s_closing_hour").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter closing hour</div>");
      }
      e.preventDefault();
      return;
    } 
    else {
      $("#s_closing_hour").parent().next(".validation").remove();
    }

    var formData = new FormData($(this)[0]);
    $.ajax({
        url: "<?php echo site_url('customer/pickup/add_process'); ?>",
        type: "POST",
        data: formData,
        async: false,
        success: function (msg) {
          $('#new_location').animate({ scrollTop: 0 }, 200);
          $("#pickup_ajax").html(msg); 
          $("#pickup_ajax").delay(500).fadeOut();
          $("#add_pickup").trigger("reset");
        },
        cache: false,
        contentType: false,
        processData: false
    });

    e.preventDefault();
});
});

</script>

<style type="text/css">
  .tooltip{
    margin:8px;
    padding:8px;
    border:1px solid blue;
    background-color:yellow;
    position: absolute;
    z-index: 2;
  }
</style>

<!-- BEGIN PAGE CONTENT -->
<div class="flyout hidden">
  <p>Testing text</p>
</div>
<div class="page-content page-thin">
  <div class="header">
    <h2><strong>Locations</strong></h2>            
  </div>
  <div class="row">
 	  <div class="col-lg-12">
      <div class="panel">
        <div class="panel-header">
          <h3><i class="fa fa-table"></i> <strong>Manage </strong> Locations</h3>
        </div>
        <div class="panel-content"> 
          <div class="m-b-20">
            <div class="btn-group">
              <!--<a href="<?php echo site_url('customer/pickup/add'); ?>" class="btn btn-sm btn-dark"><i class="fa fa-plus"></i> Add New</a>-->
              <a href="javascript:void(0)" class="btn btn-embossed btn-primary" id="add_new_location"><i class="fa fa-plus"></i> New Location</a>
            </div>
            <div> 
              <?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');}?>         
            </div>
          </div>

          <div class="panel-content pagination2 table-responsive">
            <table class="table table-hover table-dynamic filter-between_date" id="pickup_list">
              <thead>
                <tr>                        
                  <th><?php echo $this->lang->line('name'); ?></th>
                  <th>OPENING HOURS</th>
                  <th>CLOSING HOURS</th>
                  <th>CONTACT PERSON</th>
                  <th>LOCATION ADDRESS</th>
                  <th>OPTIONS</th>      
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
				</div>
      </div>
    </div>
  </div>
</div>  

<div class="modal" id="new_location" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 1;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
        <h4 class="modal-title"><strong>New Location</strong></h4>
      </div>
      <div id="pickup_ajax" style="color:red;margin-left:15px;"> 
        <?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');}?>         
      </div>

      <form id="add_pickup" name="add_pickup" class="form-validation" accept-charset="utf-8" enctype="multipart/form-data" method="post">
        <div class="modal-body">
          <div class="row">
              <div class="col-md-5">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group">
                      <label class="control-label">Location Name</label>
                      <div class="append-icon">
                        <input type="text" name="pickup_name" value="" class="form-control" id="pickup_name">
                        <i class="icon-pencil"></i>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group">
                      <label class="control-label">Location Address</label>
                      <div class="append-icon">
                        <input type="text" name="pickup_address" value="" class="form-control" id="pickup_address" placeholder="Enter pickup address">
                        <i class="icon-pencil"></i>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group">
                      <label class="control-label">Contact Person</label>
                      <div class="append-icon">
                        <select name="contact_person" id="contact_person" class="form-control" data-search="true">
                          <option value=""></option>
                          <?php foreach($contacts->result() as $contact){ ?>
                          <option value="<?php echo $contact->id;?>"><?php echo $contact->name;?></option>
                          <?php }?> 
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label class="control-label">Openning &amp; Closing Hours</label>
                        <div class="row">
                          <div class="append-icon">
                            <div class="col-sm-6">
                              <input type="text" id="s_opening_hour" name="opening_hour" class="form-control">
                              <i class="glyphicon glyphicon-time"></i>
                            </div>
                            <div class="col-sm-6">
                              <input type="text" id="s_closing_hour" name="closing_hour" class="form-control">
                              <i class="glyphicon glyphicon-time"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
              <div class="col-md-7">
                <div class="row">
                  <br/>
                  <div class="form-group">
                    <div class="append-icon">
                      <div id="map-canvas" style="width:95%; height:300px;">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
        <div id="pickup_submitbutton" class="modal-footer text-left"><button type="submit" class="btn btn-primary btn-embossed bnt-square">Add Location</button></div>
      </form>
    </div>
  </div>
</div> 

<script type="text/javascript">
  jQuery(document).ready(function($) {  
      $('#s_opening_hour').timepicker({'step': 15});
      $('#s_closing_hour').timepicker({'step': 15});
  }); 
</script>
