<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo config('site_name'); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta content="" name="description" />
        <meta content="" name="author" />
        <link rel="shortcut icon" href="<?php echo base_url(); ?>/public/assets/global/images/favicon.png">
        <link href="<?php echo base_url(); ?>/public/assets/global/css/style.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>/public/assets/global/css/ui.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>/public/assets/global/plugins/bootstrap-loading/lada.min.css" rel="stylesheet">
        
         <script>
            function create_user(){
                $.ajax({
                    type: "POST",
                    url: "<?php echo site_url('customer/create_company'); ?>",
                    data: $("#createform").serialize(),
                    beforeSend : function(msg){ $("#submitbutton").html('<img src="<?php echo base_url('public/images/loading.gif'); ?>" />'); },
                    success: function(msg){
                        $('body,html').animate({ scrollTop: 0 }, 200);
                        if(msg.substring(1,7) != 'script'){
                            $("#ajax").html(msg); 
                            $("#submitbutton").html('<button type="submit" id="submit-form" class="btn btn-lg btn-danger btn-block ladda-button" data-style="expand-left" onClick="create_user()"><?php echo $this->lang->line('sign_up_title'); ?></button>');
                            $("#createform")[0].reset();
                        }
                        else{ 
                            $("#ajax").html(msg); 
                        }
                    }
                });
            }
        </script>
    </head>
    <body class="account separate-inputs no-terms no-social" data-page="signup">
        <!-- BEGIN LOGIN BOX -->
        <div class="container" id="login-block">
            <div class="row">
                <div class="col-sm-6 col-md-6 col-md-offset-3">
                    <div class="account-wall" style="background-color: rgba(255, 255, 255, 0);">
                        <i class="user-img icons-faces-users-03" style="opacity: 1;"></i>
                        <form class="form-signup" id="createform" role="form">
                            <div id="ajax"><?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');}?></div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="append-icon">
                                        <input type="text" name="company_name" id="company_name" class="form-control form-white company_name" placeholder="Company Name" autofocus="" required>
                                        <i class="icon-user"></i>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="append-icon">
                                        <input type="email" name="company_email" id="company_email" class="form-control form-white company_email" placeholder="Company Email" required>
                                        <i class="icon-user"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="append-icon">
                                <textarea name="company_address" rows="4" class="form-control" placeholder="Company Address"></textarea>
                            </div>
                            <br/>
                            <div class="append-icon">
                                <input type="text" name="company_phone" value="" class="form-control" placeholder="Company Phone Number" required>
                                <i class="icon-screen-smartphone"></i>
                            </div>
                            <div class="append-icon">
                                <input type="password" name="pass1" id="password" class="form-control form-white password" placeholder="Password" required>
                                <i class="icon-lock"></i>
                            </div>
                            <div class="append-icon m-b-20">
                                <input type="password" name="pass2" id="password2" class="form-control form-white password2" placeholder="Repeat Password" required>
                                <i class="icon-lock"></i>
                            </div>                          
                            <div id="submitbutton"><button type="submit" id="submit-form" class="btn btn-lg btn-danger btn-block ladda-button" data-style="expand-left" onClick="create_user()"><?php echo $this->lang->line('sign_up_title'); ?></button></div>
                            <div class="clearfix">
                                <p class="pull-right m-t-20"><a href="<?php echo site_url('customer/login'); ?>"><?php echo $this->lang->line('sign_in_here'); ?></a></p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <p class="account-copyright" style="position: relative; margin-top: 40px;">
               <span>Copyright © <?php echo date('Y');?> </span><span><?php echo config('site_name'); ?></span>.<span>All rights reserved.</span>
            </p>
            
        </div>
        <script src="<?php echo base_url(); ?>/public/assets/global/plugins/jquery/jquery-1.11.1.min.js"></script>
        <script src="<?php echo base_url(); ?>/public/assets/global/plugins/jquery/jquery-migrate-1.2.1.min.js"></script>
        <script src="<?php echo base_url(); ?>/public/assets/global/plugins/gsap/main-gsap.min.js"></script>
        <script src="<?php echo base_url(); ?>/public/assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>/public/assets/global/plugins/backstretch/backstretch.min.js"></script>
        <script src="<?php echo base_url(); ?>/public/assets/global/plugins/bootstrap-loading/lada.min.js"></script>
        <script src="<?php echo base_url(); ?>/public/assets/global/js/pages/login-v1.js"></script>
    </body>
</html>