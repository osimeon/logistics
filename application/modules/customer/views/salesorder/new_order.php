<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDWEyNzk8ze3TGjyVWsdhzjnB_H1PtzVF4&libraries=places,geometry&signed_in=true&v=3.exp"></script>

<script type="text/javascript">
  jQuery(window).bind('beforeunload', function(){
    return '';
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $("#m_review_order").click(function(){
      $('#review_order').on('shown.bs.modal', function(e){
        console.log("review order is open");

        var mapOptions = {
          center: new google.maps.LatLng(-1.2833, 36.8167),
          zoom: 18,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          mapTypeControl: false,
          streetViewControl: false,
          panControl: false,
          scrollwheel: false,
          scaleControl: true,
          zoomControlOptions: {
            position: google.maps.ControlPosition.LEFT_BOTTOM
          }
        }

        var map = new google.maps.Map($("#trip_map")[0], mapOptions);

        function calculateRoute(from, to){
          console.log("Getting routes: source => " + from + ", destination => " + to);
          var directionsService = new google.maps.DirectionsService();

          var directionsRequest = {
            origin: from,
            destination: to,
            travelMode: google.maps.DirectionsTravelMode.DRIVING,
            unitSystem: google.maps.UnitSystem.METRIC
          };

          directionsService.route(directionsRequest, function(response, status){
            if(status == google.maps.DirectionsStatus.OK){
              new google.maps.DirectionsRenderer({
                map: map,
                directions: response
              });
            }
              //$("#error").append("Unable to retrieve your route<br />");
            }
          );
        }

        calculateRoute($('#pickup_location option:selected').text(), $('#delivery_location option:selected').text());
      });

      $('#review_order').modal('show').trigger('shown');
    });

    // add new location
    $("#add_new_location").click(function () {
      //$('#new_location').modal('show');

      $('#new_location').on('shown.bs.modal', function(e){
        console.log("dialog is now open");

        var mapOptions = {
          center: new google.maps.LatLng(-1.2833, 36.8167),
          zoom: 18,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          mapTypeControl: false,
          streetViewControl: false,
          panControl: false,
          scrollwheel: false,
          scaleControl: true,
          zoomControlOptions: {
            position: google.maps.ControlPosition.LEFT_BOTTOM
          }
        }
        
        var map = new google.maps.Map($("#map-canvas")[0], mapOptions);
        var pickuploc = document.getElementById('pickup_address');
        var pickupsearch = new google.maps.places.SearchBox(pickuploc);

        var markers = [];

        pickupsearch.addListener('places_changed', function(){
          var places = pickupsearch.getPlaces();

          if(places.length == 0){
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          
          markers = [];

          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            if(place.geometry.viewport){
              bounds.union(place.geometry.viewport);
            } 
            else {
              bounds.extend(place.geometry.location);
            }
          });

          map.fitBounds(bounds);
        });
      });

      $('#new_location').modal('show').trigger('shown');
    });
  });
</script>
<script>
  // add new stop
  $(document).ready(function() {
    $("form[name='new_stop']").submit(function(e) {
      //Airway bill no
      if(!$('#airway_bill_no_order_form').val()){
        if ($("#airway_bill_no_order_form").parent().next(".validation").length == 0){
          $("#airway_bill_no_order_form").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter Airwaybill/Reference#</div>");
            $('html, body').animate({scrollTop: $("#airway_bill_no_order_form").offset().top }, 1000);
        }
        e.preventDefault();
        return;
      } 
      else {
        $("#airway_bill_no").parent().next(".validation").remove();
      }

      //Contact person
      if(!$('#stop_contact_person').val()){
        if ($("#stop_contact_person").parent().next(".validation").length == 0){
          $("#stop_contact_person").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please select contact person</div>");
          $('html, body').animate({scrollTop: $("#stop_contact_person").offset().top }, 1000);
        }
        e.preventDefault();
        return;
      } 
      else {
        $("#stop_contact_person").parent().next(".validation").remove();
      }

      //Pickup location
      if(!$('#pickup_location').val()){
        if ($("#pickup_location").parent().next(".validation").length == 0){
          $("#pickup_location").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please select pickup location</div>");
          $('html, body').animate({scrollTop: $("#pickup_location").offset().top }, 1000);
        }
        e.preventDefault();
        return;
      } 
      else {
        $("#pickup_location").parent().next(".validation").remove();
      }

      //Pickup date
      if(!$('#pickup_date').val()){
        if ($("#pickup_date").parent().next(".validation").length == 0){
          $("#pickup_date").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please select pickup date</div>");
          $('html, body').animate({scrollTop: $("#pickup_date").offset().top }, 1000);
        }
        e.preventDefault();
        return;
      } 
      else {
        $("#pickup_date").parent().next(".validation").remove();
      }

      //Pickup time
      if(!$('#pickup_time').val()){
        if ($("#pickup_time").parent().next(".validation").length == 0){
          $("#pickup_time").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please select pickup time</div>");
          $('html, body').animate({scrollTop: $("#pickup_time").offset().top }, 1000);
        }
        e.preventDefault();
        return;
      } 
      else {
        $("#pickup_time").parent().next(".validation").remove();
      }

      //Delivery location
      if(!$('#delivery_location').val()){
        if ($("#delivery_location").parent().next(".validation").length == 0){
          $("#delivery_location").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter delivery location</div>");
          $('html, body').animate({scrollTop: $("#delivery_location").offset().top }, 1000);
        }
        e.preventDefault();
        return;
      } 
      else {
        $("#delivery_location").parent().next(".validation").remove();
      }

      //Delivery Date
      if(!$('#delivery_date').val()){
        if ($("#delivery_date").parent().next(".validation").length == 0){
          $("#delivery_date").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please select delivery date</div>");
          $('html, body').animate({scrollTop: $("#delivery_date").offset().top }, 1000);
        }
        e.preventDefault();
        return;
      } 
      else {
        $("#delivery_date").parent().next(".validation").remove();
      }

      //Delivery time
      if(!$('#delivery_time').val()){
        if ($("#delivery_time").parent().next(".validation").length == 0){
          $("#delivery_time").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please select delivery time</div>");
          $('html, body').animate({scrollTop: $("#delivery_time").offset().top }, 1000);
        }
        e.preventDefault();
        return;
      } 
      else {
        $("#delivery_time").parent().next(".validation").remove();
      }

      var str_pickup = new Date($('#pickup_date').val() + " " + $('#pickup_time').val());
      var str_delivery = new Date($('#delivery_date').val() + " " + $('#delivery_time').val());

      if(str_pickup.getTime() > str_delivery.getTime()){
        $("#delivery_date").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Pickup is greater than delivery time. Please correct!</div>");
          $('html, body').animate({scrollTop: $("#delivery_date").offset().top }, 1000);
        e.preventDefault();
        return;
      }

      //Item Quantity
      if(!$('#item_quantity').val()){
        if ($("#item_quantity").parent().next(".validation").length == 0){
          $("#item_quantity").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter Item quantity</div>");
          $('html, body').animate({scrollTop: $("#item_quantity").offset().top }, 1000);
        }
        e.preventDefault();
        return;
      } 
      else {
        $("#item_quantity").parent().next(".validation").remove();
      }

      //Total Weight
      if(!$('#total_weight').val()){
        if ($("#total_weight").parent().next(".validation").length == 0){
          $("#total_weight").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter total weight</div>");
          $('html, body').animate({scrollTop: $("#total_weight").offset().top }, 1000);
        }
        e.preventDefault();
        return;
      } 
      else {
        $("#total_weight").parent().next(".validation").remove();
      }

      //Package type
      if(!$('#packaging_type').val()){
        if ($("#packaging_type").parent().next(".validation").length == 0){
          $("#packaging_type").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please select Packaging</div>");
          $('html, body').animate({scrollTop: $("#packaging_type").offset().top }, 1000);
        }
        e.preventDefault();
        return;
      } 
      else {
        $("#packaging_type").parent().next(".validation").remove();
      }

      console.log('past validation');

      var formData = new FormData($(this)[0]);
      $.ajax({
        url: "<?php echo site_url('customer/salesorder/add_process'); ?>",
        type: "POST",
        data: formData,
        async: false,
        beforeSend : function(msg){ $("#makeordersubmitbutton").html('<img src="<?php echo base_url('public/images/loading.gif'); ?>" />'); },
        success: function (msg) {
          console.log(msg);

          $("#make_order_ajax").html(msg); 
          $("#make_order_ajax").delay(500).fadeOut();
          $("#new_stop").trigger("reset");

          $("#stop_contact_person").select2('data', null);
          $("#pickup_location").select2('data', null);
          $("#delivery_location").select2('data', null);
          $("#packaging_type").select2('data', null);

          $("#makeordersubmitbutton").html('<button type="submit" class="btn btn-embossed btn-primary" id="m_add_item" onclick="place_order();">Next</button>');

          // clear form elements
          $("#pickup_date").val("");
          $("#pickup_time").val("");
          $("#delivery_date").val("");
          $("#delivery_time").val("");
          $("#item_quantity").val("");
          $("#total_weight").val("");
          $("#comments").val("");

          // calculate progress
          calculateProgress();
        },

        cache: false,
        contentType: false,
        processData: false
      }); 

      e.preventDefault();
    });
  });
  
  // adding new order
	$(document).ready(function() {
		$("form[name='new_order']").submit(function(e) {
      //Airway bill no
      if(!$('#airway_bill_no').val()){
        if ($("#airway_bill_no").parent().next(".validation").length == 0){
          $("#airway_bill_no").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter Airwaybill/Reference#</div>");
            $("#takingorder").css({display: "none"});
            $('html, body').animate({scrollTop: $("#airway_bill_no").offset().top }, 1000);
        }
        e.preventDefault();
        return;
      } 
      else {
        $("#airway_bill_no").parent().next(".validation").remove();
      }

      //notes
      if(!$('#comments').val()){
        if ($("#comments").parent().next(".validation").length == 0){
          $("#comments").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter description</div>");
          $("#takingorder").css({display: "none"});
          $('html, body').animate({scrollTop: $("#comments").offset().top }, 1000);
        }
        e.preventDefault();
        return;
      } 
      else {
        $("#comments").parent().next(".validation").remove();
      }

      var formData = new FormData($(this)[0]);
        $.ajax({
          url: "<?php echo site_url('customer/salesorder/add_order_process'); ?>",
          type: "POST",
          data: formData,
          async: false,
          beforeSend : function(msg){ $("#order_submit").html('<img src="<?php echo base_url('public/images/loading.gif'); ?>" />'); },
          success: function (msg) {
            console.log(msg);

            $('body,html').animate({ scrollTop: 0 }, 200);
          	$("#order_ajax").html(msg); 

            $("#order_ajax").delay(500).fadeOut();
            $("#new_order").trigger("reset");

            $("#order_submit").html('<button type="submit" class="btn btn-embossed btn-primary pull-right">Save</button>');

            window.location.href = "<?php echo site_url('customer/salesorder/all'); ?>";
        	},

          cache: false,
          contentType: false,
          processData: false
        }); 

	        e.preventDefault();
	    });
	});

  // adding new location
  $(document).ready(function() {
    $("form[name='add_new_location']").submit(function(e) {
        var formData = new FormData($(this)[0]);

        $.ajax({
          url: "<?php echo site_url('customer/pickup/add_process_ajax'); ?>",
          type: "POST",
          data: formData,
          dataType:'json', 
          async: false,
          success: function (data) {
            $('body,html').animate({ scrollTop: 0 }, 200);

                console.log('Location Details:' + data);

                //Add to dropdown
                $("#pickup_location option:first").after($('<option>', {
                  value: data.pi_id,
                  text: data.pi_name
                }));


                $("#delivery_location option:first").after($('<option>', {
                  value: data.pi_id,
                  text: data.pi_name
                }));
        
                if(data.pi_id){
                  $("#location_ajax").html("New location created succesful");
                } 

                $("#pickup_submitbutton").html('<button type="submit" class="btn btn-embossed btn-primary">Add Location</button>');
                $("form[name='add_new_location']").find("input[type=text], input[type=checkbox]").val("");
          },
          cache: false,
          contentType: false,
          processData: false
      });

      e.preventDefault();
    });
  });

  // adding new package
  $(document).ready(function() {
    $("form[name='add_new_package']").submit(function(e) {
        var formData = new FormData($(this)[0]);

        $.ajax({
          url: "<?php echo site_url('customer/package/add_process_ajax'); ?>",
          type: "POST",
          data: formData,
          dataType:'json', 
          async: false,
          success: function (data) {
            $('body, html').animate({ scrollTop: 0 }, 200);  

            console.log('Package Details:' + data);         
            
            //Add to dropdown
            $("#packaging_type option:first").after($('<option>', {
              value: data.pa_id,
              text: data.pa_name
            }));

            /*$("#delivery_location option:first").after($('<option>', {
              value: data.pa_id,
              text: data.pa_name
            }));*/
    
            if(data.pa_id){
              $("#package_ajax").html("New package created succesful");
            } 
          
            $("#new_package_submitbutton").html('<button type="submit" class="btn btn-embossed btn-primary">New Package</button>');
            $("form[name='add_new_package']").find("input[type=text], textarea").val("");
          },
          cache: false,
          contentType: false,
          processData: false
      });

      e.preventDefault();
    });
  });

  // ading new staff
  $(document).ready(function() {
    $("form[name='add_staff']").submit(function(e) {
      //Firstname
      if(!$('#first_name').val()){
        if ($("#first_name").parent().next(".validation").length == 0){
          $("#first_name").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter first name</div>");
        }
        e.preventDefault();
        return;
      } 
      else {
        $("#first_name").parent().next(".validation").remove();
      }

      //Lastname
      if(!$('#last_name').val()){
        if ($("#last_name").parent().next(".validation").length == 0){
          $("#last_name").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter last name</div>");
        }
        e.preventDefault();
        return;
      } 
      else {
        $("#last_name").parent().next(".validation").remove();
      }

      //Phone number
      if(!$('#phone_number').val()){
        if ($("#phone_number").parent().next(".validation").length == 0){
          $("#phone_number").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter phone number</div>");
        }
        e.preventDefault();
        return;
      } 
      else {
        $("#phone_number").parent().next(".validation").remove();
      }

      //Email address
      if(!$('#email').val()){
        if ($("#email").parent().next(".validation").length == 0){
          $("#email").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter email address</div>");
        }
        e.preventDefault();
        return;
      } 
      else {
        $("#email").parent().next(".validation").remove();
      }

      //Job title
      if(!$('#jobtitle').val()){
        if ($("#jobtitle").parent().next(".validation").length == 0){
          $("#jobtitle").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter job title</div>");
        }
        e.preventDefault();
        return;
      } 
      else {
        $("#jobtitle").parent().next(".validation").remove();
      }

      //Password 1
      if(!$('#password1').val()){
        if ($("#password1").parent().next(".validation").length == 0){
          $("#password1").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter password</div>");
        }
        e.preventDefault();
        return;
      } 
      else {
        $("#password1").parent().next(".validation").remove();
      }

      //Password 2
      if(!$('#password2').val()){
        if ($("#password2").parent().next(".validation").length == 0){
          $("#password2").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter confirmation password</div>");
        }
        e.preventDefault();
        return;
      } 
      else {
        $("#password2").parent().next(".validation").remove();
      }

      var formData = new FormData($(this)[0]);
      $.ajax({
          url: "<?php echo site_url('customer/staff/add_process_ajax'); ?>",
          type: "POST",
          data: formData,
          async: false,
          beforeSend : function(msg){ $("#submitbutton").html('<img src="<?php echo base_url('public/images/loading.gif'); ?>" />'); },
          success: function (data) {
            $('body, html').animate({ scrollTop: 0 }, 200);  
            console.log('Staff Details:' + data);         
            
            //Add to dropdown
            if(data.si_id){
              $("#contact_person option:first").after($('<option>', {
                value: data.si_id,
                text: data.si_name
              }));
            }

            console.log(data.si_message);
    
            $("#staff_ajax").html(data.si_message);
          
            $("#submitbutton").html('<button type="submit" class="btn btn-embossed btn-primary">Save</button>');
            $("form[name='add_staff']").find("input[type=text], input[type=checkbox], input[type=password], input[type=email]").val("");
          },
          cache: false,
          contentType: false,
          processData: false
      });

      e.preventDefault();
    });
  });
  
  // trigger new order form submit
  $(document).ready(function() {
    function add_item(){
      $('#new_order').trigger('submit');
    }
  });

  // triggering new stop form submit
  function place_order(){
    $('#new_stop').trigger('submit');
  }

  // remove airway bill item
  function delete_airway_item(airway){
    var result = confirm("are you sure you want to delete stop?");
    
    if(result){
      $.ajax({
          type: "GET",
          url: "<?php echo site_url('customer/salesorder/delete_airway_item' ); ?>/" + airway,
          success: function(msg){
            // $("#driver_ajax").html(msg);
            // do nothing
            console.log(msg);
          }
      });
    }
  }
 </script>

<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
	<div class="header">
    <h2><strong>New Order</strong> - <?php echo $airwaybill; ?></h2>  
    <div class="breadcrumb-wrapper">
    </div>            
	</div>

  <div class="row">
    <div class="col-md-12">
      <div class="panel"> 
        <div class="panel-content">
          <table class="table" id="airwaybill_items">
            <thead>
              <tr style="font-size: 12px;">
                <td>STOP</td>
                <td>CONTACT PERSON</td>
                <td>PICKUP</td>
                <td>DELIVERY</td>
                <td>QUANTITY</td>
                <td>PACKAGE</td>
                <td></td>
              </tr>
            </thead>
            <tbody id="InputsWrapper">

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
   	
 	<div class="row">
 		<div class="col-md-12">
  		<div class="panel">
     		<div class="panel-content">
					<div id="order_ajax"> 
        		<?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');}?>         
        	</div>
	            
          <form id="new_order" name="new_order" class="form-validation" accept-charset="utf-8" enctype="multipart/form-data" method="post">
            <div class="row">
              <div class="col-sm-12">
                <a href="#" class="#" data-toggle="modal" data-target="#new_staff"><i class="fa fa-plus"></i> New Contact...</a>
                &nbsp;
                &nbsp;
                &nbsp;
                <a href="#" class="#" id="add_new_location"><i class="fa fa-plus"></i> New Location...</a>
                &nbsp;
                &nbsp;
                &nbsp;
                <a href="#" class="#" data-toggle="modal" data-target="#new_package"><i class="fa fa-plus"></i> New Package...</a>
              </div>
            </div>
            <hr/>
            <div class="row">
  					  <div class="col-sm-6">
                <div class="form-group">
              		<label class="control-label">Awaybill/Reference#</label>
                	<div class="append-icon">
                    <input type="hidden" name="airway_bill_no" id="airway_bill_no" value="<?php echo $airwaybill; ?>" class="form-control">
                  	<input type="text" name="r_airway_bill_no" id="r_airway_bill_no" value="<?php echo $airwaybill; ?>" class="form-control" disabled>
              		</div>
                </div>
            	</div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label class="control-label">Upload Documents</label>
                  <div class="append-icon">
                    <div class="file">
                      <div class="option-group">
                        <span class="file-button btn-primary">Choose File</span>
                        <input type="file" class="custom-file" name="order_attachments[]" id="order_attachments" onchange="document.getElementById('uploader_attach').value = this.files.length + ' files selected';" multiple>
                        <input type="text" class="form-control" id="uploader_attach" placeholder="no file selected" readonly="">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
			    	<div class="row">
			    		<div class="col-sm-12">
		    				<div class="form-group">
              		<label class="control-label">Notes</label>
              		<div class="append-icon">
                		<textarea name="comments" rows="4" class="form-control" id="comments"></textarea> 
              		</div>
              	</div>
			    		</div>
			    	</div>       
  				  <div class="text-left  m-t-20">
              <div class="row">
                <div class="col-sm-6">
                  <a href="#" class="btn btn-embossed btn-primary" data-toggle="modal" data-target="#make_order_form"><i class="fa fa-plus"></i> Stop</a>
                  <a href="#" class="btn btn-embossed btn-primary" id="m_review_order">Review Order</a>
                </div>
                <div class="col-sm-6">
                  <div id="order_submit">
                    <button type="submit" class="btn btn-embossed btn-primary pull-right">Save</button>
                  </div>
                </div>
              </div>
            </div>
        	</form>             
      	</div>
    	</div>
    </div>
 	</div>
</div>   
<!-- END PAGE CONTENT -->

<!-- New Pickup Location Modal -->
<div class="modal" id="new_location" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 1;">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
          <h4 class="modal-title"><strong>New Location</strong></h4>
        </div>
        <div id="location_ajax" style="color:red;margin-left:15px;"> 
          <?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');}?>         
        </div>
        <form id="add_new_location" name="add_new_location" class="form-validation" accept-charset="utf-8" method="post">
          <div class="modal-body">
            <div class="row">
              <div class="col-md-6">
                <div class="col-sm-12">
                  <div class="form-group">
                    <label class="control-label">Location Name</label>
                    <div class="append-icon">
                      <input type="text" name="pickup_name" value="" class="form-control">
                      <i class="icon-pencil"></i>
                    </div>
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="form-group">
                    <label class="control-label">Location Address</label>
                    <div class="append-icon">
                      <input type="text" name="pickup_address" value="" class="form-control" id="pickup_address" placeholder="Enter pickup address">
                      <i class="icon-pencil"></i>
                    </div>
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="form-group">
                    <label class="control-label">Contact Person</label>
                    <div class="append-icon">
                      <select name="contact_person" id="contact_person" class="form-control" data-search="true">
                        <option value=""></option>
                        <?php foreach($contacts->result() as $contact){ ?>
                        <option value="<?php echo $contact->id;?>"><?php echo $contact->name;?></option>
                        <?php }?> 
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="form-group">
                    <label class="control-label">Openning &amp; Closing Hours</label>
                    <div class="row">
                      <div class="append-icon">
                        <div class="col-sm-6">
                          <input type="text" id="opening_hour" name="opening_hour" class="form-control">
                          <!--<i class="glyphicon glyphicon-time"></i>-->
                        </div>
                        <div class="col-sm-6">
                          <input type="text" id="closing_hour" name="closing_hour" class="form-control">
                          <!--<i class="glyphicon glyphicon-time"></i>-->
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
              <div class="col-md-6">
                <div class="row">
                  <br/>
                  <div class="form-group">
                    <div class="append-icon">
                      <div id="map-canvas" style="width:95%; height:300px;" style="z-index: 20;">

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div id="pickup_submitbutton" class="modal-footer text-left"><button type="submit" class="btn btn-primary btn-embossed bnt-square">Add Location</button></div>
        </form>
    </div>
  </div>
</div>

<div id="successdialog" title="Order Submitted For Review" class="modal fade" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
        <h4 class="modal-title"><strong>Order Submitted For Review</strong></h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <p class="alert alert-success">Your order has been sent to a verified list of transporters so you can get the best value and quality service. Review order updates under My orders pending. You will also be notified by email & SMS when a transporter places a bid</p>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- new Package modal -->
<div class="modal fade" id="new_package" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
        <h4 class="modal-title"><strong>New Package</strong></h4>
      </div>
      <div id="package_ajax" style="color:red;margin-left:15px;"> 
          <?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');}?>         
      </div>
      <form id="add_new_package" name="add_new_package" class="form-validation" accept-charset="utf-8" method="post">
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                  <label class="control-label">Name</label>
                  <div class="append-icon">
                      <input type="text" name="package_name" value="" class="form-control">
                  </div>
                </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group">
                <label class="control-label">Description</label>
                <div class="append-icon">
                  <textarea name="package_desc" rows="4" class="form-control"></textarea>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div id="new_package_submitbutton" class="modal-footer text-left"><button type="submit" class="btn btn-primary btn-embossed bnt-square">Add Package Type</button></div>
      </form>
    </div>
  </div>
</div>

<!-- review Modal -->
<div class="modal fade or_review" id="review_order" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
        <h4 class="modal-title"><strong>SHIPMENT REQUEST DELIVERY SUMMARY SHEET</strong></h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
            <table width="100%" style="border-collapse:separate; border-spacing:0 1em;">
              <tr>
                <td><h5>ORDER REFERENCE#: <label class="control-label" id="pr_airway_bill_no"></label></h5></td>
                <td>
                  <h5><span class="btn pull-right" style="background-color: #ffc000; color:#fff;">ORDER STATUS: PENDING</span></h5>
                </td>
              </tr>
              <tr>
                <td>
                  <img class="pull-left" alt="Barcode" src="<?php echo site_url('customer/salesorder/generate_bar_code').'/'.$airwaybill; ?>" />
                </td>
                <td>
                  <img class="pull-right" align="Company logo" src="<?php echo base_url('uploads/site/logo.png'); ?>" />
                </td>
              </tr>
              <tr>
                <td>
                  ORDER TIME: <label class="control-label" id="order_time"><?php echo date('Y-m-d H:i:s'); ?></label>
                  <br/>
                  ORDERED BY: <strong><?php echo userdata_customer('name');?></strong>
                </td>
              </tr>
              <tr>
                <td colspan="2">
                  <center><h4 class="text-center"><strong>SHIPMENT REQUEST DETAILS</strong></h4></center>
                </td>
              </tr>
              <tr>
                <td colspan="2">
                  <div id="trip_map" style="width:100%; height:300px;" style="z-index: 20;">
                  </div>
                </td>
              </tr>
              <tr>
                <td><label class="control-label" id="ag_notes"></label></td>
              </tr>
            </table>
          </div>
        </div>
        <div class="row">
          <br/>
          <div class="col-sm-12">
            <input type="checkbox" name="agree" value="1" checked data-checkbox="icheckbox_square-blue"/>
            I have read and agree with all the conditions set forth here
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-embossed" data-dismiss="modal"><i class="fa fa-times" ></i>  Ok</button>
      </div>
    </div>
  </div>
</div>

<!-- new staff -->
<div class="modal fade" id="new_staff" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
        <h4 class="modal-title"><strong>New Staff Contact</strong></h4>
      </div>
      <div id="staff_ajax"> 
        <?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');}?>         
      </div>
      <form id="add_staff" name="add_staff" class="form-validation" accept-charset="utf-8" enctype="multipart/form-data" method="post">
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">First Name</label>
                <div class="append-icon">
                  <input type="text" name="first_name" value="" class="form-control" id="first_name">
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Last Name</label>
                <div class="append-icon">
                  <input type="text" name="last_name" value="" class="form-control" id="last_name">
                </div>
              </div>
            </div>
          </div>
          <div class="row">                                
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Phone Number</label>
                <div class="append-icon">
                  <input type="text" name="phone_number" value="" class="form-control" id="phone_number">
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Email Address</label>
                <div class="append-icon">
                  <input type="email" name="email" value="" class="form-control" id="email">
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                  <label class="control-label">Job Title</label>
                  <div class="append-icon">
                    <input type="text" name="jobtitle" value="" class="form-control" id="jobtitle">
                  </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Active</label>
                <div class="append-icon">
                  <?php $status = ($staff->status == 1) ? 'checked' : ''; ?>
                  <input type="checkbox" name="status" value="1" <?php echo $status; ?> data-checkbox="icheckbox_square-blue"/> 
                   
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Password</label>
                  <div class="append-icon">
                    <input type="password" name="pass1" id="password1" value="" class="form-control">
                    <i class="icon-lock"></i>
                  </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Repeat Password</label>
                <div class="append-icon">
                  <input type="password" name="pass2" id="password2" value="" class="form-control">
                  <i class="icon-lock"></i>
                </div>
              </div>
            </div>
          </div>
          <div class="text-left  m-t-20">
            <div id="submitbutton"><button type="submit" class="btn btn-embossed btn-primary">Create</button></div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- new stop -->
<div class="modal fade" id="make_order_form" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
        <h4 class="modal-title"><strong>New Stop</strong></h4>
      </div>
      <div id="make_order_ajax"> 
        <?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');}?>         
      </div>
      <form id="new_stop" name="new_stop" class="form-validation" accept-charset="utf-8" enctype="multipart/form-data" method="post">
        <div class="modal-body">
          <input type="hidden" name="airway_bill_no_order_form" id="airway_bill_no_order_form" value="<?php echo $airwaybill; ?>" class="form-control">
          <div class="row">
            <div class="col-sm-12">
              <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width:10%" id="prgvalue">
                  10%
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label class="control-label">Contact Person</label>
                <div class="append-icon">
                  <select name="stop_contact_person[]" id="stop_contact_person" class="form-control" data-search="true" multiple="multiple">
                    <option value=""></option>
                    <?php foreach($contactpersons->result() as $contact_person){ ?>
                    <option value="<?php echo $contact_person->staff_id;?>"><?php echo $contact_person->fname.' '.$contact_person->lname;?></option>
                    <?php }?> 
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Pickup Location</label>
                <div class="append-icon">
                  <select name="pickup_location" id="pickup_location" class="form-control" data-search="true">
                    <option value=""></option>
                    <?php foreach($pickups as $pick){ ?>
                    <option value="<?php echo $pick->loc_id;?>"><?php echo $pick->name;?></option>
                    <?php }?> 
                  </select>
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Pickup Date/Time</label>
                <div class="append-icon">
                  <div class="row">
                    <div class="col-sm-6">
                      <input type="text" id="pickup_date" name="pickup_date" class="date-picker form-control">
                      <!--<i class="icon-calendar"></i>-->
                    </div>
                    <div class="col-sm-6">
                      <input type="text" id="pickup_time" name="pickup_time" class="form-control">
                      <!--<i class="glyphicon glyphicon-time"></i>-->
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Delivery Location</label>
                <div class="append-icon">
                  <select name="delivery_location[]" id="delivery_location" class="form-control" data-search="true" multiple="multiple">
                    <option value=""></option>
                    <?php foreach($deliveries as $pick){ ?>
                    <option value="<?php echo $pick->loc_id;?>"><?php echo $pick->name;?></option>
                    <?php }?>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Delivery Date/Time</label>
                <div class="append-icon">
                  <div class="row">
                    <div class="col-sm-6">
                      <input type="text" id="delivery_date" name="delivery_date" class="date-picker form-control">
                      <!--<i class="icon-calendar"></i>-->
                    </div>
                    <div class="col-sm-6">
                      <input type="text" id="delivery_time" name="delivery_time" class="form-control">
                      <!--<i class="glyphicon glyphicon-time"></i>-->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Item Quantity</label>
                <div class="append-icon">
                  <input type="text" id="item_quantity" name="item_quantity" class="form-control">
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Total Weight</label>
                <div class="append-icon">
                  <input type="text" id="total_weight" name="total_weight" class="form-control">
                  <div style="margin-top: 15px;">
                    <label>
                      <input type="radio" name="measure" value="kgs" data-checkbox="icheckbox_square-blue" checked="checked"> Kgs
                      &nbsp;
                      &nbsp;
                      &nbsp;
                      <input type="radio" name="measure" value="lbs" data-checkbox="icheckbox_square-blue"> Lbs
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Package Type</label>
                <div class="append-icon">
                  <select name="packaging_type[]" id="packaging_type" class="form-control" data-search="true" multiple="multiple">
                    <option value=""></option>
                    <?php foreach($packages->result() as $type){ ?>
                    <option value="<?php echo $type->id;?>"><?php echo $type->type;?></option>
                    <?php }?> 
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="text-left  m-t-20">
            <div id="makeordersubmitbutton">
              <button type="submit" class="btn btn-embossed btn-primary" onclick="place_order();">Next</button>
            </div>
          </div> 
        </div>
      </form>
    </div>
  </div>
</div>