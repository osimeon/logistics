<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
  <div class="header">
    <h2 style="color: #ffc000;"><strong>Your Pending Orders</strong></h2> 
    <div class="breadcrumb-wrapper"></div>      
  </div>

  <div class="row">
    <div class="panel">                              
      <div class="panel-content">
        <div class="panel-content pagination2 table-responsive">
          <table class="table table-hover table-dynamic filter-between_date" id="pending_orders">
            <thead>
              <tr>                        
                <th>AIRWAY BILL NO</th>
                <th>TRANSPORTER</th> 
                <th>CUSTOMER</th>
                <th>STATUS</th> 
                <th>COMMENTS</th> 
                <th>DATE</th>    
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT -->