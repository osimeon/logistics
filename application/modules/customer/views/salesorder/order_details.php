<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript">

	// initialize google map
	function initialize(){
		var mapOptions = {
			center: new google.maps.LatLng(-1.2833, 36.8167),
	      	zoom: 18,
	      	mapTypeId: google.maps.MapTypeId.ROADMAP,
	      	mapTypeControl: false,
	      	streetViewControl: false,
	      	panControl: false,
	      	scrollwheel: false,
	      	scaleControl: true,
	      	zoomControlOptions: {
	        	position: google.maps.ControlPosition.LEFT_BOTTOM
	      	}
    	};

    	var map = new google.maps.Map($("#pr_trip_map")[0], mapOptions);

    	var directionsService = new google.maps.DirectionsService();

    	var directionsRequest = {
        	origin: $('#trip_from').val(),
        	destination: $('#trip_to').val(),
        	travelMode: google.maps.DirectionsTravelMode.DRIVING,
        	unitSystem: google.maps.UnitSystem.METRIC
      	};

      	directionsService.route(directionsRequest, function(response, status){
        	if(status == google.maps.DirectionsStatus.OK){
          		new google.maps.DirectionsRenderer({
            		map: map,
            		directions: response
          		});
    		}
        });
	}

    google.maps.event.addDomListener(window, "load", initialize);
</script>
<script type="text/javascript">
	function reject_quote(odernumber){
		var reason = prompt("Kindly let us know the reason for rejecting this order?");

		var custreason = "NA";

		if(reason != null){
        	custreason = reason;
    	}
	}
	function accept_quote(odernumber){
		var result = confirm("Are you sure you want to accept this quote?");

		if(result){
			$.ajax({
      			type: "GET",
      			url: "<?php echo site_url('customer/salesorder/accept_order_quote' ); ?>/" + odernumber,
      			success: function(msg){
        			if(msg == 'sent'){
        				alert("Your reponse has been received. Thank you!");
          				console.log('quote was sent');
        			}
        			else{
        				alert("Response could not be sent at this time. Try again later!");
        			}
      			}
    		});
		}
	}
</script>

<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
	<input type="hidden" id="quote_order_number" value="<?php echo $orderno; ?>" />
    <!--<div class="header">
        <h2><strong><?php echo $order->airwaybill_no;?></strong></h2> 
        <div class="breadcrumb-wrapper"></div>                
  	</div>-->

  	<div class="row">
		<div class="panel"> 
			<div class="panel-content">
				<ul class="nav nav-tabs">
            		<li class="active"><a href="#tab_summary_information" data-toggle="tab">Summary</a></li>
                    <li class=""><a href="#tab_quotation_and_invoice_information" data-toggle="tab">Quotation & Invoice Information</a></li>
                    <li class=""><a href="#tab_resource_information" data-toggle="tab">Resource Information</a></li>
              	</ul>
              	<div class="tab-content">
              		<div class="tab-pane fade active in" id="tab_summary_information">
						<h5>Summary Information for Order: <b><?php echo $order->airwaybill_no;?></b></h5>
						<hr/>
						<div class="row">
							<div class="col-sm-6">
								<div class="row">
		                        	<div class="col-sm-12">
		                          	  	<div class="form-group">
		                              		<label class="col-sm-4 control-label"><i class="fa fa-user"></i>Customer</label>
		                              		<div class="col-sm-8 append-icon">  
						                    	<?php echo $this->salesorder_model->getcompanyname($order->customer_customer_id); ?><br/>
				                          		<?php echo $this->salesorder_model->getcompanyaddress($order->customer_customer_id); ?>     
			                              	</div>
		                              	</div>
									</div>
								</div> 
							 	
							 	<div class="row">
			                 		<div class="col-sm-12">
		                      	  		<div class="form-group">
		                              		<label class="col-sm-4 control-label"><i class="fa fa-calendar"></i>Date</label>
			                              	<div class="col-sm-8 append-icon">
		                                 		<?php echo date('m/d/Y H:i', $order->order_date); ?> 
			                              	</div>
		                              	</div>
									</div>
								</div> 

								<div class="row">
		                 			<div class="col-sm-12">
		                      	  		<div class="form-group">
		                              		<label class="col-sm-4 control-label"><i class="fa fa-user-plus"></i>Contact</label>
			                              	<div class="col-sm-8 append-icon">
		                                 		<?php echo $this->salesorder_model->getcontactnames($order->contact_person); ?>
		                              		</div>
			                            </div>
									</div>
								</div>   
							</div>
							
							<div class="col-sm-6">
								<div class="row">
			                 		<div class="col-sm-12">
		                      	  		<div class="form-group">
		                              		<label class="col-sm-4 control-label"><i class="fa fa-list-ul"></i>Weight</label>
			                              	<div class="col-sm-8 append-icon">
		                               			<?php echo $order->total_weight.' '.$order->unit; ?>
		                           			</div>
		                       			</div>
		                   			</div>
		               			</div>
							 	
							 	<div class="row">
							 		<div class="col-sm-12">
							 			<div class="form-group">
							 				<label class="col-sm-4 control-label"><i class="fa fa-truck"></i>Vehicle</label>
			                              	<div class="col-sm-8 append-icon">
		                               			<?php echo $order->vehicle_size; ?>
		                           			</div>
		                       			</div>
		                   			</div>
		               			</div>
		           			</div>
		                </div>

		                <br/>
		                <!--<h5><b>Documents</b></h5>-->
						<hr/>
						<div class="row">
							<table class="table">
								<thead>
		                      		<tr style="font-size: 12px;">                         
			                        	<th>Documents</th>
			                        	<th></th>
		                      		</tr>
			                    </thead>
			                    <tbody id="InputsWrapper">
			                    	<?php if(!empty($order->order_document)){?>
			                    	<?php $documents = explode(',', $order->order_document);?>
			                    	<?php for($i = 0; $i < sizeof($documents); $i++){ ?>
			                    	<tr>
			                    		<td>
			                    			<a href="<?php echo base_url('uploads/products'); ?>/<?php echo $documents[$i]; ?>">Doc <?php echo $i+1; ?></a>
		                    			</td>
			                    		<td>
			                    			<a href="<?php echo base_url('uploads/products'); ?>/<?php echo $documents[$i]; ?>" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a>
		                    			</td>
			                    	</tr>
			                    	<?php } ?>
			                    	<?php }?>
			                    </tbody>
							</table>
						</div>
              		</div>
              		<div class="tab-pane fade" id="tab_quotation_and_invoice_information">
              			<div class="row">
							<div class="col-sm-12">
								<h5><strong>Quotation & Invoice Information</strong></h5>
								<hr/>

								<div id="add_quote_item">
									<button type="submit" class="btn btn-embossed btn-warning" onclick="reject_quote(<?php echo $orderno; ?>);"><i class="fa fa-minus"></i> Reject</button>
									<button type="submit" class="btn btn-embossed btn-success" onclick="accept_quote(<?php echo $orderno; ?>);"><i class="fa fa-paper-plane"></i> Accept Quote</button>
								</div>

								<table class="table" id="quotation_table">
									<thead>
										<tr style="font-size: 12px;">
											<td>Item</td>
											<td>Desc</td>
											<td>Quantity</td>
											<td>Unit Price</td>
										</tr>
									</thead>
									<tbody id="InputsWrapper">

									</tbody>
								</table>
							</div>
							<div class="row">
								<div class="col-sm-8">

								</div>
								<div class="col-sm-4">
									<div class="row">
		   		                 		<div class="col-sm-12">
		                          	  		<div class="form-group">
			                              		<label class="col-sm-6 control-label">Untaxed Amount </label>
			                              		<div class="col-sm-6 append-icon">
				                               		<span id="quote_untaxed">0</span> 
				                              	</div>
			                              	</div>
										</div>
									</div>
									<div class="row">
		   		                 		<div class="col-sm-12">
		                          	  		<div class="form-group">
			                              		<label class="col-sm-6 control-label">Taxes </label>
			                              		<div class="col-sm-6 append-icon">
			                               			<span id="quote_taxes">0</span> 
				                              	</div>
			                              	</div>
										</div>
									</div>
									<div class="row">
		   		                 		<div class="col-sm-12">
		                          	  		<div class="form-group">
			                              		<label class="col-sm-6 control-label">Total </label>
				                              	<div class="col-sm-6 append-icon">
			                               			<span id="quote_total">0</span>
				                              	</div>
			                              	</div>
										</div>
									</div>	
								</div>
							</div>
						</div>
              		</div>
              		<div class="tab-pane fade" id="tab_resource_information">
						<div class="row">
							<div class="col-sm-12">
								<table class="table" id="resources_table">
									<thead>
										<tr style="font-size: 12px;">
											<td>Name</td>
											<td>Desc</td>
											<td>Quantity</td>
											<td>Image</td>
										</tr>
									</thead>
									<tbody id="InputsWrapper">

									</tbody>
								</table>
							</div>
						</div>
              		</div>
              	</div>
			</div>
		</div>
  	</div>
  	<div class="row">
  		<div class="row">
			<div class="col-sm-12">
				<div class="panel">
					<div class="panel-content">
						<div class="row">&nbsp;</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="row">
									<table width="100%" style="border-collapse:separate; border-spacing:0 1em; padding: 40px;">
		          						<tr>
		            						<td colspan="2">
		              							<center><h4 class="text-center"><strong>SHIPMENT REQUEST DELIVERY SUMMARY SHEET</strong></h4></center>
		            						</td>
		          						</tr>
		          						
		          						<tr>
		            						<td><h5>ORDER REFERENCE#: <label class="control-label" id="pr_airway_bill_no"></label></h5></td>
		            						<td><h5><span class="btn pull-right" style="background-color: #ffc000; color:#fff;">ORDER STATUS: <?php 
		            						$status = $order->customer_status;

		            						if($status == 2){
		            							echo 'PENDING';
		            						}
		            						elseif($status == 3){
		            							echo 'ACTION NEEDED';
		            						}
		            						elseif($status == 4){
		            							echo 'LIVE';
		            						}
		            						elseif($status == 5){
		            							echo 'CLOSED';
		            						}
		            						else{
		            							echo 'N/A';
		            						}
		            						?>
		            						</span></h5></td>
		          						</tr>
		          						
		          						<tr>
		            						<td><img class="pull-left" alt="Barcode" src="<?php echo site_url('customer/salesorder/generate_bar_code').'/'.$order->airwaybill_no; ?>" /></td>
		        							<td>
		          								<img class="pull-right" align="Company logo" src="<?php echo base_url('uploads/site/logo.png'); ?>" />
		            						</td>
		          						</tr>
		          						
		          						<tr>
		            						<td>
		              							ORDER TIME: <label class="control-label" id="pr_order_time"><?php echo date('Y-m-d H:i:s'); ?></label>
		              							<br/>
		              							ORDERED BY: <strong><?php echo $this->salesorder_model->getcompanyname($order->customer_customer_id); ?></strong>
		              							<br/>
		              							ORDER CONTACT: <strong><label class="control-label" id="pr_order_contact"><?php echo $this->salesorder_model->getcontactnames($order->contact_person); ?></label></strong>
		            						</td>
		          						</tr>
		          						
		          						<tr>
		            						<td colspan="2">
		              							<center><h4 class="text-center"><strong>SHIPMENT REQUEST DETAILS</strong></h4></center>
		            						</td>
		          						</tr>
		          						
		          						<tr>
		            						<td>
		            							<label class="control-label" id="pr_pickup_location">
		            								<?php
		            								echo '[Pickup Location] - '.$this->salesorder_model->getlocations($order->pickup_loc).' - '.$order->pickup_date.' '.$order->pickup_time;

		            								?>
		            								<input type="hidden" id="trip_from" name="trip_from" value="<?php echo $this->salesorder_model->getlocations($order->pickup_loc); ?>" />
		            							</label>
		        							</td>
		          						</tr>
		      							
		      							<tr>
		            						<td>
		            							<label class="control-label" id="pr_delivery_location">
		            								<?php
		            								echo '[Delivery Location] - '.$this->salesorder_model->getlocations($order->del_loc).' - '.$order->del_date.' '.$order->del_time;

		            								?>	
		            								<input type="hidden" id="trip_to" name="trip_to" value="<?php echo $this->salesorder_model->getlocations($order->del_loc); ?>" />
		            							</label>
		        							</td>
		          						</tr>
		          
		          						<tr>
		            						<td colspan="2">
		            							<div id="pr_trip_map" style="width:100%; height:300px;" style="z-index: 20;"></div>
		            						</td>
		          						</tr>
		          
		          						<tr>
		            						<td>
		            							<label class="control-label" id="pr_item_quantity">
		            								<?php echo '[Item Q] - '.$order->item_quantity.' '.$order->unit; ?>
		            							</label>
		        							</td>
		          						</tr>
		          
		          						<tr>
		            						<td>
			            						<label class="control-label" id="pr_package_type">
			            							<?php 
										            $packages = $this->salesorder_model->packages($order->package_type);
										            $poutput = array();

										            foreach($packages->result() as $package){
										              $poutput[] = $package->type;
										            }

										            //var_dump($poutput);
										            echo '[P.Type] - '.implode(",", $poutput);
										            ?>
			            						</label>
		            						</td>
		          						</tr>
		          
		          						<tr>
		            						<td>
		            							<label class="control-label" id="pr_vehicle_size">
		            								<?php
		            								echo '[T.Type] - '.$order->vehicle_size;
		            								?>
		            							</label>
		        							</td>
		          						</tr>
		          
		          						<tr>
		            						<td>
		            							<label class="control-label" id="pr_notes">
		            								<?php
		            								echo '[Notes] - '.$order->comment; 
		            								?>
		            							</label>
		        							</td>
		          						</tr>
		        					</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
  	</div>
</div><!-- END PAGE CONTENT -->