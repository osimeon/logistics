<html>
  <head>
    <title>PDF Preview</title>
    <link href="<?php echo base_url(); ?>public/assets/global/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>public/assets/global/css/theme.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>public/assets/global/css/ui.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>public/assets/admin/layout1/css/layout.css" rel="stylesheet">
    <!-- BEGIN PAGE STYLE -->
    <link href="<?php echo base_url(); ?>public/assets/global/plugins/metrojs/metrojs.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>public/assets/global/plugins/maps-amcharts/ammap/ammap.min.css" rel="stylesheet">
    <!-- END PAGE STYLE -->

    <!-- BEGIN PAGE STYLE -->
    <link href="<?php echo base_url(); ?>public/assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <!-- END PAGE STYLE -->

    <link href="<?php echo base_url('public/logistics/css/jquery.timepicker.css'); ?>" rel="stylesheet">

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>public/assets/global/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDWEyNzk8ze3TGjyVWsdhzjnB_H1PtzVF4&libraries=places,geometry&signed_in=true&sensor=false&v=3.exp"></script>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  </head>

  <body>
    <table width="100%" style="border-collapse:separate; border-spacing:0 1em;">
      <tr>
        <td colspan="2">
          <center><h4 class="text-center"><strong>SHIPMENT REQUEST DELIVERY SUMMARY SHEET</strong></h4></center>
        </td>
      </tr>
      <tr>
        <td><h5>ORDER REFERENCE#: <label class="control-label" id="pr_airway_bill_no"><?php echo $airwaybill; ?></label></h5></td>
        <td>
          <h5><span class="btn pull-right" style="background-color: #ffc000; color:#fff;">ORDER STATUS: PENDING</span></h5>
        </td>
      </tr>
      <tr>
        <td>
          <img class="pull-left" alt="Barcode" src="<?php echo site_url('customer/salesorder/generate_bar_code').'/'.$airwaybill; ?>" />
        </td>
        <td>
          <img class="pull-right" align="Company logo" src="<?php echo base_url('uploads/site/logo.png'); ?>" />
        </td>
      </tr>
      <tr>

        <td>
          ORDER TIME: <label class="control-label" id="pr_order_time"><?php echo date('Y-m-d H:i:s'); ?></label>
          <br/>
          ORDERED BY: <strong><?php echo userdata_customer('name');?></strong>
          <br/>
          ORDER CONTACT: <strong><label class="control-label" id="pr_order_contact">
            <?php 
            $m_contacts = $this->site_model->getcontactnames($ordercontact); 
            $arr_contacts = array();

            foreach($m_contacts->result() as $m_co){
              $arr_contacts[] = $m_co->lname.' '.$m_co->fname;
            }

            echo implode(",", $arr_contacts);
            ?>


          </label></strong>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <center><h4 class="text-center"><strong>SHIPMENT REQUEST DETAILS</strong></h4></center>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" id="pr_pickup_location">
            <?php 
            $pick = $this->site_model->getpickup($pickup);
            echo '[Pickup Location] - '.$this->site_model->getpickup($pickup).' - '.$pdate.' '.$ptime; 
            ?>
          </label>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" id="pr_delivery_location">
            <?php 
            $delivery = $this->site_model->getStops($delivery);
            $output = array();

            foreach($delivery->result() as $deliv){
              $output[] = $deliv->name;
            }

            echo '[Delivery Location] - '.implode(",", $output).' - '.$ddate.' '.$dtime;
            ?>
          </label>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <div id="pr_trip_map" style="width:100%; height:300px;" style="z-index: 20;">
          </div>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" id="pr_item_quantity">
            <?php echo '[Item Q] - '.$quantity; ?>
          </label>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" id="pr_package_type">
            <?php 
            $packages = $this->site_model->packages($package);
            $poutput = array();

            foreach($packages->result() as $package){
              $poutput[] = $package->type;
            }

            //var_dump($poutput);
            echo '[P.Type] - '.implode(",", $poutput);
            ?>
          </label>
        </td>
      </tr>
      <tr>
        <td>
          <label class="control-label" id="pr_vehicle_size">
            <?php echo '[T.Type] - '.$vehicle; ?>
          </label>
        </td>
      </tr>
      <tr>
        <td><label class="control-label" id="pr_notes">[Notes] - <br/><?php echo $notes;?></td>
      </tr>

      <script type="text/javascript">
    var mapOptions = {
      center: new google.maps.LatLng(-1.2833, 36.8167),
      zoom: 18,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControl: false,
      streetViewControl: false,
      panControl: false,
      scrollwheel: false,
      scaleControl: true,
      zoomControlOptions: {
        position: google.maps.ControlPosition.LEFT_BOTTOM
      }
    }

      var map = new google.maps.Map($("#pr_trip_map")[0], mapOptions);

      function calculateRoute(from, to){
        console.log("Getting routes: source => " + from + ", destination => " + to);
        var directionsService = new google.maps.DirectionsService();

        var directionsRequest = {
          origin: from,
          destination: to,
          travelMode: google.maps.DirectionsTravelMode.DRIVING,
          unitSystem: google.maps.UnitSystem.METRIC
        };

        directionsService.route(directionsRequest, function(response, status){
          if(status == google.maps.DirectionsStatus.OK){
            new google.maps.DirectionsRenderer({
              map: map,
              directions: response
            });
          }
            //$("#error").append("Unable to retrieve your route<br />");
          }
        );
      }

      var from = $('#map_from').val();
      var to = $('#map_to').val();

      console.log("From: " + from);
      console.log("To: " + to);

      calculateRoute('Nairobi Kenya', 'Kisumu Kenya');

  </script>

    </table>


  </body>
</html>