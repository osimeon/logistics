                <div class="footer">
                    <div class="copyright">
                        <p class="pull-left sm-pull-reset">
                            <span>Copyright <span class="copyright">©</span> <?php echo date('Y');?> </span>
                            <span><?php echo config('site_name'); ?></span>.
                            <span>All rights reserved. </span>
                        </p>
                        <p class="pull-right sm-pull-reset">
                            <span><a href="#" class="m-r-10">Support</a> | <a href="#" class="m-l-10 m-r-10">Terms of use</a> | <a href="#" class="m-l-10">Privacy Policy</a></span>
                        </p>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT -->
        </section>
   
        <!-- BEGIN PRELOADER -->
        <div id="dataloading"></div>
            
        <div class="loader-overlay">
          <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
          </div>
        </div> 
        <!-- END PRELOADER -->

        <a href="#" class="scrollup"><i class="fa fa-angle-up"></i></a> 
        <script src="<?php echo base_url(); ?>public/assets/global/plugins/jquery/jquery-1.11.1.min.js"></script>
        <script src="<?php echo base_url(); ?>public/assets/global/plugins/jquery/jquery-migrate-1.2.1.min.js"></script>
        <script src="<?php echo base_url(); ?>public/assets/global/plugins/jquery-ui/jquery-ui-1.11.2.min.js"></script>
        <script src="<?php echo base_url(); ?>public/assets/global/plugins/gsap/main-gsap.min.js"></script>
        <script src="<?php echo base_url(); ?>public/assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>

        <!-- simulate synchronous behavior when using AJAX -->
        <script src="<?php echo base_url(); ?>public/assets/global/plugins/jquery-block-ui/jquery.blockUI.min.js"></script> 

        <!-- Modal with Validation -->
        <script src="<?php echo base_url(); ?>public/assets/global/plugins/bootbox/bootbox.min.js"></script> 

        <!-- Custom Scrollbar sidebar -->
        <script src="<?php echo base_url(); ?>public/assets/global/plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script> 

        <!-- Show Dropdown on Mouseover -->
        <script src="<?php echo base_url(); ?>public/assets/global/plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js"></script> 

        <!-- Charts Sparkline -->
        <script src="<?php echo base_url(); ?>public/assets/global/plugins/charts-sparkline/sparkline.min.js"></script> 

        <!-- Retina Display -->
        <script src="<?php echo base_url(); ?>public/assets/global/plugins/retina/retina.min.js"></script> 

        <!-- Select Inputs -->
        <script src="<?php echo base_url(); ?>public/assets/global/plugins/select2/select2.min.js"></script> 

        <!-- Checkbox & Radio Inputs -->
        <script src="<?php echo base_url(); ?>public/assets/global/plugins/icheck/icheck.min.js"></script> 

        <!-- Background Image -->
        <script src="<?php echo base_url(); ?>public/assets/global/plugins/backstretch/backstretch.min.js"></script>

        <!-- Animated Progress Bar -->
        <script src="<?php echo base_url(); ?>public/assets/global/plugins/bootstrap-progressbar/bootstrap-progressbar.min.js"></script> 
             
        <!-- Sidebar on Hover -->
        <script src="<?php echo base_url(); ?>public/assets/global/js/sidebar_hover.js"></script>

        <!-- Main Application Script -->
        <script src="<?php echo base_url(); ?>public/assets/global/js/application.js"></script> 

        <!-- Main Plugin Initialization Script -->
        <script src="<?php echo base_url(); ?>public/assets/global/js/plugins.js"></script> 
            
        <!-- Notes Widget -->
        <script src="<?php echo base_url(); ?>public/assets/global/js/widgets/notes.js"></script>

        <!-- Chat Script -->
        <script src="<?php echo base_url(); ?>public/assets/global/js/quickview.js"></script> 
            
        <!-- BEGIN PAGE SCRIPT -->
        <!-- Notifications -->
        <script src="<?php echo base_url(); ?>public/assets/global/plugins/noty/jquery.noty.packaged.min.js"></script>  

        <!-- Inline Edition X-editable -->
        <script src="<?php echo base_url(); ?>public/assets/global/plugins/bootstrap-editable/js/bootstrap-editable.min.js"></script> 

        <!-- Context Menu -->
        <script src="<?php echo base_url(); ?>public/assets/global/plugins/bootstrap-context-menu/bootstrap-contextmenu.min.js"></script> 

        <!-- Time Picker -->
        <script src="<?php echo base_url(); ?>public/assets/global/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> 

        <!-- Multi dates Picker -->
        <script src="<?php echo base_url(); ?>public/assets/global/plugins/multidatepicker/multidatespicker.min.js"></script> 
        <script src="<?php echo base_url(); ?>public/assets/global/js/widgets/todo_list.js"></script>

        <!-- Flipping Panel -->
        <script src="<?php echo base_url(); ?>public/assets/global/plugins/metrojs/metrojs.min.js"></script> 

        <!-- ChartJS Chart -->
        <script src="<?php echo base_url(); ?>public/assets/global/plugins/charts-chartjs/Chart.min.js"></script>  

        <!-- financial Charts -->
        <script src="<?php echo base_url(); ?>public/assets/global/plugins/charts-highstock/js/highstock.min.js"></script> 

        <!-- Financial Charts Export Tool -->
        <script src="<?php echo base_url(); ?>public/assets/global/plugins/charts-highstock/js/modules/exporting.min.js"></script> 
        <script src="<?php echo base_url(); ?>public/assets/global/plugins/dashboard/ammap.min.js"></script>   

        <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?php echo base_url(); ?>public/assets/global/plugins/datatables/jquery.dataTables.min.js"></script> 
        <script src="<?php echo base_url(); ?>public/assets/global/js/pages/table_dynamic.js"></script>

        <!-- Simple HTML Editor -->
        <script src="<?php echo base_url(); ?>public/assets/global/plugins/summernote/summernote.min.js"></script> 

        <!-- Advanced HTML Editor --> 
        <script src="<?php echo base_url(); ?>public/assets/global/plugins/cke-editor/ckeditor.js"></script> 
        <script src="<?php echo base_url(); ?>public/assets/global/plugins/cke-editor/adapters/adapters.min.js"></script>

        <!-- END PAGE SCRIPTS -->

        <!-- Animated Counter Number -->        
        <script src="<?php echo base_url(); ?>public/assets/global/plugins/countup/countUp.min.js"></script> 
            
        <!-- END PAGE SCRIPT -->

        <!-- Buttons Loading State -->
        <script src="<?php echo base_url(); ?>public/assets/global/plugins/bootstrap-loading/lada.min.js"></script> 

        <!-- Search Script -->
        <script src="<?php echo base_url(); ?>public/assets/global/js/pages/search.js"></script> 

        <!-- Search Filter -->
        <script src="<?php echo base_url(); ?>public/assets/global/plugins/quicksearch/quicksearch.min.js"></script> 
        <script src="<?php echo base_url(); ?>public/assets/global/js/pages/mailbox.js"></script>
            
        
        <script src='<?php echo base_url(); ?>public/assets/global/plugins/moment/moment.min.js'></script>
        <script src='<?php echo base_url(); ?>public/assets/global/plugins/fullcalendar/fullcalendar.min.js'></script>
            
        <script src="<?php echo base_url(); ?>public/assets/global/js/pages/dashboard.js"></script>
        <!-- BEGIN PAGE SCRIPTS -->
            
        <script src="<?php echo base_url(); ?>public/assets/admin/layout1/js/layout.js"></script>
        <script src="<?php echo base_url('public/logistics/js/jquery.timepicker.js'); ?>"></script>
             
        <!-- Search between two date -->   
        <?php $get_controller_nm = $this->uri->segment(2); ?>

        <script type="text/javascript" class="init"> 
            $.fn.dataTable.ext.search.push(
            	function( settings, data, dataIndex ) {
            		var min = new Date($('#min').val()).getTime();
            		var max = new Date($('#max').val()).getTime(); 

            		<?php 
                    if($get_controller_nm=="contracts") { ?>
                        var date = new Date(data[0]).getTime() || 0;
            		<?php } else { ?>
                        var date = new Date(data[1]).getTime() || 0;
            		<?php } ?> 

            		// use data for the date column
            		//alert(date);
            		if((isNaN(min) && isNaN(max)) || (isNaN(min) && date <= max) || (min <= date   && isNaN(max)) || (min <= date   && date <= max)){
            			return true;
            		}

            		return false;
            	}
            );		
        </script>

        <style>
            .error_on_date{ 
                border-color: #CD5C5C; 
                border-width:1px; 
                border-style:solid; 
            }
        </style>

        <script>
            // site preloader -- also uncomment the div in the header and the css style for #preloader	 
            jQuery(document).ready(function($) {  
                $(window).load(function(){
                	$('#preloader').fadeOut('slow',function(){$(this).remove();});
                });
            });	

            jQuery(document).ready(function($) {  
                $(window).load(function(){
                    $('#dataloading').fadeOut('slow',function(){$(this).remove();});
                });

                $('#pickup_time').timepicker({'step': 15});
                $('#delivery_time').timepicker({'step': 15});
                $('#opening_hour').timepicker({'step': 15});
                $('#closing_hour').timepicker({'step': 15});
                $('#d_opening_hour').timepicker({'step': 15});
                $('#d_closing_hour').timepicker({'step': 15});
            }); 

            // pending orders

            
            var url = window.location.href;
            var lastPart = url.substr(url.lastIndexOf('/') + 1);

            // all orders
            if(lastPart === "all"){
                var all_table = $('#all_orders').DataTable( {
                    ajax: {
                        url: "<?php echo site_url('customer/salesorder/all_ajax'); ?>",
                        dataSrc: "items"
                    },
                    columns: [
                        { data: "airway" },
                        { data: "transporter" },
                        { data: "customer" },
                        { data: "status" },
                        { data: "comments" },
                        { data: "date" },
                    ],
                    bPaginate: false,
                    searching: false
                });

                setInterval( function () {
                        all_table.ajax.reload();
                }, 1000 );
            }

            // pending orders
            else if(lastPart === "pending"){
                var pending_table = $('#pending_orders').DataTable( {
                    ajax: {
                        url: "<?php echo site_url('customer/salesorder/pending_ajax'); ?>",
                        dataSrc: "items"
                    },
                    columns: [
                        { data: "airway" },
                        { data: "transporter" },
                        { data: "customer" },
                        { data: "status" },
                        { data: "comments" },
                        { data: "date" },
                    ],
                    bPaginate: false,
                    searching: false
                });

                setInterval( function () {
                        pending_table.ajax.reload();
                }, 1000 );
            }

            // orders needing action
            else if(lastPart == "need_action"){
                var action_table = $('#action_needed').DataTable( {
                    ajax: {
                        url: "<?php echo site_url('customer/salesorder/need_action_ajax'); ?>",
                        dataSrc: "items"
                    },
                    columns: [
                        { data: "airway" },
                        { data: "transporter" },
                        { data: "customer" },
                        { data: "status" },
                        { data: "comments" },
                        { data: "date" },
                    ],
                    bPaginate: false,
                    searching: false
                });

                setInterval( function () {
                        action_table.ajax.reload();
                }, 1000 );
            }

            // live orders
            else if(lastPart == "live"){
                var live_table = $('#live_orders').DataTable( {
                    ajax: {
                        url: "<?php echo site_url('customer/salesorder/live_ajax'); ?>",
                        dataSrc: "items"
                    },
                    columns: [
                        { data: "airway" },
                        { data: "transporter" },
                        { data: "customer" },
                        { data: "status" },
                        { data: "comments" },
                        { data: "date" },
                    ],
                    bPaginate: false,
                    searching: false
                });

                setInterval( function () {
                        live_table.ajax.reload();
                }, 1000 );
            }

            // closed orders
            else if(lastPart == "closed"){
                var closed_table = $('#closed_orders').DataTable( {
                    ajax: {
                        url: "<?php echo site_url('customer/salesorder/closed_ajax'); ?>",
                        dataSrc: "items"
                    },
                    columns: [
                        { data: "airway" },
                        { data: "transporter" },
                        { data: "customer" },
                        { data: "status" },
                        { data: "comments" },
                        { data: "date" },
                    ],
                    bPaginate: false,
                    searching: false
                });

                setInterval( function () {
                        closed_table.ajax.reload();
                }, 1000 );
            }

            // staff list
            else if(lastPart == "staff"){
                var staff_list = $('#customer_staff').DataTable( {
                    ajax: {
                        url: "<?php echo site_url('customer/staff/get_customers_ajax'); ?>",
                        dataSrc: "items"
                    },
                    columns: [
                        { data: "name" },
                        { data: "email" },
                        { data: "job" },
                        { data: "active" },
                        { data: "registered" },
                        { data: "options" },
                    ],
                    bPaginate: false,
                    searching: false
                });

                setInterval( function () {
                        staff_list.ajax.reload();
                }, 1000 );
            }

            // location contacts
            else if(lastPart == "contacts"){
                var location_contacts = $('#location_contacts').DataTable( {
                    ajax: {
                        url: "<?php echo site_url('customer/get_contacts_ajax'); ?>",
                        dataSrc: "items"
                    },
                    columns: [
                        { data: "name" },
                        { data: "email" },
                        { data: "position" },
                        { data: "options" },
                    ],
                    bPaginate: false,
                    searching: false
                });

                setInterval( function () {
                        location_contacts.ajax.reload();
                }, 1000 );
            }

            // pickup locations
            else if(lastPart == "pickup"){
                var pickup_list = $('#pickup_list').DataTable( {
                    ajax: {
                        url: "<?php echo site_url('customer/pickup/get_pickups_ajax'); ?>",
                        dataSrc: "items"
                    },
                    columns: [
                        { data: "name" },
                        { data: "opening" },
                        { data: "closing" },
                        { data: "contacts" },
                        { data: "address" },
                        { data: "options" }
                    ],
                    bPaginate: false,
                    searching: false
                });

                setInterval( function () {
                        pickup_list.ajax.reload();
                }, 1000 );
            }

            // packages
            else if(lastPart == "package"){
                var packages_list = $('#packages_list').DataTable( {
                    ajax: {
                        url: "<?php echo site_url('customer/package/get_packages_ajax'); ?>",
                        dataSrc: "items"
                    },
                    columns: [
                        { data: "type" },
                        { data: "description" },
                        { data: "options" }
                    ],
                    bPaginate: false,
                    searching: false
                });

                setInterval( function () {
                        packages_list.ajax.reload();
                }, 1000 );
            }

            function getServerUpdates(){
                $.ajax({
                    url: "<?php echo site_url('customer/salesorder/ajax'); ?>",
                    type: "POST",
                    async: false,
                    success: function(msg){
                        //console.log("Server message: "+msg);
                        var svr_resp = $.parseJSON(msg);
                        /*$.each(svr_resp, function(key, value){
                            console.log(key + ' . '+ value); 
                        });*/
                        var all = svr_resp.all;
                        var pending = svr_resp.pending;
                        var live = svr_resp.live;
                        var actions = svr_resp.action_needed;
                        var closed = svr_resp.closed;

                        document.getElementById("cust_all_orders").innerHTML = all;
                        document.getElementById("cust_pending_orders").innerHTML = pending;
                        document.getElementById("cust_live_orders").innerHTML = live;
                        document.getElementById("cust_action_orders").innerHTML = actions;
                        document.getElementById("cust_closed_orders").innerHTML = closed;

                        var url = window.location.href;
                        var lastPart = url.substr(url.lastIndexOf('/') + 1);

                        if(lastPart === "dashboard"){
                            document.getElementById("glance_all").innerHTML = "All (" + all + ")";
                            document.getElementById("glance_pending").innerHTML = "Pending (" + pending + ")";
                            document.getElementById("glance_action_needed").innerHTML = "Action Needed(" + actions + ")";
                            document.getElementById("glance_live").innerHTML = "Live (" + live + ")";
                            document.getElementById("glance_closed").innerHTML = "Closed (" + closed + ")";
                        }
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }

            setInterval(getServerUpdates, 1000);

            // pickup date
            $("#pickup_date").on("change keyup paste", function(){
                var dempty = "Please Select Pickup Date!";
                var dmessage = "Pickup date should not be less than today!";
                validateDate($(this).val(), dmessage, dempty);
            });

            // delivery date
            $("#delivery_date").on("change keyup paste", function(){
                var dempty = "Please Select Delivery Date!";
                var dmessage = "Delivery date should not be less than Pickup date!";
                validateDelivery($(this).val(), dmessage, dempty);
            });

            function validateDelivery(dvalue, dmessage, dempty){
                // get selected pickupdate
                var pickup = $('#pickup_date').val();
                var delivery = dvalue;
                var pickupDate = new Date(pickup);
                var deliveryDate = new Date(delivery);

                if(pickup = "" || pickup == null){
                    alert("Please select a pickup date to proceed");
                    $('#pickup_date').addClass("error_on_date");
                }

                if(delivery == ""){
                    alert(dempty);
                    $('#delivery_date').addClass("error_on_date");
                }
                else if(deliveryDate.setHours(0, 0, 0, 0) < pickupDate.setHours(0, 0, 0, 0)){
                    alert(dmessage);
                    $('#delivery_date').addClass("error_on_date");
                    $('#delivery_date').val('');
                }
                else{
                    $('#delivery_date').removeClass("error_on_date");
                }
            }

            function validateDate(dvalue, dmessage, dempty){
                var selectedDate = new Date(dvalue);
                var currentDate = new Date();
                console.log("Selected Date: " + selectedDate + ", Current Date: " + currentDate);

                if(selectedDate == ""){
                    alert(dempty);
                    $('#pickup_date').addClass("error_on_date");
                }
                else if(selectedDate.setHours(0, 0, 0, 0) < currentDate.setHours(0, 0, 0, 0)){
                    alert(dmessage);
                    $('#pickup_date').addClass("error_on_date");
                    $('#pickup_date').val('');
                }
                else{
                    $('#pickup_date').removeClass('error_on_date');
                }
            }

            function calculateProgress(){
                var total = 100;
                var factor = total / 11;

                // contact persons
                var contacts = Array();
                $('#stop_contact_person > option:selected').each(function() {
                    contacts.push($(this).text());
                });
                console.log('size of contacts: ' + contacts.length);

                if(contacts.length <= 0){
                   total -= factor;  
                }

                // pickup location
                var pickup = $("#pickup_location option:selected").text();
                console.log('Pickup loc: ' + pickup);

                if(pickup == ''){
                   total -= factor;  
                }

                // pickup date
                var p_dvalue = $('#pickup_date').val();
                console.log('Pickup date: ' + p_dvalue);

                if(p_dvalue == ''){
                    total -= factor;
                }

                // pickup time
                var p_tvalue = $('#pickup_time').val();
                console.log('Pickup time: ' + p_tvalue); 

                if(p_tvalue == ''){
                    total -= factor;
                }

                // delivery location
                var d_locs = Array();
                $('#delivery_location > option:selected').each(function() {
                    d_locs.push($(this).text());
                });

                console.log('size of delivery: ' + d_locs.length);

                if(d_locs.length <= 0){
                   total -= factor;  
                }

                // delivery date
                var d_dvalue = $('#delivery_date').val();
                console.log('Delivery date: ' + d_dvalue);

                if(d_dvalue == ''){
                    total -= factor;
                }

                // delivery time
                var d_tvalue = $('#delivery_time').val();
                console.log('Delivery time: ' + d_tvalue);

                if(d_tvalue == ''){
                    total -= factor;
                }

                // item quantity
                var itemq = $('#item_quantity').val();
                console.log('Item quantity: ' + itemq);

                if(itemq == ''){
                    total -= factor;
                }

                // total weight
                var totalw = $('#total_weight').val();
                console.log('Total weight: ' + totalw);

                if(totalw == ''){
                    total -= factor;
                }

                // package type
                var ptypes = Array();
                $('#packaging_type > option:selected').each(function() {
                    ptypes.push($(this).text());
                });

                console.log('Size Of Contacts: ' + ptypes.length);

                if(ptypes.length <= 0){
                   total -= factor;  
                }

                // airwaybill no
                var airway = $('#airway_bill_no').val();
                if(airway == ''){
                    total -= factor;
                }

                if(total >= 75 && total < 100){
                    $(".progress-bar").delay(3000).css("background-color", "orange");
                }
                else if(total == 100){
                    $(".progress-bar").delay(3000).css("background-color", "green");
                }
                else{
                    $(".progress-bar").delay(3000).css("background-color", "blue");
                }
                

                $('.progress-bar').css('width', total+'%').attr('aria-valuenow', total);
                $('#prgvalue').text(Math.floor(total) +'%');
            }

            calculateProgress();

            // setting agreement part
            // airway bill no
            console.log("Airway Bill No: " + $("#airway_bill_no").val());
            $('#pr_airway_bill_no').text($("#r_airway_bill_no").val());
            $("#airway_bill_no").on("change keyup paste", function(){
                calculateProgress();
            });

            // contact person
            $('#stop_contact_person').on('change', function() {
                calculateProgress();
            });

            //Pickup Location
            $('#pickup_location').on('change', function() {
                calculateProgress();
            });

            $("#pickup_date").on("change keyup paste", function(){
                calculateProgress();
            });

            $("#pickup_time").on("change keyup paste", function(){
                calculateProgress();
            });

            //Delivery Location
            $('#delivery_location').on('change', function() {
                calculateProgress();
            });

            $("#delivery_date").on("change keyup paste", function(){
                calculateProgress();
            });

            $("#delivery_time").on("change keyup paste", function(){
                calculateProgress();
            });

            //Item Quantity
            $('#item_quantity').on('change', function() {
                calculateProgress();
            });

            //Total Weight
            $('#total_weight').on('change', function() {
                calculateProgress();
            });

            //Package Size
            $("#packaging_type").on("change keyup paste", function(){
                calculateProgress();
            });

            //Notes
            $("#comments").on("change keyup paste", function(){
                calculateProgress();
            });

            $("#delivery_location").change(function() {
                calculateProgress();
            });
        </script>

        <script type="text/javascript">
            var orderno = $('#quote_order_number').val();

            if(orderno){
                $(document).ready( function (){
                    var resources = $('#resources_table').DataTable( {
                        ajax: {
                            url: "<?php echo site_url('admin/salesorder/get_resource_details_ajax'); ?>"+"/"+orderno,
                            dataSrc: "resources"
                        },
                        columns: [
                            { data: "name" },
                            { data: "desc" },
                            { data: "qua" },
                            { data: "avatar" }
                        ],
                        bPaginate: false
                    });

                    var table = $('#quotation_table').DataTable( {
                        ajax: {
                            url: "<?php echo site_url('admin/salesorder/quote_items_ajax'); ?>"+"/"+orderno,
                            dataSrc: "items"
                        },
                        columns: [
                            { data: "item" },
                            { data: "description" },
                            { data: "quantity" },
                            { data: "unit_price" }
                        ],
                        bPaginate: false
                    });

                    function getbillinfo(){
                        var onumber = $('#quote_order_number').val();
                        $.ajax({
                            url: "<?php echo site_url('admin/salesorder/quote_items_bill'); ?>" + "/" + onumber,
                            type: "GET",
                            async: false,
                            success: function(msg){
                                console.log("Server message: "+msg);
                                var svr_resp = $.parseJSON(msg);
                                $.each(svr_resp, function(key, value){
                                    console.log(key + ' . '+ value); 
                                });

                                var bill = svr_resp.bill;

                                document.getElementById("quote_untaxed").innerHTML = bill;
                                document.getElementById("quote_total").innerHTML = bill;
                            },

                            cache: false,
                            contentType: false,
                            processData: false
                        });
                    }

                    setInterval( function () {
                        table.ajax.reload();
                        resources.ajax.reload();
                        getbillinfo();
                    }, 1000 );
                });
            }

            var airwaybillno = $('#airway_bill_no').val();
          
            if(airwaybillno){
                // populate the items table
                var table = $('#airwaybill_items').DataTable( {
                    ajax: {
                        url: "<?php echo site_url('customer/salesorder/get_airwaybill_details_ajax'); ?>"+"/"+airwaybillno,
                        dataSrc: "items"
                    },
                    columns: [
                        { data: "stop" },
                        { data: "contact" },
                        { data: "pickup" },
                        { data: "delivery" },
                        { data: "quantity" },
                        { data: "package" },
                        { data: "options" }
                    ],
                    bPaginate: false,
                    searching: false
                });

                setInterval( function () {
                        table.ajax.reload();
                }, 1000 );
            }
        </script>
    </body>
</html>