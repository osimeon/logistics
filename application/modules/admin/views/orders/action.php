<!-- BEGIN PAGE CONTENT -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<div class="page-content">
	<div class="header">
    	<h2><strong>Orders that Need Action</strong></h2>         
  	</div>
     
    <div class="row">
       <div class="panel">																				
       		<div class="panel-content">
   				<div class="row">
					<div class="col-sm-3">
						<div class="form-group">
				  			<label class="control-label">Start Date</label>
				  			
				  			<div class="append-icon">
				    			<input type="text" id="min" name="min" class="date-picker form-control">
				    			<i class="icon-calendar"></i>
				  			</div>
						</div>
					</div>
			
					<div class="col-sm-3">
						<div class="form-group">
				  			<label class="control-label">End Date</label>
			  				<div class="append-icon">
				    			<input type="text" id="max" name="max" class="date-picker form-control">
				    			<i class="icon-calendar"></i>
				  			</div>
						</div>
					</div>
				</div>
   		
   				<div class="panel-content pagination2 table-responsive">
            <table class="table table-hover table-dynamic filter-between_date" id="action_table">
              <thead>
                <tr>                        
                  <th>AIRWAY BILL NO</th>
                  <th>TRANSPORTER</th> 
                  <th>CUSTOMER</th>
                  <th>STATUS</th> 
                  <th>COMMENTS</th> 
                  <th>DATE</th> 
                  <th>OPTIONS</th>   
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
      		</div>
   			</div>
	   	</div>
   	</div>
</div>
<!-- END PAGE CONTENT -->
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>