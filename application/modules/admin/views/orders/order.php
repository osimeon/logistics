<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDWEyNzk8ze3TGjyVWsdhzjnB_H1PtzVF4&libraries=places,geometry&signed_in=true&v=3.exp"></script>
<script type="text/javascript">
	// delete driver
	function delete_driver_resource(quote){
		$.ajax({
  			type: "GET",
  			url: "<?php echo site_url('admin/salesorder/delete_driver_resource' ); ?>/" + quote,
  			success: function(msg){
    			$("#driver_ajax").html(msg);
  			}
		});
	}

	// delete vehicle
	function delete_vehicle_resource(quote){
		$.ajax({
  			type: "GET",
  			url: "<?php echo site_url('admin/salesorder/delete_vehicle_resource' ); ?>/" + quote,
  			success: function(msg){
    			$("#driver_ajax").html(msg);
  			}
		});
	}

	// delete security
	function delete_security_resource(quote){
		$.ajax({
  			type: "GET",
  			url: "<?php echo site_url('admin/salesorder/delete_security_resource' ); ?>/" + quote,
  			success: function(msg){
    			$("#driver_ajax").html(msg);
  			}
		});
	}

	// assign driver
	function assigndriver(driver, order){
		$.ajax({
  			type: "GET",
  			url: "<?php echo site_url('admin/salesorder/assign_driver_ajax' ); ?>/" + driver + "/" + order,
  			success: function(msg){
    			$("#driver_ajax").html(msg);
  			}
		});
	}

	// assign vehicle
	function assignvehicle(vehicle, order){
		$.ajax({
  			type: "GET",
  			url: "<?php echo site_url('admin/salesorder/assign_vehicle_ajax' ); ?>/" + vehicle + "/" + order,
  			success: function(msg){
    			$("#vehicle_ajax").html(msg);
  			}
		});
	}

	// assign security
	function assignsecurity(security, order){
		$.ajax({
  			type: "GET",
  			url: "<?php echo site_url('admin/salesorder/assign_security_ajax' ); ?>/" + security + "/" + order,
  			success: function(msg){
    			$("#security_ajax").html(msg);
  			}
		});
	}

	// initialize google map
	function initialize(){
		var mapOptions = {
  		center: new google.maps.LatLng(-1.2833, 36.8167),
      	zoom: 14,
      	mapTypeId: google.maps.MapTypeId.ROADMAP,
      	mapTypeControl: false,
      	streetViewControl: false,
      	panControl: false,
      	scrollwheel: false,
      	scaleControl: true,
      	zoomControlOptions: {
        	position: google.maps.ControlPosition.LEFT_BOTTOM
      	}
  	};

    var map = new google.maps.Map($("#pr_trip_map")[0], mapOptions);

    	// get stops for items
    	$.ajax({
        url: "<?php echo site_url('admin/orders/get_airway_bill_items_locations_ajax'); ?>/<?php echo $order->airwaybill_no; ?>",
        	type: "GET",
            async: false,
            dataType: "json",
            success: function(msg){
              $.each(msg, function(index, element){
              	var stop = element.stop;
              	var contact = element.contact;
              	var pickup = element.pickup;
              	var delivery = element.delivery;
              	var loc_from = element.loc_from;
              	var loc_to = element.loc_to;

              	// google maps
              	var markerArray = [];
              	var directionsService = new google.maps.DirectionsService();
              	var directionsDisplay = new google.maps.DirectionsRenderer({map: map});
              	var stepDisplay = new google.maps.InfoWindow;

              	calculateAndDisplayRoute(directionsDisplay, directionsService, markerArray, stepDisplay, map, loc_from, loc_to);
                console.log(contact);
				      });
            },
            cache: false,
            contentType: false,
            processData: false
          });
        }

  google.maps.event.addDomListener(window, "load", initialize);

  function calculateAndDisplayRoute(directionsDisplay, directionsService, markerArray, stepDisplay, map, start, end){
		for(var i = 0; i < markerArray.length; i++){
  		markerArray[i].setMap(null);
		}

		directionsService.route({origin: start, destination: end, travelMode: google.maps.TravelMode.WALKING}, function(response, status){
  		if(status === google.maps.DirectionsStatus.OK){
    			directionsDisplay.setDirections(response);
    			showSteps(response, markerArray, stepDisplay, map);
  		} 
  		else{
    			console.log("failed");
  		}
		});
	}

	function showSteps(directionResult, markerArray, stepDisplay, map){
		var myRoute = directionResult.routes[0].legs[0];
		
		for(var i = 0; i < myRoute.steps.length; i++){
  		var marker = markerArray[i] = markerArray[i] || new google.maps.Marker;
  		marker.setMap(map);
  		marker.setPosition(myRoute.steps[i].start_location);
  		attachInstructionText(stepDisplay, marker, myRoute.steps[i].instructions, map);
		}
	}

	function attachInstructionText(stepDisplay, marker, text, map){
  		google.maps.event.addListener(marker, 'click', function(){
    		stepDisplay.setContent(text);
    		stepDisplay.open(map, marker);
  		});
	}
</script>
<script type="text/javascript">
	function delete_quote_item(qnumber){
		var result = confirm("Are you sure you want to delete this item");
		
		if(result){
    		console.log("was confirmed");

    		$.ajax({
      			type: "GET",
      			url: "<?php echo site_url('admin/salesorder/delete_item_ajax' ); ?>/" + qnumber,
      			success: function(msg){
        			if(msg == 'deleted'){
          				console.log('was deleted');
        			}
      			}
    		});
		}
	}

	function send_quote(odernumber){
		var result = confirm("Are you sure you want to send this quote to the customer?");

		if(result){
			$.ajax({
      			type: "GET",
      			url: "<?php echo site_url('admin/salesorder/send_customer_quote' ); ?>/" + odernumber,
      			success: function(msg){
        			if(msg == 'sent'){
        				alert("Quotation has been sent to customer. Thank you!");
          				console.log('quote was sent');
        			}
        			else{
        				alert("Quotation could not be sent at this time. Try again later!");
        			}
      			}
    		});
		}
	}
</script>

<script>
	$(document).ready(function(){
		$("form[name='quote_item_form']").submit(function(e){
			$("#takingorder").css({display: "block"});

			// item name
			if(!$('#quote_item_name').val()){
            	if($("#quote_item_name").parent().next(".validation").length == 0){
              		$("#quote_item_name").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter item name</div>");
            	}

            	$("#takingorder").css({display: "none"});
            	e.preventDefault();
            	return;
          	} 
          	else {
            	$("#quote_item_name").parent().next(".validation").remove();
          	}

          	// item quantity
          	if(!$('#quote_item_quantity').val()){
            	if($("#quote_item_quantity").parent().next(".validation").length == 0){
              		$("#quote_item_quantity").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter item quantity</div>");
            	}

            	$("#takingorder").css({display: "none"});
            	e.preventDefault();
            	return;
          	} 
          	else {
            	$("#quote_item_quantity").parent().next(".validation").remove();
          	}

          	// item unit price
          	if(!$('#quote_item_unit_price').val()){
            	if($("#quote_item_unit_price").parent().next(".validation").length == 0){
              		$("#quote_item_unit_price").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter item unit price</div>");
            	}

            	$("#takingorder").css({display: "none"});
            	e.preventDefault();
            	return;
          	} 
          	else {
            	$("#quote_item_unit_price").parent().next(".validation").remove();
          	}

          	// item description
          	if(!$('#quote_item_des').val()){
            	if($("#quote_item_des").parent().next(".validation").length == 0){
              		$("#quote_item_des").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter item description</div>");
            	}

            	$("#takingorder").css({display: "none"});
            	e.preventDefault();
            	return;
          	} 
          	else {
            	$("#quote_item_des").parent().next(".validation").remove();
          	}

          	var formData = new FormData($(this)[0]);
          		$.ajax({
              		url: "<?php echo site_url('admin/salesorder/add_quote_item_ajax'); ?>",
              		type: "POST",
              		data: formData,
              		async: false,
              
              		beforeSend : function(msg){ 
              			$("#takingorder").css({display: "block"}); 
              		},
              		
              		success: function (msg) {
                		$("#takingorder").css({display: "none"});
                		$('#new_staff').animate({ scrollTop: 0 }, 200);
        				$("#quote_item_ajax").html(msg);
                  		$("form[name='quote_item_form']").find("input[type=text], textarea, input[type=number]").val("");
              		},
              		
              		cache: false,
              		contentType: false,
              		processData: false
          		});

          	e.preventDefault();
		});
	});
</script>

<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
	<input type="hidden" id="quote_order_number" value="<?php echo $orderno; ?>" />
	<input type="hidden" id="quote_order_number_airway" value="<?php echo $order->airwaybill_no; ?>" />
    <!--<div class="header">
        <h2><strong><?php echo $order->airwaybill_no;?></strong></h2> 
        <div class="breadcrumb-wrapper"></div>                
  	</div>-->

  	<div class="row">
		<div class="panel"> 
			<div class="panel-content">
				<ul class="nav nav-tabs">
            		<li class="active"><a href="#tab_summary_information" data-toggle="tab">Summary</a></li>
                    <li class=""><a href="#tab_quotation_and_invoice_information" data-toggle="tab">Quotation & Invoice Information</a></li>
                    <li class=""><a href="#tab_resource_information" data-toggle="tab">Resource Information</a></li>
              	</ul>
              	<div class="tab-content">
              		<div class="tab-pane fade active in" id="tab_summary_information">
						<h5>Summary Information for Order: <b><?php echo $order->airwaybill_no;?></b></h5>
						<hr/>
						<div class="row">
							<div class="col-sm-6">
								<div class="row">
		                        	<div class="col-sm-12">
		                          	  	<div class="form-group">
		                              		<label class="col-sm-4 control-label"><i class="fa fa-user"></i>Customer</label>
		                              		<div class="col-sm-8 append-icon">  
						                    	<?php echo $this->orders_model->getcompanyname($order->customer_customer_id); ?><br/>
				                          		<?php echo $this->orders_model->getcompanyaddress($order->customer_customer_id); ?>     
			                              	</div>
		                              	</div>
									</div>
								</div> 
							 	
							 	<div class="row">
			                 		<div class="col-sm-12">
		                      	  		<div class="form-group">
		                              		<label class="col-sm-4 control-label"><i class="fa fa-calendar"></i>Date</label>
			                              	<div class="col-sm-8 append-icon">
		                                 		<?php echo date('m/d/Y H:i', $order->order_date); ?> 
			                              	</div>
		                              	</div>
									</div>
								</div> 
							</div>
							<div class="col-sm-6">
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label class="col-sm-4 control-label"><i class="fa fa-bullhorn"></i>Status</label>
		                              		<div class="col-sm-8 append-icon">  
		                              			<?php
		                              			$status = $order->order_status;
            									$m_status = '';

									            if($status == 1){
									            	$m_status = '<span class="btn" style="background-color: #00b0f0; color:#ffffff; ">NEW</span>';
									            }
									            elseif($status == 2){
									                $m_status = '<span class="btn" style="background-color: #ffc000; color:#ffffff; ">PENDING</span>';
									            }
									            elseif($status == 3){
									                $m_status = '<span class="btn" style="background-color: #ff99cc; color:#ffffff; ">ACTION NEEDED</span>';
									            }
									            elseif($status == 4){
									                $m_status = '<span class="btn" style="background-color: #008000; color:#ffffff; ">COMPLETE</span>';
									            }
									            elseif($status == 5){
									                $m_status = '<span class="btn" style="background-color: #f2a057; color:#ffffff; ">LIVE</span>';
									            }

									            echo $m_status;
		                              			?>
	                              			</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label class="col-sm-4 control-label"><i class="icon-note"></i>Notes</label>
		                              		<div class="col-sm-8 append-icon">  
		                              			<?php echo $order->comment; ?>
	                              			</div>
										</div>
									</div>
								</div>
							</div>
		                </div>

		                <br/>
		                <!--<h5><b>Documents</b></h5>-->
						<hr/>
						<h5><b>Details</b></h5>
						<hr/>
						<div class="row">
							<div class="col-sm-12">
								<div class="row">
									<div class="form-group">
										<div class="col-sm-12 append-icon"> 
											<table class="table" id="airwaybill_items">
									            <thead>
													<tr style="font-size: 12px;">
														<td>STOP</td>
														<td>CONTACT PERSON</td>
														<td>PICKUP</td>
														<td>DELIVERY</td>
														<td>QUANTITY</td>
														<td>PACKAGE</td>
													</tr>
									            </thead>
									            <tbody id="InputsWrapper">

									            </tbody>
				          					</table>
			          					</div>
		          					</div>
	          					</div>
          					</div>
						</div>
						<hr/>
						<h5><b>Documents</b></h5>
						<div class="row">
							<div class="col-sm-12">
								<div class="row">
									<div class="form-group">
										<div class="col-sm-12 append-icon">
											<table class="table">
												<!--<thead>
						                      		<tr style="font-size: 12px;">                         
							                        	<th>Documents</th>
							                        	<th></th>
						                      		</tr>
							                    </thead>-->
							                    <tbody id="InputsWrapper">
							                    	<?php if(!empty($order->order_document)){?>
							                    	<?php $documents = explode(',', $order->order_document);?>
							                    	<?php for($i = 0; $i < sizeof($documents); $i++){ ?>
							                    	<tr>
							                    		<td>
							                    			<a href="<?php echo base_url('uploads/products'); ?>/<?php echo $documents[$i]; ?>">Doc <?php echo $i+1; ?></a>
						                    			</td>
							                    		<td>
							                    			<a href="<?php echo base_url('uploads/products'); ?>/<?php echo $documents[$i]; ?>" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a>
						                    			</td>
							                    	</tr>
							                    	<?php } ?>
							                    	<?php }?>
							                    </tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
              		</div>
              		<div class="tab-pane fade" id="tab_quotation_and_invoice_information">
              			<div class="row">
							<div class="col-sm-12">
								<h5><strong>Quotation & Invoice Information</strong></h5>
								<hr/>

								<div id="add_quote_item">
									<button type="submit" class="btn btn-embossed btn-primary" data-toggle="modal" data-target="#add_quotation"><i class="fa fa-plus"></i> Item</button>
									<button type="submit" class="btn btn-embossed btn-info" onclick="send_quote(<?php echo $orderno; ?>);"><i class="fa fa-paper-plane"></i> Send Quote</button>
								</div>

								<table class="table" id="quotation_table">
									<thead>
										<tr style="font-size: 12px;">
											<td>Item</td>
											<td>Desc</td>
											<td>Quantity</td>
											<td>Unit Price</td>
											<td></td>
										</tr>
									</thead>
									<tbody id="InputsWrapper">

									</tbody>
								</table>
							</div>
							<div class="row">
								<div class="col-sm-8">

								</div>
								<div class="col-sm-4">
									<div class="row">
		   		                 		<div class="col-sm-12">
		                          	  		<div class="form-group">
			                              		<label class="col-sm-6 control-label">Untaxed Amount </label>
			                              		<div class="col-sm-6 append-icon">
				                               		<span id="quote_untaxed">0</span> 
				                              	</div>
			                              	</div>
										</div>
									</div>
									<div class="row">
		   		                 		<div class="col-sm-12">
		                          	  		<div class="form-group">
			                              		<label class="col-sm-6 control-label">Taxes </label>
			                              		<div class="col-sm-6 append-icon">
			                               			<span id="quote_taxes">0</span> 
				                              	</div>
			                              	</div>
										</div>
									</div>
									<div class="row">
		   		                 		<div class="col-sm-12">
		                          	  		<div class="form-group">
			                              		<label class="col-sm-6 control-label">Total </label>
				                              	<div class="col-sm-6 append-icon">
			                               			<span id="quote_total">0</span>
				                              	</div>
			                              	</div>
										</div>
									</div>	
								</div>
							</div>
						</div>
              		</div>
              		<div class="tab-pane fade" id="tab_resource_information">
              			<h5><strong>Actions</strong></h5>
              			<a href="" class="btn btn-primary" data-toggle="modal" data-target="#add_driver"><i class="fa fa-plus"></i> Driver</a>
              			<a href="" class="btn btn-primary" data-toggle="modal" data-target="#add_vehicle"><i class="fa fa-plus"></i> Vehicle</a>
              			<a href="" class="btn btn-primary" data-toggle="modal" data-target="#add_security"><i class="fa fa-plus"></i> Security</a>
						<hr/>
						<div class="row">
							<div class="col-sm-12">
								<table class="table" id="resources_table">
									<thead>
										<tr style="font-size: 12px;">
											<td>Name</td>
											<td>Desc</td>
											<td>Quantity</td>
											<td>Image</td>
											<td></td>
										</tr>
									</thead>
									<tbody id="InputsWrapper">

									</tbody>
								</table>
							</div>
						</div>
              		</div>
              	</div>
			</div>
		</div>
  	</div>
  	<div class="row">
  		<div class="row">
			<div class="col-sm-12">
				<div class="panel">
					<div class="panel-content">
						<div class="row">&nbsp;</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="row">
									<table width="100%" style="border-collapse:separate; border-spacing:0 1em; padding: 40px;">
		          						<tr>
		            						<td colspan="2">
		              							<center><h4 class="text-center"><strong>SHIPMENT REQUEST DELIVERY SUMMARY SHEET</strong></h4></center>
		            						</td>
		          						</tr>
		          						
		          						<tr>
		            						<td><img class="pull-left" alt="Barcode" src="<?php echo site_url('admin/orders/generate_bar_code').'/'.$order->airwaybill_no; ?>" /></td>
		        							<td>
		          								<img class="pull-right" align="Company logo" src="<?php echo base_url('uploads/site/logo.png'); ?>" />
		            						</td>
		          						</tr>
		          						
		          						<tr>
		            						<td colspan="2">
		              							<center><h4 class="text-center"><strong>SHIPMENT REQUEST DETAILS</strong></h4></center>
		            						</td>
		          						</tr>
		          
		          						<tr>
		            						<td colspan="2">
		            							<div id="pr_trip_map" style="width:100%; height:500px;" style="z-index: 20;"></div>
		            						</td>
		          						</tr>
		        					</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
  	</div>
</div><!-- END PAGE CONTENT -->

<div class="modal" id="add_driver" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
          		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
	         	<h4 class="modal-title"><strong>Assign Driver</strong></h4>
       		</div>

       		<div id="driver_ajax" style="color:red;margin-left:15px;"> 
          		<?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');}?>         
        	</div>

        	<form id="assign_driver" name="assign_driver" class="form-validation" accept-charset="utf-8" method="post">
        		<div class="modal-body">
        			<table class="table table-hover table-dynamic">
              			<thead>
                			<tr>
                  				<th>Name</th>
                  				<th>Position</th>
                  				<th>Image</th>
                  				<th></th>     
            				</tr>
          				</thead>
          				<tbody>
		        			<?php foreach($drivers->result() as $driver){ ?>
		        			<tr>
		        				<td><?php echo $driver->fname.' '.$driver->lname; ?></td>
		        				<td><?php echo $driver->job; ?></td>
		        				<td><img src="<?php echo base_url('uploads'); ?>/<?php echo $driver->avatar; ?>" alt="user image" style="height: 100px;width: 100px;"></td>
		        				<td>
		        					<a href="#" class="edit btn btn-sm btn-default dlt_sm_table" onclick="assigndriver(<?php echo $driver->staff_id.','.$orderno; ?>)"><i class="icon-note"></i>Assign</a> 
		        				</td>
		        			</tr>
		        			<?php } ?>
	        			</tbody>
        			</table>
        		</div>
        	</form>
		</div>
	</div>
</div>

<div class="modal" id="add_vehicle" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
          		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
	         	<h4 class="modal-title"><strong>Assign Vehicle</strong></h4>
       		</div>
       		<div id="vehicle_ajax" style="color:red;margin-left:15px;"> 
          		<?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');}?>         
        	</div>
        	<form id="assign_vehicle" name="assign_vehicle" class="form-validation" accept-charset="utf-8" method="post">
        		<div class="modal-body">
        			<table class="table table-hover table-dynamic">
              			<thead>
                			<tr>
                  				<th>Reg No</th>
                  				<th>Type</th>
                  				<th>Image</th>
                  				<th></th>     
            				</tr>
          				</thead>
          				<tbody>
		        			<?php foreach($vehicles->result() as $vehicle){ ?>
		        			<tr>
		        				<td><?php echo $vehicle->registration; ?></td>
		        				<td>
		        					<?php 
				                    if($vehicle->type == 1){
				                      echo 'Trailer';
				                    }
				                    else if($vehicle->type == 2){
				                      echo 'Lorry';
				                    }
				                    else if($vehicle->type == 3){
				                      echo 'Pickup';
				                    }
				                    else{
				                      echo '';
				                    }
				                    ?>
		        				</td>
		        				<td><img src="<?php echo base_url('uploads/resources'); ?>/<?php echo $vehicle->avatar; ?>" alt="user image" style="height: 150px;width: 200px;"></td>
		        				<td>
		        					<a href="#" class="edit btn btn-sm btn-default dlt_sm_table" onclick="assignvehicle(<?php echo $vehicle->id.','.$orderno; ?>)"><i class="icon-note"></i>Assign</a> 
		        				</td>
		        			</tr>
		        			<?php } ?>
	        			</tbody>
        			</table>
        		</div>
        	</form>
		</div>
	</div>
</div>

<div class="modal" id="add_security" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
          		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
	         	<h4 class="modal-title"><strong>Assign Security</strong></h4>
       		</div>
       		<div id="security_ajax" style="color:red;margin-left:15px;"> 
          		<?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');}?>         
        	</div>
        	<form id="assign_security" name="assign_security" class="form-validation" accept-charset="utf-8" method="post">
        		<div class="modal-body">
        			<table class="table table-hover table-dynamic">
              			<thead>
                			<tr>
                  				<th>Name</th>
                  				<th>Position</th>
                  				<th>Image</th>
                  				<th></th>     
            				</tr>
          				</thead>
          				<tbody>
		        			<?php foreach($securities->result() as $security){ ?>
		        			<tr>
		        				<td><?php echo $security->fname.' '.$security->lname; ?></td>
		        				<td><?php echo $security->job; ?></td>
		        				<td><img src="<?php echo base_url('uploads'); ?>/<?php echo $security->avatar; ?>" alt="user image" style="height: 100px;width: 100px;"></td>
		        				<td>
		        					<a href="#" class="edit btn btn-sm btn-default dlt_sm_table" onclick="assignsecurity(<?php echo $security->staff_id.','.$orderno; ?>)"><i class="icon-note"></i>Assign</a> 
		        				</td>
		        			</tr>
		        			<?php } ?>
	        			</tbody>
        			</table>
        		</div>
        	</form>
		</div>
	</div>
</div>

<div class="modal" id="add_quotation" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
          		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
	         	<h4 class="modal-title"><strong>Add Quotation Item</strong></h4>
       		</div>
       		<div id="quote_item_ajax" style="color:red;margin-left:15px;"> 
          		<?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');}?>         
        	</div>
        	<form id="quote_item_form" name="quote_item_form" class="form-validation" accept-charset="utf-8" method="post">
        		<div class="modal-body">
        			<div class="row">
        				<div class="col-sm-12">
        					<input type="hidden" id="quote_order_number_new" name="quote_order_number_new" value="<?php echo $orderno; ?>" />
              				<div class="form-group">
                				<label class="control-label">Name</label>
                				<div class="append-icon">
                  					<input type="text" name="quote_item_name" value="" class="form-control" id="quote_item_name">
                				</div>
              				</div>
            			</div>
            			<div class="col-sm-12">
              				<div class="form-group">
                				<label class="control-label">Quantity</label>
                				<div class="append-icon">
                  					<input type="number" name="quote_item_quantity" value="" class="form-control" id="quote_item_quantity">
                				</div>
              				</div>
            			</div>
            			<div class="col-sm-12">
              				<div class="form-group">
                				<label class="control-label">Unit Price</label>
                				<div class="append-icon">
                  					<input type="number" name="quote_item_unit_price" value="" class="form-control" id="quote_item_unit_price">
                				</div>
              				</div>
            			</div>
            			<div class="col-sm-12">
              				<div class="form-group">
	                			<label class="control-label">Desc</label>
	                			<div class="append-icon">
	                  				<textarea name="quote_item_des" rows="4" class="form-control" id="quote_item_des"></textarea>
	                			</div>
              				</div>
            			</div>
        			</div>
        			<div class="text-left  m-t-20">
        				<div id="submitquoteitem"><button type="submit" class="btn btn-embossed btn-primary">Save</button></div>
          			</div>
        		</div>
        	</form>
		</div>
	</div>
</div>