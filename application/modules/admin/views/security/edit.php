<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js">
</script>
<script>
	$(document).ready(function(){
		$("form[name='update_security']").submit(function(e){
			// firstname
			if(!$('#first_name').val()){
            	if($("#first_name").parent().next(".validation").length == 0){
              		$("#first_name").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>firstname cannot be empty</div>");
            	}
            	e.preventDefault();
            	return;
          	} 
          	else {
            	$("#first_name").parent().next(".validation").remove();
          	}

          	// lastname
			if(!$('#last_name').val()){
            	if($("#last_name").parent().next(".validation").length == 0){
              		$("#last_name").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>lastname cannot be empty</div>");
            	}
            	e.preventDefault();
            	return;
          	} 
          	else {
            	$("#last_name").parent().next(".validation").remove();
          	}

          	// phone number
			if(!$('#phone_number').val()){
            	if($("#phone_number").parent().next(".validation").length == 0){
              		$("#phone_number").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>phone number cannot be empty</div>");
            	}
            	e.preventDefault();
            	return;
          	} 
          	else {
            	$("#phone_number").parent().next(".validation").remove();
          	}

          	// email address
			if(!$('#email').val()){
            	if($("#email").parent().next(".validation").length == 0){
              		$("#email").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>email address cannot be empty</div>");
            	}
            	e.preventDefault();
            	return;
          	} 
          	else {
            	$("#email").parent().next(".validation").remove();
          	}

          	// job title
			if(!$('#jobtitle').val()){
            	if($("#jobtitle").parent().next(".validation").length == 0){
              		$("#jobtitle").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>job position cannot be empty</div>");
            	}
            	e.preventDefault();
            	return;
          	} 
          	else {
            	$("#jobtitle").parent().next(".validation").remove();
          	}

          	// checking that passwords 1 and 2 match
          	$pass1 = $('#password1').val();
          	$pass2 = $('#password2').val();

          	if($pass1 != $pass2){
          		$("#password1").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>passwords do not match</div>");
          		e.preventDefault();
            	return;
          	}
          	else{
          		$("#password1").parent().next(".validation").remove();
          	}

          	var formData = new FormData($(this)[0]);
          	
          	$.ajax({
          		url: "<?php echo site_url('admin/security/update_process'); ?>",
	            type: "POST",
	            data: formData,
	            async: false,
	            success: function (msg) {
					$('body,html').animate({ scrollTop: 0 }, 200);
	            	$("#security_ajax").html(msg); 
	        	},
	            cache: false,
	            contentType: false,
	            processData: false
      		});

			e.preventDefault();
		});
	});
</script>
<div class="page-content page-thin">
	<div class="row">
		<div class="col-md-7">
			<div class="panel">
				<div class="panel-content">
					<h4><strong>Security Staff Details</strong></h4>
      				<hr/>
      				<div id="security_ajax"> 
        				<?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');}?>         
      				</div>
      				
      				<form id="update_security" name="update_security" class="form-validation" accept-charset="utf-8" enctype="multipart/form-data" method="post">
        				<div class="modal-body">
			            	<div class="col-sm-12">
			            		<input type="hidden" name="user_id" value="<?php echo $security->staff_id; ?>" />
			              		<div class="form-group">
					                <label class="control-label">Firstname</label>
					                <div class="append-icon">
				                  		<input type="text" name="first_name" class="form-control" id="first_name" value="<?php echo $security->fname; ?>">
				                  		<i class="icon-user"></i>
					                </div>
				              	</div>
				            </div>
            				
            				<div class="col-sm-12">
              					<div class="form-group">
                					<label class="control-label">Lastname</label>
					                <div class="append-icon">
				                  		<input type="text" name="last_name" class="form-control" id="last_name" value="<?php echo $security->lname; ?>">
				                  		<i class="icon-user"></i>
					                </div>
              					</div>
            				</div>                               
            				
            				<div class="col-sm-12">
              					<div class="form-group">
                					<label class="control-label">Phone Number</label>
                					<div class="append-icon">
                  						<input type="text" name="phone_number" class="form-control" id="phone_number" value="<?php echo $security->phone; ?>">
              							<i class="icon-screen-smartphone"></i>
                					</div>
              					</div>
            				</div>

            				<div class="col-sm-12">
              					<div class="form-group">
	                				<label class="control-label">Email Address</label>
	                				<div class="append-icon">
	                  					<input type="email" name="email" class="form-control" id="email" value="<?php echo $security->email; ?>">
	                  					<i class="icon-envelope"></i>
	                				</div>
              					</div>
            				</div>
            
            				<div class="col-sm-12">
              					<div class="form-group">
                  					<label class="control-label">Job Title</label>
                  					<div class="append-icon">
                    					<input type="jobtitle" name="jobtitle" class="form-control" id="jobtitle" value="<?php echo $security->job; ?>">
                    					<i class="icon-envelope"></i>
                  					</div>
              					</div>
            				</div>
            				
            				<div class="col-sm-12">
              					<div class="form-group">
                					<label class="control-label">Active</label>
                					<div class="append-icon">
                  						<?php $status = ($security->status == 1) ? 'checked' : ''; ?>
              							<input type="checkbox" name="status" value="<?php echo $security->status; ?>" <?php echo $status; ?> data-checkbox="icheckbox_square-blue"/>
                					</div>
              					</div>
            				</div>
            				<div class="col-sm-12">
              					<div class="form-group">
                					<label class="control-label">Password</label>
                  					<div class="append-icon">
                    					<input type="password" name="pass1" id="password1" value="" class="form-control">
                    					<i class="icon-lock"></i>
                  					</div>
              					</div>
            				</div>
            
            				<div class="col-sm-12">
              					<div class="form-group">
                					<label class="control-label">Repeat Password</label>
					                <div class="append-icon">
				                  		<input type="password" name="pass2" id="password2" value="" class="form-control">
				                  		<i class="icon-lock"></i>
					                </div>
              					</div>
            				</div>
            
            				<div class="col-sm-12">
              					<div class="form-group">
                					<label class="control-label">Security Image</label>
                					<div class="append-icon">
                  						<div class="file">
                    						<div class="option-group">
                      							<span class="file-button btn-primary">Choose File</span>
                      							<input type="file" class="custom-file" name="user_avatar" id="user_avatar" onchange="document.getElementById('uploader').value = this.value;">
                      							<input type="text" class="form-control" id="uploader" placeholder="no file selected" readonly="">
                    						</div>
                  						</div>
                					</div>
              					</div>
            				</div>
          					<div class="text-left  m-t-20">
            					<div id="submitbutton"><button type="submit" class="btn btn-embossed btn-primary">Update</button></div>
          					</div>
        				</div>
      				</form>
				</div>
			</div>
		</div>
		<div class="col-md-5">
			<div class="panel">
				<div class="panel-content">
					<h4><strong>Security Staff Image</strong></h4>
					<hr/>
					<img src="<?php echo base_url('uploads'); ?>/<?php echo $security->avatar; ?>" alt="user image" style="height: 100%; width: 100%;">
				</div>
			</div>
		</div>
	</div>
</div>