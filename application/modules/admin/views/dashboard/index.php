<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>-->

<!--<script type="text/javascript">
$(function(){
    $('.marquee').marquee({
    showSpeed:1000, //speed of drop down animation
    scrollSpeed: 10, //lower is faster
    yScroll: 'bottom',  // scroll direction on y-axis 'top' for down or 'bottom' for up
    direction: 'left', //scroll direction 'left' or 'right'
    pauseSpeed: 1000, // pause before scroll start in milliseconds
    duplicated: true  //continuous true or false
    });
});
</script> -->

<!-- BEGIN PAGE CONTENT -->
<div class="page-content page-thin">
    <div class="row">
        <div class=	"widget-infobox" style="padding-bottom:15px; padding-left:6px;">
            <div class="row"></div>
            <h2><b style="text-transform: uppercase;"><?php echo config('site_name'); ?></b> - Transporter Dashboard</h2> 
    							
    		<a href="<?php echo site_url('admin/orders/'); ?>">     
         		<div class="infobox"> 
                    <div class="left"> 
                        <i class="fa fa-shopping-cart" style="background-color:#00b0f0 !important;"></i> 
                    </div>                                 
                    <div class="right"> 
                        <div> 
                            <span class="pull-left" style="color: #00b0f0;" id="dashboard_new_orders">0</span> 
                            <br> 
                        </div>                                     
                        <div class="txt" style="color: #00b0f0;">New Orders</div>                                     
                    </div>                                 
                </div>
    		</a>
    							
    		<a href="<?php echo site_url('admin/orders/pending'); ?>"> 
                <div class="infobox"> 
                    <div class="left"> 
                        <i class="fa fa-shopping-cart" style="background-color:#ffc000 !important;"></i> 
                    </div>                                 
                    <div class="right"> 
                        <div> 
                            <span class="pull-left" style="color: #ffc000;" id="dashboard_pending_orders">0</span> 
                            <br> 
                        </div>                                     
                        <div class="txt" style="color: #ffc000;">Pending Orders</div>                                     
                    </div>                                 
                </div>
    		</a>
    							
    		<a href="<?php echo site_url('admin/orders/action'); ?>"> 
                <div class="infobox"> 
                    <div class="left"> 
                        <i class="fa fa-shopping-cart" style="background-color:#ff99cc !important;"></i> 
                    </div>                                 
                    <div class="right"> 
                        <div class="clearfix"> 
                            <div> 
                                <span class="pull-left" style="color: #ff99cc;" id="dashboard_action_needed">0</span> 
                                <br> 
                            </div>                                         
                            <div class="txt" style="color: #ff99cc;">Action Needed</div>                                         
                        </div>                                     
                    </div>                                 
                </div>
    		</a>
    							
    		<a href="<?php echo site_url('admin/orders/complete'); ?>"> 
                <div class="infobox"> 
                    <div class="left"> 
                        <i class="fa fa-shopping-cart" style="background-color:#008000 !important;"></i> 
                    </div>                                 
                    <div class="right"> 
                        <div class="clearfix"> 
                            <div> 
                                <span class="pull-left" style="color: #008000;" id="dashboard_complete_orders">0</span> 
                                <br> 
                            </div>                                         
                            <div class="txt" style="color: #008000;">Complete Orders</div>                                         
                        </div>                                     
                    </div>                                 
                </div>
    		</a>

            <a href="<?php echo site_url('admin/orders/live'); ?>"> 
                <div class="infobox"> 
                    <div class="left"> 
                        <i class="fa fa-shopping-cart" style="background-color:#f2a057 !important;"></i> 
                    </div>                                 
                    <div class="right"> 
                        <div class="clearfix"> 
                            <div> 
                                <span class="pull-left" style="color: #f2a057;" id="dashboard_live_orders">0</span> 
                                <br> 
                            </div>                                         
                            <div class="txt" style="color: #f2a057;">Live Orders</div>                                         
                        </div>                                     
                    </div>                                 
                </div>
            </a>
    							
    		<!--<a href="<?php echo site_url('admin/#/'); ?>"> 
                <div class="infobox"> 
                    <div class="left"> 
                        <i class="icon-tag bg-orange"></i> 
                    </div>                                 
                    <div class="right"> 
                        <div class="clearfix"> 
                            <div> 
                                <span class="c-orange pull-left">0</span> 
                                <br> 
                            </div>                                         
                            <div class="txt">Quotations</div>                                         
                        </div>                                     
                    </div>                                 
                </div>
    		</a>-->
        </div>
    </div>                
</div>
<!-- END PAGE CONTENT --> 