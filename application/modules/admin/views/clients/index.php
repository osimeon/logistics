<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/1.12.0/semantic.min.css">
<style type="text/css">
    #takingorder{
        position:fixed;
        top:0px;
        right:0px;
        width:100%;
        height:100%;
        background-color:#666;
        background-image:url('http://pulse.sindlab.com.pk//images/ajax-loading.gif'); 
        background-repeat:no-repeat;
        background-position:center;
        z-index:10000000;  
        opacity: 0.4;
        filter: alpha(opacity=40); /* For IE8 and earlier */
    }
</style>
<script>
function deactivate_users(staff_id){
    $.ajax({
      type: "GET",
      url: "<?php echo site_url('admin/clients/deactivate' ); ?>/" + staff_id,
      success: function(msg){
        if(msg == 'deleted'){
          $('#staff_id_' + staff_id).fadeOut('normal');
        }
      }
    });
 }
</script>

<div class="page-content page-thin">
  <div class="header">
    <h2><strong>Clients</strong></h2>            
  </div>
  <div class="row">
 	  <div class="col-lg-12">
      <div class="panel">
        <div class="panel-header">
          <h3><i class="fa fa-table"></i> <strong>Manage </strong> Clients</h3>
        </div>
        <div class="panel-content"> 
          <div class="m-b-20">
            <div> 
              <?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');}?>         
            </div>
          </div>

          <div class="panel-content pagination2 table-responsive">
            <table class="table table-hover table-dynamic filter-between_date" id="clients_list">
              <thead>
                <tr>                        
                  <th><?php echo $this->lang->line('name'); ?></th>
                  <th><?php echo $this->lang->line('email'); ?></th>
                  <th>ADDRESS</th>
                  <th>WEBSITE</th>
                  <th>PHONE</th>
                  <th><?php echo $this->lang->line('register_time'); ?></th>         
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
				</div>
      </div>
    </div>
  </div>
</div>   

<div id="takingorder" style="display: none;"></div> 