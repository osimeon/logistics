<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/1.12.0/semantic.min.css">
<script>
function delete_resource(resource_id){
    var result = confirm("are you sure you want to delete vehicle?");
    
    if(result){
      $.ajax({
        type: "GET",
        url: "<?php echo site_url('admin/resources/delete' ); ?>/" + resource_id,
        success: function(msg){
          if(msg == 'deleted'){
            $('#staff_id_' + staff_id).fadeOut('normal');
          }
        }
      });
    }
 }
</script>

<script>
  $(document).ready(function() {
    $("form[name='add_resource']").submit(function(e) {
          //Name
          if(!$('#resource_name').val()){
            if ($("#resource_name").parent().next(".validation").length == 0){
              $("#resource_name").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please vehicle name</div>");
            }

            e.preventDefault();
            return;
          } 
          else {
            $("#resource_name").parent().next(".validation").remove();
          }

          //Registration Number
          if(!$('#resource_regno').val()){
            if ($("#resource_regno").parent().next(".validation").length == 0){
              $("#resource_regno").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please vehicle registration</div>");
            }

            e.preventDefault();
            return;
          } 
          else {
            $("#resource_regno").parent().next(".validation").remove();
          }

          //Description
          if(!$('#resource_description').val()){
            if ($("#resource_description").parent().next(".validation").length == 0){
              $("#resource_description").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please vehicle Description</div>");
            }

            e.preventDefault();
            return;
          } 
          else {
            $("#resource_description").parent().next(".validation").remove();
          }

          //Type
          if(!$('#resource_type').val()){
            if ($("#resource_type").parent().next(".validation").length == 0){
              $("#resource_type").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please select vehicle type</div>");
            }

            e.preventDefault();
            return;
          } 
          else {
            $("#resource_type").parent().next(".validation").remove();
          }

          //Avatar
          if(!$('#resource_avatar').val()){
            if ($("#resource_avatar").parent().next(".validation").length == 0){
              $("#resource_type").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please select vehicle image</div>");
            }

            e.preventDefault();
            return;
          } 
          else {
            $("#resource_type").parent().next(".validation").remove();
          }

          var formData = new FormData($(this)[0]);
          $.ajax({
              url: "<?php echo site_url('admin/resources/add_process'); ?>",
              type: "POST",
              data: formData,
              async: false,
              beforeSend : function(msg){ $("#submitbutton").html('<img src="<?php echo base_url('public/images/loading.gif'); ?>" />'); },
              success: function (msg) {
                console.log(msg);
                $('#new_staff').animate({ scrollTop: 0 }, 200);
                $("#staff_ajax").html(msg); 
                $("#staff_ajax").delay(500).fadeOut();
                $("#submitbutton").html('<button type="submit" class="btn btn-embossed btn-primary">Save</button>');
                $("#add_resource").trigger("reset");
              },
              cache: false,
              contentType: false,
              processData: false
          });

          e.preventDefault();
      });
  });
 </script>

<!-- BEGIN PAGE CONTENT -->
<div class="page-content page-thin">
  <div class="header">
    <h2><strong>Vehicles</strong></h2>            
  </div>
  <div class="row">
 	  <div class="col-lg-12">
      <div class="panel">
        <div class="panel-header">
          <h3><i class="fa fa-table"></i> <strong>Manage </strong> Vehicles</h3>
        </div>
        <div class="panel-content"> 
          <div class="m-b-20">
            <div class="btn-group">
              <a href="javascript:void(0)" class="btn btn-embossed btn-primary" data-toggle="modal" data-target="#new_resource"><i class="fa fa-plus"></i> Add New</a>
            </div>
            <div> 
              <?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');}?>         
            </div>
          </div>

          <div class="panel-content pagination2 table-responsive">
            <table class="table table-hover table-dynamic filter-between_date" id="vehicles_list">
              <thead>
                <tr>                        
                  <th><?php echo $this->lang->line('name'); ?></th>
                  <th>REG NO</th>
                  <th>DESCRIPTION</th>
                  <th>TYPE</th>
                  <th>ACTIVE</th>
                  <th><?php echo $this->lang->line('register_time'); ?></th>
                  <th>IMAGE</th>
                  <th><?php echo $this->lang->line('options'); ?></th>    
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
				</div>
      </div>
    </div>
  </div>
</div>   

<div id="takingorder" style="display: none;"></div>    

<div class="modal fade" id="new_resource" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
        <h4 class="modal-title"><strong>Add Vehicle</strong></h4>
      </div>
      <div id="staff_ajax"> 
        <?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');}?>         
      </div>
      <form id="add_resource" name="add_resource" class="form-validation" accept-charset="utf-8" enctype="multipart/form-data" method="post">
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Name</label>
                <div class="append-icon">
                  <input type="text" name="resource_name" value="" class="form-control" id="resource_name">
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Registration Number</label>
                <div class="append-icon">
                  <input type="text" name="resource_regno" value="" class="form-control" id="resource_regno">
                </div>
              </div>
            </div>
          </div>
          <div class="row">                                
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Type</label>
                <div class="append-icon">
                  <select name="resource_type" id="resource_type" class="form-control" data-search="true">
                      <option value=""></option>
                      <option value="2">Lorry</option>
                      <option value="3">Pickup</option>
                      <option value="1">Trailer</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Active</label>
                <div class="append-icon">
                  <input type="checkbox" name="status" value="1" checked data-checkbox="icheckbox_square-blue"/> 
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label class="control-label">Description</label>
                <div class="append-icon">
                  <textarea name="resource_description" rows="4" class="form-control" id="resource_description"></textarea>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label class="control-label">Image</label>
                <div class="append-icon">
                  <div class="file">
                    <div class="option-group">
                      <span class="file-button btn-primary">Choose File</span>
                      <input type="file" class="custom-file" name="resource_avatar" id="resource_avatar" onchange="document.getElementById('uploader').value = this.value;">
                      <input type="text" class="form-control" id="uploader" placeholder="no file selected" readonly="">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="text-left  m-t-20">
            <div id="submitbutton"><button type="submit" class="btn btn-embossed btn-primary">Create</button></div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>