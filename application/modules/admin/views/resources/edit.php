<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>
	$(document).ready(function(){
		$("form[name='update_vehicle']").submit(function(e){
			// vehicle name
          	if(!$('#vehicle_name').val()){
            	if($("#vehicle_name").parent().next(".validation").length == 0){
              		$("#vehicle_name").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>vehicle name cannot be empty</div>");
            	}

            	e.preventDefault();
            	return;
          	} 
          	else {
            	$("#vehicle_name").parent().next(".validation").remove();
          	}

          	// registration Number
          	if(!$('#vehicle_registration').val()){
            	if($("#vehicle_registration").parent().next(".validation").length == 0){
              		$("#vehicle_registration").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>vehicle registration cannot be empty</div>");
            	}

            	e.preventDefault();
            	return;
          	} 
          	else{
            	$("#vehicle_registration").parent().next(".validation").remove();
          	}

          	// description
          	if(!$('#vehicle_description').val()){
            	if($("#vehicle_description").parent().next(".validation").length == 0){
              		$("#vehicle_description").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>vehicle description cannot be empty</div>");
            	}

            	e.preventDefault();
            	return;
          	} 
          	else {
            	$("#vehicle_description").parent().next(".validation").remove();
          	}

          	var formData = new FormData($(this)[0]);
          	$.ajax({
          		url: "<?php echo site_url('admin/resources/update_process'); ?>",
              	type: "POST",
              	data: formData,
              	async: false,
              	beforeSend : function(msg){ 
              		$("#vehicle_submitbutton").html('<img src="<?php echo base_url('public/images/loading.gif'); ?>" />'); 
              	},
              	success: function (msg){
                	console.log(msg);
                	
                	$('#update_vehicle').animate({ scrollTop: 0 }, 200);
                	$("#vehicle_ajax").html(msg); 
                	$("#vehicle_submitbutton").html('<button type="submit" class="btn btn-embossed btn-primary">Update</button>');
                  	window.location.reload();
              },
          		cache: false,
          		contentType: false,
          		processData: false
      		});

          	e.preventDefault();
		});
	});
</script>

<!-- BEGIN PAGE CONTENT -->
<div class="page-content page-thin">
	<div class="row">
		<div class="col-md-7">
			<div class="panel">
				<div class="panel-content">
					<h4><strong>Vehicle Details</strong></h4>
					<hr/>
					<div id="vehicle_ajax">                      	                       
	                	<?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');}?>                     	
	              	</div>

	              	<form id="update_vehicle" name="update_vehicle" class="form-validation" accept-charset="utf-8" enctype="multipart/form-data" method="post">
	              		<input type="hidden" name="vehicle_id" value="<?php echo $vehicle->id; ?>" />
	              		<div class="row">
	              			<div class="col-sm-12">
	              				<div class="form-group">
                              		<label class="control-label">Name</label>
                              		<div class="append-icon">
	                                	<input type="text" name="vehicle_name" value="<?php echo $vehicle->name; ?>" class="form-control" id="vehicle_name">
	                              	</div>
	                            </div>
	              			</div>
	              			<div class="col-sm-12">
	              				<div class="form-group">
                              		<label class="control-label">Registration Number</label>
                              		<div class="append-icon">
	                                	<input type="text" name="vehicle_registration" value="<?php echo $vehicle->registration; ?>" class="form-control" id="vehicle_registration">
	                              	</div>
	                            </div>
	              			</div>
	              			<div class="col-sm-12">
              					<div class="form-group">
                					<label class="control-label">Type</label>
                					<div class="append-icon">
                						<?php $type = $vehicle->type; ?>
                  						<select name="vehicle_type" id="vehicle_type" class="form-control" data-search="true">
                      						<option value=""></option>
                      						<option value="2" <?php echo $type == '2' ? 'selected' : '';?>>Lorry</option>
                      						<option value="3" <?php echo $type == '3' ? 'selected' : '';?>>Pickup</option>
                      						<option value="1" <?php echo $type == '1' ? 'selected' : '';?>>Trailer</option>
                  						</select>
                					</div>
              					</div>
            				</div>
            				<div class="col-sm-12">
              					<div class="form-group">
                					<label class="control-label">Active</label>
                					<div class="append-icon">
                  						<input type="checkbox" name="status" value="<?php echo $vehicle->status; ?>" <?php echo $vehicle->status == '1' ? 'checked' : ''; ?> data-checkbox="icheckbox_square-blue"/> 
                					</div>
              					</div>
            				</div>
	              			<div class="col-sm-12">
              					<div class="form-group">
                					<label class="control-label">Description</label>
                					<div class="append-icon">
                  						<textarea name="vehicle_description" rows="4" class="form-control" id="vehicle_description"><?php echo $vehicle->description; ?></textarea>
                					</div>
              					</div>
            				</div>
            				<div class="col-sm-12">
              					<div class="form-group">
                					<label class="control-label">Change Image</label>
                					<div class="append-icon">
                  						<div class="file">
                    						<div class="option-group">
                      							<span class="file-button btn-primary">Choose File</span>
                      							<input type="file" class="custom-file" name="resource_avatar" id="resource_avatar" onchange="document.getElementById('uploader').value = this.value;">
                      							<input type="text" class="form-control" id="uploader" placeholder="no file selected" readonly="">
                    						</div>
                  						</div>
                					</div>
              					</div>
            				</div>
            				<div class="text-left  m-t-20">
 				 				<div id="vehicle_submitbutton"><button type="submit" class="btn btn-embossed btn-primary">Update</button></div>
                			</div>
	              		</div>
	              	</form>
              	</div>
			</div>
		</div>
		<div class="col-md-5">
			<div class="panel">
				<div class="panel-content">
					<h4><strong>Vehicle Image</strong></h4>
					<hr/>
  					<img src="<?php echo base_url('uploads/resources'); ?>/<?php echo $vehicle->avatar; ?>" alt="user image" style="height: 100%; width: 100%;">
				</div>
			</div>
		</div>
	</div>
</div>