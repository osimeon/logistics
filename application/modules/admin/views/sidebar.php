<div class="sidebar">
  <div class="logopanel">
    <a href="<?php echo site_url('admin/dashboard/'); ?>"><img src="<?php echo base_url('uploads/site').'/'.config('site_logo'); ?>" alt="company logo" class="" style="height: 30px;"></a>
  </div>
  
  <div class="sidebar-inner">
    <div class="menu-title">
      Navigation 
      <!--<div class="pull-right menu-settings">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" data-delay="300"> 
        <i class="icon-settings"></i>
        </a>
        <ul class="dropdown-menu">
          <li><a href="#" id="reorder-menu" class="reorder-menu">Reorder menu</a></li>
          <li><a href="#" id="remove-menu" class="remove-menu">Remove elements</a></li>
          <li><a href="#" id="hide-top-sidebar" class="hide-top-sidebar">Hide user &amp; search</a></li>
        </ul>
      </div>-->
    </div>
      
  	<ul class="nav nav-sidebar">
      <li class=" nav-active <?php echo is_active_menu('dashboard'); ?>"><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="icon-home"></i><span>Dashboard</span></a></li>
      <li class=" nav-active <?php echo is_active_menu('customers'); ?>"><a href="<?php echo site_url('admin/clients'); ?>"><i class="icon-user"></i><span>Clients</span></a></li>
        <li class=" nav-active <?php echo is_active_menu('salesorder'); ?>"><a href="<?php echo site_url('admin/orders'); ?>"><i class="fa fa-shopping-cart"></i><span>Orders</span></a></li>

      <li class=" nav-active <?php echo is_active_menu('calendar'); ?>"><a href="<?php echo site_url('admin/calendar'); ?>"><i class="fa fa-calendar"></i><span>Schedule</span></a></li> 
      <li class=" nav-active <?php echo is_active_menu('drivers'); ?>"><a href="<?php echo site_url('admin/driver'); ?>"><i class="fa fa-truck"></i><span>Drivers</span></a></li>
      <li class=" nav-active <?php echo is_active_menu('drivers'); ?>"><a href="<?php echo site_url('admin/resources'); ?>"><i class="fa fa-folder-open"></i><span>Vehicles</span></a></li>
      <li class=" nav-active <?php echo is_active_menu('security'); ?>"><a href="<?php echo site_url('admin/security'); ?>"><i class="fa fa-anchor"></i><span>Security</span></a></li>
        <li class=" nav-active <?php echo is_active_menu('staff'); ?>"><a href="<?php echo site_url('admin/staff'); ?>"><i class="icon-users"></i><span>Staff</span></a></li>
    </ul>
  </div>
</div>