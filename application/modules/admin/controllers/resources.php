<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Resources extends CI_Controller{
    function Resources(){
 		parent::__construct();
 		$this->load->database();
 		$this->load->model("resources_model");
 		$this->load->library('form_validation');
 		check_login(); 
	}

	function edit($vehicle){
		$data["vehicle"] = $this->resources_model->getvehicle($vehicle);
		$this->load->view('header');
		$this->load->view('resources/edit', $data);
		$this->load->view('footer');
	}

	function index(){
		$data['resources'] = $this->resources_model->getresources();
		$this->load->view('header');
		$this->load->view('resources/index', $data);
		$this->load->view('footer');
	}

	function vehicles_ajax(){
		$items = $this->resources_model->getresources();

        $response = array();
        $response["items"] = array();

        foreach($items->result() as $item){
            $edit = site_url('admin/resources/edit/').'/'.$item->id;

            $status = ($item->status == 1) ? 'checked' : '';
            $base_url = base_url('uploads/resources').'/'.$item->avatar;

            $type = '';

            if($item->type == 1){
              $type = 'Trailer';
            }
            else if($item->type == 2){
              $type = 'Lorry';
            }
            else if($item->type == 3){
              $type = 'Pickup';
            }

            $tmp = array(
                'name' => $item->name,
                'registration' => $item->registration,
                'description' => $item->description,
                'type' => $type,
                'status' => '<input type="checkbox" name="status" value="1" '.$status.' data-checkbox="icheckbox_square-blue"/>',
                'register' => date('d F Y g:i a', $item->registered),
                'avatar' => "<img src=\"$base_url\" alt=\"user image\" style=\"height: 150px;width: 200px;\">",
                'options' => "<a href=\"$edit\" class=\"edit btn btn-sm btn-default dlt_sm_table\"><i class=\"icon-note\"></i></a><a class=\"delete btn btn-sm btn-danger dlt_sm_table\"><i class=\"glyphicon glyphicon-trash\" onclick=\"delete_resource($item->id)\"></i></a>"
            );

            array_push($response["items"], $tmp);
        }

        echo json_encode($response);
	}

	function get_vehicles_ajax(){
		$vehicles = $this->resources_model->getresources();
		$response = array();

		foreach($vehicles->result() as $vehicle){
			$type = $vehicle->type;
			$type_str = '';

			if($type == 1){
              $type_str = 'Trailer';
        	}
            else if($type == 2){
          		$type_str = 'Lorry';
        	}
            else if($type == 3){
          		$type_str = 'Pickup';
            }

			$response[] = array(
				'id' => $vehicle->id,
				'registration' => $vehicle->registration,
				'type' => $type_str,
				'avatar' => base_url('uploads/resources').'/'.$vehicle->avatar
			);
		}

		echo json_encode($response);
	}

	function add_process(){
		$this->form_validation->set_rules('resource_name', 'Vehicle Name', 'required|trim|xss_clean|htmlspecialchars');
		$this->form_validation->set_rules('resource_regno', 'Vehicle Registration', 'required|trim|xss_clean|htmlspecialchars');
		$this->form_validation->set_rules('resource_description', 'Vehicle Description', 'required|trim|xss_clean|htmlspecialchars');
		$this->form_validation->set_rules('resource_type', 'Vehicle Type', 'required');

		if($this->form_validation->run() == FALSE){
        	echo '<div class="alert error"><ul>' . validation_errors('<li style="color:red">','</li>') . '</ul></div>';
        }
        else{
        	if($this->resources_model->addresource()){
        		echo '<div class="alert alert-success">'.$this->lang->line('create_succesful').'</div>';
            }
            else{
                echo $this->lang->line('technical_problem');
            }
        }
	}

	function update_process(){
		$this->form_validation->set_rules('vehicle_name', 'Vehicle Name', 'required|trim|xss_clean|htmlspecialchars');
		$this->form_validation->set_rules('vehicle_registration', 'Vehicle Registration', 'required|trim|xss_clean|htmlspecialchars');
		$this->form_validation->set_rules('vehicle_description', 'Vehicle Description', 'required|trim|xss_clean|htmlspecialchars');
		$this->form_validation->set_rules('vehicle_type', 'Vehicle Type', 'required');

		if($this->form_validation->run() == FALSE){
        	echo '<div class="alert error"><ul>' . validation_errors('<li style="color:red">','</li>') . '</ul></div>';
        }
        else{
        	if($this->resources_model->updatevehicle()){
        		echo '<div class="alert alert-success">vehicle information updated succesfully!</div>';
            }
            else{
                echo $this->lang->line('technical_problem');
            }
        }
	}

	function delete($vehicle){
		if($this->resources_model->delete($vehicle)){
			echo 'deleted';
		}	
	}
}

?>