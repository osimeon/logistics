<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Security extends MX_Controller {
    function Security(){
    	parent::__construct();
		$this->load->database();
		$this->load->model("security_model");
		$this->load->library('form_validation');
         
         /*cache control*/
		$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
    	$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");  
        check_login();
    } 

    function edit($security){
        $data['security'] = $this->security_model->getdetails($security);
        $this->load->view('header');
        $this->load->view('security/edit', $data);
        $this->load->view('footer');
    }

	function index(){
		$data['securities'] = $this->security_model->getsecurity();
		$this->load->view('header');
		$this->load->view('security/index',$data);
		$this->load->view('footer');
	}

    function get_security_ajax(){
        $items = $this->security_model->getsecurity();

        $response = array();
        $response["items"] = array();

        foreach($items->result() as $item){
            $edit = site_url('admin/security/edit').'/'.$item->staff_id;

            $status = ($item->status == 1) ? 'checked' : '';
            $base_url = base_url('uploads').'/'.$item->avatar;

            $tmp = array(
                'name' => $item->fname.' '.$security->item,
                'email' => $item->email,
                'job' => $item->job,
                'status' => '<input type="checkbox" name="status" value="1" '.$status.' data-checkbox="icheckbox_square-blue"/>',
                'register' => date('d F Y g:i a', $item->register_time),
                'avatar' => "<img src=\"$base_url\" alt=\"user image\" style=\"height: 100px;width: 100px;\">",
                'options' => "<a href=\"$edit\" class=\"edit btn btn-sm btn-default dlt_sm_table\"><i class=\"icon-note\"></i></a><a class=\"delete btn btn-sm btn-danger dlt_sm_table\" data-toggle=\"modal\" ><i class=\"glyphicon glyphicon-trash\" onclick=\"delete_user($item->staff_id)\"></i></a>"
            );

            array_push($response["items"], $tmp);
        }

        echo json_encode($response);
    }

    function update_process(){
        if($this->security_model->updatesecurity()){
            echo '<div class="alert alert-success">security staff details updated successfully!</div>';
        }
        else{
            echo $this->lang->line('technical_problem');
        }
    }

	function add_process(){
		if($this->form_validation->run('add_staff') == FALSE){
        	echo '<div class="alert error"><ul>' . validation_errors('<li style="color:red">','</li>') . '</ul></div>';
        }
        elseif($this->security_model->exists_email( $this->input->post('email')) > 0){
            echo '<div class="alert error">Email already used.</div>';
        }
        else{
        	if($this->security_model->addsecurity()){
        		$staff = $this->input->post('last_name').' '.$this->input->post('first_name');
                $staff_email = $this->input->post('email');
                $team = userdata('last_name').' '.userdata('first_name');
                $team_email = userdata('email');
                $password = $this->input->post('pass1');

                // $this->sendstaffmail($staff, $team, $team_email, $staff_email, $password);

        		echo '<div class="alert alert-success">'.$this->lang->line('create_succesful').'</div>';
            }
            else{
                echo $this->lang->line('technical_problem');
            }
        }
	}

	function sendstaffmail($staff, $team, $team_email, $staff_email, $staff_password){
        $mail_str = '<html><head> <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> <title>Logistics and Energy Africa</title> <style type="text/css"> a{color: #4A72AF;}body, #header h1, #header h2, p{margin: 0; padding: 0;}#main{border: 1px solid #cfcece;}img{display: block;}#top-message p, #bottom-message p{color: #3f4042; font-size: 12px; font-family: Arial, Helvetica, sans-serif;}#header h1{color: #ffffff !important; font-family: "Lucida Grande", "Lucida Sans", "Lucida Sans Unicode", sans-serif; font-size: 24px; margin-bottom: 0!important; padding-bottom: 0;}#header h2{color: #ffffff !important; font-family: Arial, Helvetica, sans-serif; font-size: 24px; margin-bottom: 0 !important; padding-bottom: 0;}#header p{color: #ffffff !important; font-family: "Lucida Grande", "Lucida Sans", "Lucida Sans Unicode", sans-serif; font-size: 12px;}h1, h2, h3, h4, h5, h6{margin: 0 0 0.8em 0;}h3{font-size: 28px; color: #444444 !important; font-family: Arial, Helvetica, sans-serif;}h4{font-size: 22px; color: #4A72AF !important; font-family: Arial, Helvetica, sans-serif;}h5{font-size: 18px; color: #444444 !important; font-family: Arial, Helvetica, sans-serif;}p{font-size: 12px; color: #444444 !important; font-family: "Lucida Grande", "Lucida Sans", "Lucida Sans Unicode", sans-serif; line-height: 1.5;}</style></head><body><table width="100%" cellpadding="0" cellspacing="0" bgcolor="e4e4e4"><tr><td><table id="top-message" cellpadding="20" cellspacing="0" width="600" align="center"><tr><td align="center"><p style="display:none;">Trouble viewing this email? <a href="#">View in Browser</a></p></td></tr></table><table id="main" width="600" align="center" cellpadding="0" cellspacing="15" bgcolor="ffffff"><tr><td><table id="header" cellpadding="10" cellspacing="0" align="center" bgcolor="8fb3e9"><tr><td width="570" bgcolor="#013f45"><h1>Logistics &amp; Africa Energy</h1></td></tr></table></td></tr><tr><td></td></tr><tr><td><table id="content-1" cellpadding="0" cellspacing="0" align="center"><tr><td width="375" valign="top" colspan="3"><center><img src="http://logistics.devshop.co.ke/uploads/site/logo.png"/></center></td></tr></table></td></tr><tr><td><table id="content-2" cellpadding="0" cellspacing="0" align="center"><tr><td width="570" align="center"><p>Hello R_STAFF_NAME, <br/><br/>You have been invited to join R_TEAM team,<br/>R_TEAM (S_T_M) sent you this invitation. Below are your sign-in details:</p><br/><p><b>Email: </b>R_EMAIL<br/><b>Password: </b>R_PASSWORD<br/><br/><b>Please click here for activation: </b></p></td></tr></table></td></tr><tr><td align="center"><table id="content-6" cellpadding="0" cellspacing="0" align="center"><p align="center">You may copy paste this link in your browser:</p><p align="center"><a href="R_SITE_URL">R_SITE_URL</a></p></table></td></tr></table><table id="bottom-message" cellpadding="20" cellspacing="0" width="600" align="center"><tr><td align="center"><p>You are receiving this email because you signed up for Logistics and Energy Africa Ltd customer portal</p></td></tr></table></td></tr></table></body></html>';

        $mail_str = str_replace('R_STAFF_NAME', $staff, $mail_str);
        $mail_str = str_replace('R_TEAM', $team, $mail_str);
        $mail_str = str_replace('S_T_M', $team_email, $mail_str);
        $mail_str = str_replace('R_EMAIL', $staff_email, $mail_str);
        $mail_str = str_replace('R_PASSWORD', $staff_password, $mail_str);
        $mail_str = str_replace('R_SITE_URL', site_url('admin/login'), $mail_str);

        $date = date("l jS \of F Y");
        $subject = "$team has invited you to join $team on Ikanban";

        send_staff_mail_welcome($staff_email, $subject, $mail_str);
    }

    function delete($staff_id){
		if($this->security_model->delete($staff_id)){
			echo 'deleted';
		}	
	}
}

?>