<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Orders extends CI_Controller{
    function Orders(){
 		parent::__construct();
 		$this->load->database();
 		$this->load->model("orders_model");
 		$this->load->model("quotations_model");
 		$this->load->model("driver_model");
 		$this->load->model("resources_model");
 		$this->load->model("security_model");
        $this->load->library('zend');
        $this->zend->load('Zend/Barcode');
 		check_login();
	}

	function index(){
		$data['orders'] = $this->orders_model->getorders();
		$this->load->view('header');
		$this->load->view('orders/index', $data);
		$this->load->view('footer');
	}

    function generate_bar_code($airwaybill){
        echo Zend_Barcode::render('code39', 'image', array('text' => $airwaybill), array());
    }

    // get order items locations
    function get_airway_bill_items_locations_ajax($airwaybill){
        $items = $this->orders_model->get_airway_items($airwaybill);
        $response = array();
        $stop = 1;

        foreach($items->result() as $item){
            $tmp = array(
                'stop' => 'Stop '.$stop,
                'contact' => $this->orders_model->getcontactnames($item->contact_person),
                'pickup' => 'From > '.$this->orders_model->getlocations($item->pickup_loc).' - '.$item->pickup_date.' '.$item->pickup_time,
                'delivery' => 'To > '.$this->orders_model->getlocations($item->del_loc).' - '.$item->del_date.' '.$item->del_time,
                'loc_from' => $this->orders_model->getlocations_address($item->pickup_loc),
                'loc_to' => $this->orders_model->getlocations_address($item->del_loc)
            );

            $response[] = $tmp;

            // array_push($response["items"], $tmp);

            $stop++;
        }

        echo json_encode($response);
    }

    // get order items
    function get_airwaybill_details_ajax($airwaybill){
        $items = $this->orders_model->get_airway_items($airwaybill);

        $response = array();
        $response["items"] = array();

        $stop = 1;

        foreach($items->result() as $item){
            $tmp = array(
                'stop' => $stop,
                'contact' => $this->orders_model->getcontactnames($item->contact_person),
                'pickup' => $this->orders_model->getlocations($item->pickup_loc).' - '.$item->pickup_date.' '.$item->pickup_time,
                'delivery' => $this->orders_model->getlocations($item->del_loc).' - '.$item->del_date.' '.$item->del_time,
                'quantity' => $item->item_quantity.' '.$item->unit,
                'package' => $this->orders_model->packagenames($item->package_type)
            );

            array_push($response["items"], $tmp);

            $stop++;
        }

        echo json_encode($response);
    }

	function get_pending_ajax(){
		$items = $this->orders_model->getpending();

        $response = array();
        $response["items"] = array();

        foreach($items->result() as $item){
            $status = $item->order_status;
            $m_status = '';

            if($status == 1){
            	$m_status = '<span class="btn-sm" style="background-color: #00b0f0; color:#ffffff; ">NEW</span>';
            }
            elseif($status == 2){
                $m_status = '<span class="btn-sm" style="background-color: #ffc000; color:#ffffff; ">PENDING</span>';
            }
            elseif($status == 3){
                $m_status = '<span class="btn-sm" style="background-color: #ff99cc; color:#ffffff; ">ACTION NEEDED</span>';
            }
            elseif($status == 4){
                $m_status = '<span class="btn-sm" style="background-color: #008000; color:#ffffff; ">COMPLETE</span>';
            }
            elseif($status == 5){
                $m_status = '<span class="btn-sm" style="background-color: #f2a057; color:#ffffff; ">LIVE</span>';
            }

            $view = site_url('admin/orders/view/').'/'.$item->order_id;

            $tmp = array(
                'airway' => $item->airwaybill_no,
                'transporter' => config('site_name'),
                'customer' => $this->orders_model->getcompanyname($item->customer_customer_id),
                'status' => $m_status,
                'comments' => $item->comment,
                'date' => date('d F Y g:i a', $item->order_date),
                'options' => "<a href=\"$view\" class=\"edit btn btn-sm btn-default dlt_sm_table\"><i class=\"fa fa-bars\"></i></a>"
            );

            array_push($response["items"], $tmp);
        }

        echo json_encode($response);
	}

	function get_orders_ajax(){
		$items = $this->orders_model->getorders();

        $response = array();
        $response["items"] = array();

        foreach($items->result() as $item){
            $status = $item->order_status;
            $m_status = '';

            if($status == 1){
            	$m_status = '<span class="btn-sm" style="background-color: #00b0f0; color:#ffffff; ">NEW</span>';
            }
            elseif($status == 2){
                $m_status = '<span class="btn-sm" style="background-color: #ffc000; color:#ffffff; ">PENDING</span>';
            }
            elseif($status == 3){
                $m_status = '<span class="btn-sm" style="background-color: #ff99cc; color:#ffffff; ">ACTION NEEDED</span>';
            }
            elseif($status == 4){
                $m_status = '<span class="btn-sm" style="background-color: #008000; color:#ffffff; ">COMPLETE</span>';
            }
            elseif($status == 5){
                $m_status = '<span class="btn-sm" style="background-color: #f2a057; color:#ffffff; ">LIVE</span>';
            }

            $view = site_url('admin/orders/view/').'/'.$item->order_id;

            $tmp = array(
                'airway' => $item->airwaybill_no,
                'transporter' => config('site_name'),
                'customer' => $this->orders_model->getcompanyname($item->customer_customer_id),
                'status' => $m_status,
                'comments' => $item->comment,
                'date' => date('d F Y g:i a', $item->order_date),
                'options' => "<a href=\"$view\" class=\"edit btn btn-sm btn-default dlt_sm_table\"><i class=\"fa fa-bars\"></i></a>"
            );

            array_push($response["items"], $tmp);
        }

        echo json_encode($response);
	}

	function get_action_ajax(){
		$items = $this->orders_model->needaction();

        $response = array();
        $response["items"] = array();

        foreach($items->result() as $item){
            $status = $item->order_status;
            $m_status = '';

            if($status == 1){
            	$m_status = '<span class="btn-sm" style="background-color: #00b0f0; color:#ffffff; ">NEW</span>';
            }
            elseif($status == 2){
                $m_status = '<span class="btn-sm" style="background-color: #ffc000; color:#ffffff; ">PENDING</span>';
            }
            elseif($status == 3){
                $m_status = '<span class="btn-sm" style="background-color: #ff99cc; color:#ffffff; ">ACTION NEEDED</span>';
            }
            elseif($status == 4){
                $m_status = '<span class="btn-sm" style="background-color: #008000; color:#ffffff; ">COMPLETE</span>';
            }
            elseif($status == 5){
                $m_status = '<span class="btn-sm" style="background-color: #f2a057; color:#ffffff; ">LIVE</span>';
            }

            $view = site_url('admin/orders/view/').'/'.$item->order_id;

            $tmp = array(
                'airway' => $item->airwaybill_no,
                'transporter' => config('site_name'),
                'customer' => $this->orders_model->getcompanyname($item->customer_customer_id),
                'status' => $m_status,
                'comments' => $item->comment,
                'date' => date('d F Y g:i a', $item->order_date),
                'options' => "<a href=\"$view\" class=\"edit btn btn-sm btn-default dlt_sm_table\"><i class=\"fa fa-bars\"></i></a>"
            );

            array_push($response["items"], $tmp);
        }

        echo json_encode($response);
	}

	function get_complete_ajax(){
		$items = $this->orders_model->getcomplete();

        $response = array();
        $response["items"] = array();

        foreach($items->result() as $item){
            $status = $item->order_status;
            $m_status = '';

            if($status == 1){
            	$m_status = '<span class="btn-sm" style="background-color: #00b0f0; color:#ffffff; ">NEW</span>';
            }
            elseif($status == 2){
                $m_status = '<span class="btn-sm" style="background-color: #ffc000; color:#ffffff; ">PENDING</span>';
            }
            elseif($status == 3){
                $m_status = '<span class="btn-sm" style="background-color: #ff99cc; color:#ffffff; ">ACTION NEEDED</span>';
            }
            elseif($status == 4){
                $m_status = '<span class="btn-sm" style="background-color: #008000; color:#ffffff; ">COMPLETE</span>';
            }
            elseif($status == 5){
                $m_status = '<span class="btn-sm" style="background-color: #f2a057; color:#ffffff; ">LIVE</span>';
            }

            $view = site_url('admin/orders/view/').'/'.$item->order_id;

            $tmp = array(
                'airway' => $item->airwaybill_no,
                'transporter' => config('site_name'),
                'customer' => $this->orders_model->getcompanyname($item->customer_customer_id),
                'status' => $m_status,
                'comments' => $item->comment,
                'date' => date('d F Y g:i a', $item->order_date),
                'options' => "<a href=\"$view\" class=\"edit btn btn-sm btn-default dlt_sm_table\"><i class=\"fa fa-bars\"></i></a>"
            );

            array_push($response["items"], $tmp);
        }

        echo json_encode($response);
	}

	function get_live_ajax(){
		$items = $this->orders_model->getlive();

        $response = array();
        $response["items"] = array();

        foreach($items->result() as $item){
            $status = $item->order_status;
            $m_status = '';

            if($status == 1){
            	$m_status = '<span class="btn-sm" style="background-color: #00b0f0; color:#ffffff; ">NEW</span>';
            }
            elseif($status == 2){
                $m_status = '<span class="btn-sm" style="background-color: #ffc000; color:#ffffff; ">PENDING</span>';
            }
            elseif($status == 3){
                $m_status = '<span class="btn-sm" style="background-color: #ff99cc; color:#ffffff; ">ACTION NEEDED</span>';
            }
            elseif($status == 4){
                $m_status = '<span class="btn-sm" style="background-color: #008000; color:#ffffff; ">COMPLETE</span>';
            }
            elseif($status == 5){
                $m_status = '<span class="btn-sm" style="background-color: #f2a057; color:#ffffff; ">LIVE</span>';
            }

            $view = site_url('admin/orders/view/').'/'.$item->order_id;

            $tmp = array(
                'airway' => $item->airwaybill_no,
                'transporter' => config('site_name'),
                'customer' => $this->orders_model->getcompanyname($item->customer_customer_id),
                'status' => $m_status,
                'comments' => $item->comment,
                'date' => date('d F Y g:i a', $item->order_date),
                'options' => "<a href=\"$view\" class=\"edit btn btn-sm btn-default dlt_sm_table\"><i class=\"fa fa-bars\"></i></a>"
            );

            array_push($response["items"], $tmp);
        }

        echo json_encode($response);
	}

	function pending(){
		$data['orders'] = $this->orders_model->getpending();
		$this->load->view('header');
		$this->load->view('orders/pending', $data);
		$this->load->view('footer');
	}

	function live(){
		$data['orders'] = $this->orders_model->getlive();
		$this->load->view('header');
		$this->load->view('orders/live', $data);
		$this->load->view('footer');
	}

	function action(){
		$this->load->view('header');
		$this->load->view('orders/action');
		$this->load->view('footer');
	}

	function complete(){
		$this->load->view('header');
		$this->load->view('orders/complete');
		$this->load->view('footer');
	}

	function view($order){
		//check to see if we have quotation, if yes get it if no create it
		if(!$this->quotations_model->doesquotationexist($order)){
			$this->quotations_model->createquotation($order);
		}

		//get quote data
		$data['quotation'] = $this->quotations_model->getquote($order);
		$data['orderno'] = $order;
		$data["drivers"] = $this->driver_model->getdrivers();
		$data["vehicles"] = $this->resources_model->getresources();
		$data["securities"] = $this->security_model->getsecurity();

		$data['order'] = $this->orders_model->getorderdetails($order);
		$this->load->view('header');
		$this->load->view('orders/order', $data);
		$this->load->view('footer');
	}
}
?>