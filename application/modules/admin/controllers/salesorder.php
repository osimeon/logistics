<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Salesorder extends CI_Controller {
	function Salesorder(){
 		parent::__construct();
	 	$this->load->database();		 
	 	$this->load->model("salesorder_model");
	 	$this->load->model("qtemplates_model");
	 	$this->load->model("customers_model");
	 	$this->load->model("staff_model");
	 	$this->load->model("salesteams_model");
	 	$this->load->model("pricelists_model");	
	 	$this->load->model("products_model");
		$this->load->model("quotations_model");
		$this->load->model("driver_model");
		$this->load->model("resources_model");
		$this->load->model("security_model");
		$this->load->model("orders_model");
     	$this->load->library('form_validation');
         
     	$this->load->helper('pdf_helper');  
         
         /*cache control*/
		$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
        $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
         
     	check_login(); 
    }

    // getting resource details for ajax call
	function get_resource_details_ajax($order){
    	$quotation = $this->quotations_model->getquote($order);

    	$driver = $quotation->driver;
    	$vehicle = $quotation->vehicle;
    	$security = $quotation->security;

    	// get driver details
    	$driver_detail = $this->driver_model->getdetails($driver);

    	// get vehicle details
    	$vehicle_detail = $this->resources_model->getvehicle($vehicle);

    	// get security details
    	$security_detail = $this->security_model->getdetails($security);

    	if(!$driver_detail && !$vehicle_detail && !$security_detail){
    		$response['resources'] = array();
    		die(json_encode($response));
    	}

    	// response output
    	$response['resources'] = array();
    	$driver_avatar = base_url('uploads').'/'.$driver_detail->avatar;

    	$driver_arr = array(
    		'name' => $driver_detail->lname.' '.$driver_detail->fname,
    		'desc' => $driver_detail->job,
    		'qua' => 1,
    		'avatar' => '<img src="'.$driver_avatar.'" alt="user image" style="height: 100px;width: 100px;">',
    		'options' => '<a href="javascript:void(0)" class="delete btn btn-sm btn-danger dlt_sm_table" data-toggle="modal" data-target="#modal-basic4" onclick="delete_driver_resource('.$quotation->id.')"><i class="glyphicon glyphicon-trash"></i></a>'
    	);

    	$vehicle_avatar = base_url('uploads/resources').'/'.$vehicle_detail->avatar;
    	$vehicle_arr = array(
    		'name' => $vehicle_detail->name,
    		'desc' => $vehicle_detail->description,
    		'qua' => 1,
    		'avatar' => '<img src="'.$vehicle_avatar.'" alt="user image" style="height: 100px;width: 100px;">',
    		'options' => '<a href="javascript:void(0)" class="delete btn btn-sm btn-danger dlt_sm_table" data-toggle="modal" data-target="#modal-basic4" onclick="delete_vehicle_resource('.$quotation->id.')"><i class="glyphicon glyphicon-trash"></i></a>'
    	);

    	$security_avatar = base_url('uploads').'/'.$security_detail->avatar;
    	$security_arr = array(
    		'name' => $security_detail->lname.' '.$security_detail->fname,
    		'desc' => $security_detail->job,
    		'qua' => 1,
    		'avatar' => '<img src="'.$security_avatar.'" alt="user image" style="height: 100px;width: 100px;">',
    		'options' => '<a href="javascript:void(0)" class="delete btn btn-sm btn-danger dlt_sm_table" data-toggle="modal" data-target="#modal-basic4" onclick="delete_security_resource('.$quotation->id.')"><i class="glyphicon glyphicon-trash"></i></a>'
    	);

    	if($driver_detail){
    		array_push($response['resources'], $driver_arr);
		}

    	if($vehicle_detail){
    		array_push($response['resources'], $vehicle_arr);
		}

		if($security_detail){
			array_push($response['resources'], $security_arr);
		}
		
    	echo json_encode($response);
    }

    // delete order item
	function delete_item_ajax($quote){
		if($this->salesorder_model->delete_quote($quote)){
			echo 'deleted';
		}	
	}

	// delete driver resource from quote
	function delete_driver_resource($quote){
		if($this->salesorder_model->delete_driver($quote)){
			echo 'deleted';
		}
	}

	// delete vehicle resource from quote
	function delete_vehicle_resource($quote){
		if($this->salesorder_model->delete_vehicle($quote)){
			echo 'deleted';
		}
	}

	// delete security resource from quote
	function delete_security_resource($quote){
		if($this->salesorder_model->delete_security($quote)){
			echo 'deleted';
		}
	}

	// assign driver
	function assign_driver_ajax($driver, $order){
		if($this->salesorder_model->assign_driver($driver, $order)){
			echo '<div class="alert alert-success">driver was assigned</div>';
		}
		else{
			echo $this->lang->line('technical_problem');
		}
	}

	// assign vehicle
	function assign_vehicle_ajax($vehicle, $order){
		if($this->salesorder_model->assign_vehicle($vehicle, $order)){
			echo '<div class="alert alert-success">vehicle was assigned</div>';
		}
		else{
			echo $this->lang->line('technical_problem');
		}
	}

	// assign security
	function assign_security_ajax($security, $order){
		if($this->salesorder_model->assign_security($security, $order)){
			echo '<div class="alert alert-success">security personnel was assigned</div>';
		}
		else{
			echo $this->lang->line('technical_problem');
		}
	}

	function quote_items_ajax($order){
		$quoteitems = $this->salesorder_model->quote_items_ajax($order);

		$response = array();
		$response["items"] = array();

		foreach($quoteitems->result() as $item){
			$tmp = array('item' => $item->item, 'description' => $item->description, 'quantity' => $item->quantity, 
				'unit_price' => number_format($item->unit_price, 2), 'options' => '<a href="javascript:void(0)" class="delete btn btn-sm btn-danger dlt_sm_table" data-toggle="modal" data-target="#modal-basic4" onclick="delete_quote_item('.$item->id.')"><i class="glyphicon glyphicon-trash"></i></a>');
			array_push($response["items"], $tmp);
		}

		echo json_encode($response);
	}

	function send_customer_quote($order){
		$mail_template = '<html><head> <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> <title>Logistics and Energy Africa</title> <style type="text/css"> a{color: #4A72AF;}body, #header h1, #header h2, p{margin: 0; padding: 0;}#main{border: 1px solid #cfcece;}img{display: block;}#top-message p, #bottom-message p{color: #3f4042; font-size: 12px; font-family: Arial, Helvetica, sans-serif;}#header h1{color: #ffffff !important; font-family: "Lucida Grande", "Lucida Sans", "Lucida Sans Unicode", sans-serif; font-size: 24px; margin-bottom: 0!important; padding-bottom: 0;}#header h2{color: #ffffff !important; font-family: Arial, Helvetica, sans-serif; font-size: 24px; margin-bottom: 0 !important; padding-bottom: 0;}#header p{color: #ffffff !important; font-family: "Lucida Grande", "Lucida Sans", "Lucida Sans Unicode", sans-serif; font-size: 12px;}h1, h2, h3, h4, h5, h6{margin: 0 0 0.8em 0;}h3{font-size: 28px; color: #444444 !important; font-family: Arial, Helvetica, sans-serif;}h4{font-size: 22px; color: #4A72AF !important; font-family: Arial, Helvetica, sans-serif;}h5{font-size: 18px; color: #444444 !important; font-family: Arial, Helvetica, sans-serif;}p{font-size: 12px; color: #444444 !important; font-family: "Lucida Grande", "Lucida Sans", "Lucida Sans Unicode", sans-serif; line-height: 1.5;}.call_to_action{text-decoration: none;display: block; height: 50px; width: 300px; background: #34696f; border: 2px solid rgba(33, 68, 72, 0.59); color: rgb(255, 255, 255); text-align: center;font: bold 2.2em/50px "Helvetica Neue", Arial, Helvetica, Geneva, sans-serif;background: -webkit-linear-gradient(top, #34696f, #2f5f63);background: -moz-linear-gradient(top, #34696f, #2f5f63);background: -o-linear-gradient(top, #34696f, #2f5f63);background: -ms-linear-gradient(top, #34696f, #2f5f63);background: linear-gradient(top, #34696f, #2f5f63);-webkit-border-radius: 50px;-khtml-border-radius: 50px;-moz-border-radius: 50px;border-radius: 50px;-webkit-box-shadow: 0 8px 0 #1b383b;-moz-box-shadow: 0 4px 0 #1b383b;box-shadow: 0 4px 0 #1b383b;text-shadow: 0 2px 2px rgba(255, 255, 255, 0.2);}</style></head><body><table width="100%" cellpadding="0" cellspacing="0" bgcolor="e4e4e4"><tr><td><table id="top-message" cellpadding="20" cellspacing="0" width="600" align="center"><tr><td align="center"><p style="display:none;">Trouble viewing this email? <a href="#">View in Browser</a></p></td></tr></table><table id="main" width="600" align="center" cellpadding="0" cellspacing="15" bgcolor="ffffff"><tr><td><table id="header" cellpadding="10" cellspacing="0" align="center" bgcolor="8fb3e9"><tr><td width="570" bgcolor="#013f45"><h1>Logistics &amp; Africa Energy</h1></td></tr></table></td></tr><tr><td></td></tr><tr><td><table id="content-1" cellpadding="0" cellspacing="0" align="center"><tr><td width="375" valign="top" colspan="3"><center><img src="http://logistics.devshop.co.ke/uploads/site/logo.png"/></center></td></tr></table></td></tr><tr><td><table id="content-2" cellpadding="0" cellspacing="0" align="center"><tr><td width="570" align="center"><p>Hello __CUSTOMER__, <br/><br/>A quotation has been sent to you regarding your order __ORDER_NO__,<br/>placed on the __DATE__. Please click on the link below to login and see your quotation details.</p><br/></td></tr></table></td></tr><br/><tr><td align="center"><table id="content-6" cellpadding="0" cellspacing="0" align="center"><p align="center">You may copy paste this link in your browser:</p><br/><p align="center"><a href="__ORDER_URL__" class="call_to_action">R_ORDER_URL</a></p></table></td></tr></table><table id="bottom-message" cellpadding="20" cellspacing="0" width="600" align="center"><tr><td align="center"><p>You are receiving this email because you signed up for Logistics and Energy Africa Ltd customer portal</p></td></tr></table></td></tr></table></body></html>';

		$order_details = $this->orders_model->getorderdetails($order);
		$name = $this->orders_model->getcompanyname($order_details->customer_customer_id);
		$orderno = $order_details->airwaybill_no;
		$date = date('m/d/Y H:i', $order_details->order_date);
		$email = $this->orders_model->getcompanyemail($order_details->customer_customer_id);
		$order_url = site_url('customer/salesorder/need_action').'/'.$order;

		if($this->orders_model->actionneeded($order)){
			// send email
			$mail_template = str_replace('__CUSTOMER__', $name, $mail_template);
			$mail_template = str_replace('__ORDER_NO__', $orderno, $mail_template);
			$mail_template = str_replace('__DATE__', $date, $mail_template);
			$mail_template = str_replace('R_ORDER_URL', 'View Order', $mail_template);
			$mail_template = str_replace('__ORDER_URL__', $order_url, $mail_template);

			$subject = 'Quotation for order: '.$orderno;

			// echo $mail_template;

        	send_notice($email, $subject, $mail_template);

        	echo 'sent';
		}
		else{
			echo 'not sent';
		}
	}

	function quote_items_bill($order){

		$bill = $this->salesorder_model->quote_items_bill($order);

		if(!$bill->total){
			$bill = 0;
			$tmp["bill"] = number_format($bill, 2);
		}
		else{
			$tmp["bill"] = number_format($bill->total, 2);
		}

		echo json_encode($tmp);
	}

	function ajax(){
        //All Orders
        $all_obj = $this->salesorder_model->getAllOrders();
        $all_count = ($all_obj->num_rows() > 0) ? $all_obj->num_rows() : 0;

        //Pending Orders
        $pending_obj = $this->salesorder_model->getPendingOrders();
        $pending_count = ($pending_obj->num_rows() > 0) ? $pending_obj->num_rows() : 0;

        //Action needed Orders
        $actions_obj = $this->salesorder_model->getActionsNeeded();
        $actions_count = ($actions_obj->num_rows() > 0) ? $actions_obj->num_rows() : 0;

        //Live Orders
        $live_obj = $this->salesorder_model->getLiveOrders();
        $live_count = ($live_obj->num_rows() > 0) ? $live_obj->num_rows() : 0;

        //Closed Orders
        $closed_obj = $this->salesorder_model->getClosedOrders();
        $closed_count = ($closed_obj->num_rows() > 0) ? $closed_obj->num_rows() : 0;

        $response = array(
            'all' => $all_count,
            'pending' => $pending_count,
            'action_needed' => $actions_count,
            'live' => $live_count,
            'closed' => $closed_count
        );
        echo json_encode($response);
    }

	function index($customer_id = ''){
		$data['salesorder'] = $this->salesorder_model->quotations_list($customer_id);
		    	 	    	 
		$this->load->view('header');
		$this->load->view('salesorder/index',$data);
		$this->load->view('footer');
			 
	}

	function add(){ 
		$data['qtemplates'] = $this->qtemplates_model->qtemplate_list();
		$data['companies'] = $this->customers_model->company_list();
    	$data['staffs'] = $this->staff_model->staff_list(); 
    	$data['salesteams'] = $this->salesteams_model->salesteams_list(); 
    	$data['pricelists'] = $this->pricelists_model->pricelists_list(); 
    	$data['products'] = $this->products_model->products_list();
		$this->load->view('header');
		$this->load->view('quotations/add',$data);
		$this->load->view('footer');
	}

	function add_quote_item_ajax(){
		if($this->salesorder_model->add_quote_item_ajax()){
			echo '<div class="alert alert-success">'.$this->lang->line('create_succesful').'</div>';
		}
		else{
			echo $this->lang->line('technical_problem');
		}
	}

	function add_process(){
		$this->form_validation->set_rules('customer_id', 'Customer', 'required');
		$this->form_validation->set_rules('date', 'Date', 'required');
		$this->form_validation->set_rules('sales_person', 'Salesperson', 'required');
		$this->form_validation->set_rules('sales_team_id', 'Sales Team', 'required');
		$this->form_validation->set_rules('grand_total', 'Total', 'required');
		 
		if($this->form_validation->run() == FALSE){
            echo '<div class="alert error" style="color:red">' . validation_errors() . '</div>';
        } 
        else{
            if($this->salesorder_model->add_quotation()){ 
                echo '<div class="alert alert-success">item was added sucessfully!</div>';
            }
            else{
                echo $this->lang->line('technical_problem');
            }
        }
	}
	
	function view($quotation_id){
		$data['quotation'] = $this->salesorder_model->get_quotation($quotation_id);
    	$data['qtemplates'] = $this->qtemplates_model->qtemplate_list();
		$data['companies'] = $this->customers_model->company_list();
		$data['staffs'] = $this->staff_model->staff_list(); 
    	$data['pricelists'] = $this->pricelists_model->pricelists_list(); 
    	$data['qo_products'] = $this->salesorder_model->quot_order_products($quotation_id); 
		$this->load->view('header');
		$this->load->view('salesorder/view',$data);
		$this->load->view('footer');
	}
	
	function update($qo_id){
    	$data['quotation'] = $this->salesorder_model->get_quotation($qo_id);
    	$data['qtemplates'] = $this->qtemplates_model->qtemplate_list();
		$data['companies'] = $this->customers_model->company_list();
    	$data['staffs'] = $this->staff_model->staff_list(); 
    	$data['salesteams'] = $this->salesteams_model->salesteams_list(); 
    	$data['pricelists'] = $this->pricelists_model->pricelists_list(); 
    	$data['products'] = $this->products_model->products_list();
    	$data['qo_products'] = $this->salesorder_model->quot_order_products($qo_id);    
		$this->load->view('header');
		$this->load->view('salesorder/update',$data);
		$this->load->view('footer');
	}
	
	function update_process(){
		$this->form_validation->set_rules('customer_id', 'Customer', 'required');
		$this->form_validation->set_rules('date', 'Date', 'required');
		$this->form_validation->set_rules('sales_person', 'Salesperson', 'required');
		$this->form_validation->set_rules('sales_team_id', 'Sales Team', 'required');
		$this->form_validation->set_rules('grand_total', 'Total', 'required');
		
		if($this->form_validation->run() == FALSE){
            echo '<div class="alert error" style="color:red"> ' . validation_errors() . '</div>';
        }
        else{
            if($this->salesorder_model->update_quotation()){
                echo '<div class="alert alert-success">'.$this->lang->line('update_succesful').'</div>';
            }
            else{
                echo $this->lang->line('technical_problem');
            }
        }
	}
	
	/*
     * deletes pricelist     *  
     */
	function delete($order_id){
		if($this->salesorder_model->delete($order_id)){
			echo 'deleted';
		}
		
	}
	 
	
	function ajax_qtemplates_products($qtemplate_id, $pricelist_id){	
    	$data['qtemplate_products'] = $this->qtemplates_model->qtemplate_products($qtemplate_id);  
    	$data['pricelist_version'] = $this->salesorder_model->get_pricelist_version_by_pricelist_id($pricelist_id);
        $this->load->view('salesorder/ajax_qtemplates_products',$data);
   	}
   	
   	function print_quot($quotation_id){ 
        $data['quotation'] = $this->salesorder_model->get_quotation($quotation_id);
    	$data['qtemplates'] = $this->qtemplates_model->qtemplate_list();
		$data['companies'] = $this->customers_model->company_list();
    	$data['staffs'] = $this->staff_model->staff_list(); 
    	$data['pricelists'] = $this->pricelists_model->pricelists_list(); 
    	$data['qo_products'] = $this->salesorder_model->quot_order_products($quotation_id);   		
		$this->load->view('salesorder/order_print',$data);
	}
	
	function ajax_create_pdf($quotation_id){ 
        $data['quotation'] = $this->salesorder_model->get_quotation($quotation_id);
    	$data['qtemplates'] = $this->qtemplates_model->qtemplate_list();
		$data['companies'] = $this->customers_model->company_list();
    	$data['staffs'] = $this->staff_model->staff_list(); 
    	$data['pricelists'] = $this->pricelists_model->pricelists_list(); 
    	$data['qo_products'] = $this->salesorder_model->quot_order_products($quotation_id);   		 
		$html = $this->load->view('salesorder/ajax_create_pdf',$data,true);
		$filename = 'Order-'.$data['quotation']->quotations_number;
		$pdfFilePath = FCPATH."/pdfs/".$filename.".pdf";
		$mpdf=new mPDF('c','A4','','',20,15,48,25,10,10); 
		$mpdf->SetProtection(array('print'));
		$mpdf->SetTitle("Acme Trading Co. - Invoice");
		$mpdf->SetAuthor("Acme Trading Co.");
		$mpdf->SetWatermarkText($data['quotation']->payment_term);
		$mpdf->showWatermarkText = true;
		$mpdf->watermark_font = 'DejaVuSansCondensed';
		$mpdf->watermarkTextAlpha = 0.1;
		$mpdf->SetDisplayMode('fullpage');		 
		$mpdf->WriteHTML($html);
		$mpdf->Output($pdfFilePath, 'F');
		echo base_url()."pdfs/".$filename.".pdf";
	 	exit;	
	}
	
	function send_quotation(){
 		$this->load->helper('template');
		$quotation_id 	= $this->input->post('quotation_id');
		$email_subject 	= $this->input->post('email_subject');
		$to 			= implode(',',$this->input->post('recipients'));
		$email_body     = $this->input->post('message_body');
		$message_body = parse_template($email_body);	 
		$quotation_pdf = $this->input->post('quotation_pdf');
		
		if(send_email($email_subject, $to,  $message_body,$quotation_pdf)){
			echo "success";
		}
		else{
			echo "not sent";
		}
	} 
	
	
	/*
     * deletes product     *  
     */
	function delete_qo_product( $product_id){ 
	   	if($this->salesorder_model->delete_qo_product($product_id)){
			echo 'deleted';
		}
	}
	
	/*
	Create Invoice
	*/
	function create_invoice( $order_id){ 
		$invoice_id=$this->salesorder_model->create_invoice($order_id);	   	 
		redirect('admin/invoices/update/'.$invoice_id);   	 
	}

	function ajax_get_products_price($product_id, $pricelist_id){	
    	$data['products_price'] = $this->salesorder_model->get_product_price($product_id,$pricelist_id);  
    	   	 
        echo  $data['products_price']->special_price;
        exit;
   	}
}
