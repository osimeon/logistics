<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller {
    function Admin(){
    	parent::__construct();
		$this->load->database();
		$this->load->model("user_model");
		$this->load->library('form_validation');
         
         /*cache control*/
		$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
    	$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");  
    } 

	function index(){
		redirect(site_url('admin/login'),'refresh');
	}

    /*
     * Displays form for new member
     */
    function create(){
		if(is_login() == 1 ){ 
			redirect(site_url('admin/dashboard'),'refresh'); 
		}
       
        $this->load->view('user/create_user');
    }

    /*
     * Makes controls for new member.
     */
    function create_process(){
    	$email = $this->input->post('email');
    	$password = $this->input->post('pass1');
    	$name = $this->input->post('last_name').' '.$this->input->post('first_name');

    	if($this->form_validation->run('create_user') == FALSE){
            echo '<div class="alert error"><ul>' . validation_errors('<li>','</li>') . '</ul></div>';
        }
        elseif($this->user_model->exists_email( $this->input->post('email') ) > 0){
            echo '<div class="alert error">'.$this->lang->line('already_account').'</div>';
        }
        else{
            if($this->user_model->create_user()){
            	//Send email
                $email_body = '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">';
                $email_body .= '<title>Logistics & Energy Africa</title><style type="text/css">';
                $email_body .= 'a{color: #4A72AF;}body, #header h1, #header h2, p {margin: 0;padding: 0;}';
                $email_body .= '#main {border: 1px solid #cfcece;}img {display: block;}';
                $email_body .= '#top-message p, #bottom-message p {color: #3f4042;font-size: 12px;font-family: Arial, Helvetica, sans-serif;}';
                $email_body .= '#header h1{color: #ffffff !important;font-family: "Lucida Grande", "Lucida Sans", "Lucida Sans Unicode", sans-serif;font-size: 24px; margin-bottom: 0!important; padding-bottom: 0;}';
                $email_body .= '#header h2 {color: #ffffff !important;font-family: Arial, Helvetica, sans-serif;font-size: 24px; margin-bottom: 0 !important; padding-bottom: 0;}';
                $email_body .= '#header p {color: #ffffff !important;font-family: "Lucida Grande", "Lucida Sans", "Lucida Sans Unicode", sans-serif;font-size: 12px;}';
                $email_body .= 'h1, h2, h3, h4, h5, h6 {margin: 0 0 0.8em 0;}h3{font-size: 28px;color: #444444 !important;font-family: Arial, Helvetica, sans-serif;}h4 {font-size: 22px;color: #4A72AF !important;font-family: Arial, Helvetica, sans-serif;}';
                $email_body .= 'h5{font-size: 18px;color: #444444 !important;font-family: Arial, Helvetica, sans-serif;}';
                $email_body .= 'p{font-size: 12px;color: #444444 !important;font-family: "Lucida Grande", "Lucida Sans", "Lucida Sans Unicode", sans-serif; line-height: 1.5;}';
                $email_body .= '</style></head><body><table width="100%" cellpadding="0" cellspacing="0" bgcolor="e4e4e4"><tr>
                                <td><table id="top-message" cellpadding="20" cellspacing="0" width="600" align="center"><tr>
                                <td align="center"><p style="display:none;">Trouble viewing this email? <a href="#">View in Browser</a></p>
                                </td></tr></table><!-- top message -->
                                <table id="main" width="600" align="center" cellpadding="0" cellspacing="15" bgcolor="ffffff">
                                <tr><td><table id="header" cellpadding="10" cellspacing="0" align="center" bgcolor="8fb3e9">
                                <tr><td width="570" bgcolor="7aa7e9"><h1>Logistics &amp; Energy Africa</h1></td>
                                </tr></table><!-- header -->
                                </td></tr><!-- header -->
                                <tr><td></td></tr><tr><td><table id="content-1" cellpadding="0" cellspacing="0" align="center">
                                <tr><td width="375" valign="top" colspan="3"><h3>Welcome to Logistics &amp; Energy Africa</h3>
                                <h4>Going Beyond | Going Places</h4></td></tr>
                                </table><!-- content 1 -->
                                </td></tr><!-- content 1 -->
                                <tr><td><table id="content-2" cellpadding="0" cellspacing="0" align="center"><tr>
                                <td width="570" align="center"><p>';
                $email_body .= 'Hello '.$name.', <br/><br/>
                                Your account at Logistics & Energy Africa Ltd is ready,<br/>
                                Below are your account login details: 
                                </p><br/><p>
                                <b>Email: </b>'.$email.'<br/>
                                <b>Password: </b>'.$password.'<br/><br/>
                                <b>Please <a href="'.site_url('admin/activate').'/'.md5($password).'">click here</a> for activation </b></p>';
                $email_body .= '</td></tr></table><!-- content-2 --></td></tr><!-- content-2 --><tr>
                                <td align="center"><table id="content-6" cellpadding="0" cellspacing="0" align="center">
                                <p align="center">You may copy paste this link in your browser:</p><p align="center"><a href="';

                $email_body .= site_url('admin');

                $email_body .= '">Login</a></p>
                                </table></td></tr></table><!-- main -->
                                <table id="bottom-message" cellpadding="20" cellspacing="0" width="600" align="center"><tr>
                                <td align="center"><p>You are receiving this email because you signed up for Logistics and Energy Africa Ltd customer portal</p>
                                </td></tr></table><!-- top message --></td>
                                </tr></table><!-- wrapper --></body></html>';
                
                $subject = 'Transporter login details';
            	send_notice($this->input->post('email'), $subject, $email_body);

            	echo '<div class="alert error">' .$this->lang->line('register_succes_msg1'). '</div>';
            }
            else{
				echo '<div class="alert error">' .$this->lang->line('technical_problem'). '</div>';
            }
        }
    }

    function register_succes(){
    	$userdata = $this->user_model->user_data( $this->session->userdata('username') );
        $data['name'] = $userdata->name;
        
        $this->load->view('header');
        $this->load->view('user/register_succes',$data);
        $this->load->view('footer');
    }

	function lostpw_succes(){
    	$this->load->view('user/lostpw_succes');
    }

    /*
     * Displays the login form
     */
    function login(){
		if(is_login() == 1 ){ 
			redirect(site_url('admin/dashboard'),'refresh'); 
		}
        
        $this->load->view('user/login_user');
    }

    function login_process(){
        if($this->form_validation->run('login_user') == FALSE){
            echo '<div class="alert error"><ul>' . validation_errors('<li>','</li>') . '</ul></div>';
        }
        elseif($this->user_model->exists_email($this->input->post('email')) == 0){
            echo '<div class="alert error">'.$this->lang->line('you_must_create_account').'</div>';
        }
        elseif($this->user_model->check_user_detail() == FALSE){
            echo '<div class="alert error">'.$this->lang->line('invalid_email_or_pass').'</div>';
        }
        else{
            $userdata = $this->user_model->user_data( $this->input->post('email') );
            $session_data = array("username"   => $userdata->email, "userhash"   => md5($userdata->password.$this->config->item('password_hash')));
            $this->session->set_userdata($session_data);
            echo '<script>location.href="'.site_url('admin/dashboard').'";</script>';
        }
    }

    /*
     * Logout function
     */
    function logout(){
        $session_items = array('username' => '', 'userhash' => '', 'logged_in'=> FALSE);
		$this->session->unset_userdata($session_items);
		$this->output->nocache();
		redirect(site_url('admin/login'),"refresh");
    }

	/*
     * Displays the Password reset form
     */
	function lostpassword(){
		if(is_login() == 1){ 
			redirect(site_url('admin/dashboard'),'refresh'); 
		}
		
		$this->load->view('header');
		$this->load->view('user/lostpassword');
		$this->load->view('footer');
	}

	/*
     * Sends request for password reset
     */
	function lostpassword_process(){
		$this->form_validation->set_rules('email', 'Email', 'required');
			
		if($this->form_validation->run('lostpassword') == FALSE){
			echo '<div class="alert error">'.validation_errors().'</div>';
		}
		elseif($this->user_model->exists_email( $this->input->post('email')) == 0){
			echo '<div class="alert error">'.$this->lang->line('you_must_create_account').'</div>';
		}
		else{
			$lostpw_code = $this->user_model->create_lostpw_code();
			$subject = 'Reset password request';
            $message = 'Hello, <br> To reset your password please follow the link below: <br> <a href="'.base_url('admin/check_code/'.$this->input->post('email').'/'.$lostpw_code).'">'.base_url('admin/check_code/'.$this->input->post('email').'/'.$lostpw_code).'</a>';
			send_notice($this->input->post('email'),$subject,$message);
			echo '<script>location.href="'.site_url('admin/lostpw_succes').'";</script>';
		}
	}

	/*
     * Checks the reset password code
     */
	function check_code($email, $code){
		$status = $this->user_model->check_code($email, $code);
		
		if($status == 0){
			$this->session->set_flashdata('message','<div class="alert error">'.$this->lang->line('invalid_reset_code').'</div>');
			redirect(base_url('admin/login'),'refresh');
		}
		else{
			$new_password = $this->user_model->create_new_password( $email );
			$subject = 'New Password';
            $message = 'Hello, <br><br> New password is <b>'.$new_password.'</b>. Please <a href="'.base_url('admin/login').'">click here</a> for login';	
			send_notice($email,$subject,$message);
			
			$this->session->set_flashdata('message','<div class="alert ok">'.$this->lang->line('new_pass_sent').'</div>');
			redirect(site_url('admin/login'),'refresh');
		}
	}
	/*
     * Displays the settings
     */

    
    function account_settings(){
		check_login();
		
		$data['user'] = $this->user_model->user_data( userdata('email') );
			
		$this->load->view('header');
		$this->load->view('user/user_settings',$data);
		$this->load->view('footer');
	}

	function change_password(){
		check_login();
		
		if($this->form_validation->run('change_password') == FALSE){
			echo '<div class="alert error"><ul>'.validation_errors('<li>','</li>').'</ul></div>';
		}
		elseif($this->user_model->check_password() == 0){
			echo '<div class="alert error">'.$this->lang->line('invalid_pass').'</div>';
		}
		else{
			if($this->user_model->password_update()){
				$session_data = array("username"   => userdata('email'), "userhash"   => md5(userdata('password').$this->config->item('password_hash')));
				$this->session->set_userdata($session_data);
				echo '<div class="alert ok">'.$this->lang->line('update_succesful').'</div>';
			}
			else{
				echo '<div class="alert error">'.$this->lang->line('technical_problem').'</div>';
			}
		}
	}
	
	function change_profile(){
		check_login();
		
		if($this->form_validation->run('change_profile') == FALSE){
			echo '<div class="alert error"><ul>'.validation_errors('<li>','</li>').'</ul></div>';
		}
		else{
			$session_data = array("username" => $this->input->post('email'), "userhash" => md5(userdata('password').$this->config->item('password_hash')),);
										  
			if($this->user_model->change_profile()){
				$this->session->set_userdata($session_data);
				echo '<div class="alert ok">'.$this->lang->line('update_succesful').'</div>';
			}
			else{
				echo '<div class="alert error">'.$this->lang->line('technical_problem').'</div>';
			}
		}
	}
    
	function user_list(){
		check_login();  
		$data['users'] = $this->user_model->user_list();
		$this->load->view('header');
		$this->load->view('user/user_list',$data);
		$this->load->view('footer');
	}

	/*
     * Displays member update form
     */
	function update($user_id){
		check_login();   
		$data['user'] = $this->user_model->get_user( $user_id );		
		$this->load->view('header');
		$this->load->view('user/update_user',$data);
		$this->load->view('footer');
	}

	/*
     * update processing
     */
	function update_process(){
		check_login();   
		if($this->form_validation->run('update_user') == FALSE){
            echo '<div class="alert error"><ul>' . validation_errors('<li>','</li>') . '</ul></div>';
        }
        else{
            if($this->user_model->update_user()){
                echo '<div class="alert ok">'.$this->lang->line('update_succesful').'</div>';
            }
            else{
                echo $this->lang->line('technical_problem');
            }
        }
	}
	/*
     * private function - Checks user ID
     * @param  a user id integer
     * @return bool
     */
	function _check_user_id($user_id){
		if($this->user_model->check_user_id( $user_id ) == 0){
			$this->form_validation->set_message('_check_user_id', 'The user id hidden field is invalid.');
			return false;
		}
		else{
			return true;
		}
	}

	/*
     * deletes user
     * @param  a user id integer
     * @return string for ajax
     */
	function delete($user_id){
		check_login();   
		
		if($this->_check_user_id($user_id)){
			if($this->user_model->delete($user_id)){
				echo 'deleted';
			}
		}
	}    
}