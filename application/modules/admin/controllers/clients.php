<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Clients extends CI_Controller{
    function Clients(){
 		parent::__construct();
 		$this->load->database();
 		$this->load->model("clients_model");
 		check_login();
	}

	function index(){
		$data['clients'] = $this->clients_model->getclients();
		$this->load->view('header');
		$this->load->view('clients/index',$data);
		$this->load->view('footer');
	}

	function get_clients_ajax(){
		$items = $this->clients_model->getclients();

        $response = array();
        $response["items"] = array();

        foreach($items->result() as $item){
            $tmp = array(
                'name' => $item->name,
                'email' => $item->email,
                'address' => $item->address,
                'website' => $item->website,
                'phone' => $item->phone,
                'register' => date('d F Y g:i a', $item->register_time)
            );

            array_push($response["items"], $tmp);
        }

        echo json_encode($response);
	}
}

?>