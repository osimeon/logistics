<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Orders_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function get_airway_items($airway){
        return $this->db->get_where('logistics_order_items', array('airwaybill_no' => $airway));
    }

    function packagenames($package){
        $packages = array();

        $pckgs = $this->db->query("SELECT * FROM logistics_packaging_type WHERE id IN ($package)");

        foreach($pckgs->result() as $package){
            $packages[] = $package->type;
        }

        return implode(',', $packages);
    }

    function getorderdetails($order){
        return $this->db->get_where('logistics_order', array('order_id' => $order))->row();
    }

    function getorders(){
    	return $this->db->get_where('logistics_order', array('order_status' => 1));
    }

    function getpending(){
        return $this->db->get_where('logistics_order', array('order_status' => 2));
    }

    function needaction(){
        return $this->db->get_where('logistics_order', array('order_status' => 3));
    }

    function getlive(){
        return $this->db->get_where('logistics_order', array('order_status' => 4));
    }

    function getcomplete(){
        return $this->db->get_where('logistics_order', array('order_status' => 5));
    }

    function getcontactnames($ids){
    	$names = array();

        $clients = $this->db->query("SELECT * FROM logistics_cust_staff WHERE staff_id IN ($ids)");
        
        foreach($clients->result() as $client){
        	$names[] = $client->lname .' '.$client->fname;
        }

        return implode(',', $names);
    }

    function getlocations($stops){
    	$locations = array();

        $locs = $this->db->query("SELECT name FROM logistics_pickup_loc WHERE loc_id IN ($stops)");

        foreach($locs->result() as $loc){
        	$locations[] = $loc->name;
        }

        return implode(',', $locations);
    }

    function getlocations_address($stops){
        $locations = array();

        $locs = $this->db->query("SELECT address FROM logistics_pickup_loc WHERE loc_id IN ($stops)");

        foreach($locs->result() as $loc){
            $locations[] = $loc->address;
        }

        return implode(',', $locations);
    }

    function getcompanyaddress($customer){
        $address = '';

        $results = $this->db->get_where('company', array('id' => $customer));

        if($results){
            $row = $results->row();
            $address = $row->address;
        }

        return $address;
    }

    function getcompanyname($customer){
        $company = '';

        $results = $this->db->get_where('company', array('id' => $customer));

        if($results){
            $row = $results->row();
            $company = $row->name;
        }

        return $company;
    }

    function getcompanyemail($customer){
        $email = '';

        $results = $this->db->get_where('company', array('id' => $customer));

        if($results){
            $row = $results->row();
            $email = $row->email;
        }

        return $email;
    }

    // action needed
    function actionneeded($order){
        return $this->db->update('logistics_order', array('order_status' => 2, 'customer_status' => 3), array('order_id' => $order));
    }

    function packages($package){
        return $this->db->query("SELECT * FROM logistics_packaging_type WHERE id IN ($package)");
    }
}

?>