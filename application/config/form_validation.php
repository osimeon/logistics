<?php
$config = array(
                'create_user' => array(
                                    array(
                                            'field' => 'first_name',
                                            'label' => 'First Name',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[3]|max_length[50]'
                                         ),
                                    array(
                                            'field' => 'last_name',
                                            'label' => 'Last Name',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[3]|max_length[50]'
                                         ),
                                    array(
                                            'field' => 'email',
                                            'label' => 'E-mail',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|max_length[50]|valid_email'
                                         ),
                                    array(
                                            'field' => 'pass1',
                                            'label' => 'Password',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[2]|max_length[20]|matches[pass2]'
                                         ),
                                    array(
                                            'field' => 'pass2',
                                            'label' => 'Password again',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[2]|max_length[20]'
                                         )
                                    ),
				'update_user' => array(
                                    array(
                                            'field' => 'first_name',
                                            'label' => 'First Name',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[3]|max_length[50]'
                                         ),
                                    array(
                                            'field' => 'last_name',
                                            'label' => 'Last Name',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[3]|max_length[50]'
                                         ),     
                                    array(
                                            'field' => 'email',
                                            'label' => 'E-mail',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|max_length[50]|valid_email'
                                         ),
                                         array(
                                            'field' => 'pass1',
                                            'label' => 'Password',
                                            'rules' => 'trim|xss_clean|htmlspecialchars|min_length[2]|max_length[20]|matches[pass2]'
                                         ),
                                    array(
                                            'field' => 'pass2',
                                            'label' => 'Password again',
                                            'rules' => 'trim|xss_clean|htmlspecialchars|min_length[2]|max_length[20]'
                                         ),
									array(
											'field' => 'user_id',
                                            'label' => 'User ID',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|numeric|callback__check_user_id'
										)
                                    ),
                                    
                'login_user' => array(
                                    array(
                                            'field' => 'email',
                                            'label' => 'E-mail',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|max_length[50]|valid_email'
                                         ),
                                    array(
                                            'field' => 'password',
                                            'label' => 'Password',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[2]|max_length[20]'
                                         )
                                    ),                                    
				'lostpassword' => array(
                                    array(
                                            'field' => 'email',
                                            'label' => 'E-mail',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|valid_email'
                                         )
                                    ), 
				'change_password' => array(
									array(
                                            'field' => 'currentpass',
                                            'label' => 'Current password',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[2]|max_length[20]'
                                         ),
                                    array(
                                            'field' => 'pass1',
                                            'label' => 'New password',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[2]|max_length[20]|matches[pass2]'
                                         ),
									array(
                                            'field' => 'pass2',
                                            'label' => 'New password again',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[2]|max_length[20]'
                                         )
                                    ),
				'change_profile' => array(
									array(
                                            'field' => 'first_name',
                                            'label' => 'First Name',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[3]|max_length[50]'
                                         ),
                                    array(
                                            'field' => 'last_name',
                                            'label' => 'Last Name',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[3]|max_length[50]'
                                         ),
									array(
                                            'field' => 'email',
                                            'label' => 'E-mail',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|max_length[60]|valid_email'
                                         )
                                    ),
                'upload_settings' => array(
									array(
                                            'field' => 'allowed_extensions',
                                            'label' => 'Allowed Extensions',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|max_length[50]'
                                         ),
                                    array(
                                            'field' => 'max_upload_files',
                                            'label' => 'Maximum upload files',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|max_length[2]|numeric'
                                         ),
                                    array(
                                            'field' => 'max_upload_file_size',
                                            'label' => 'Maximum upload file size',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|max_length[5]|numeric'
                                         )
                                    ),                                        
                'general_settings' => array(
									array(
                                            'field' => 'site_name',
                                            'label' => 'Site name',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|max_length[255]'
                                         ),
                                    array(
                                            'field' => 'site_email',
                                            'label' => 'Site E-mail',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|max_length[60]|valid_email'
                                         )
                                    ),
                'add_pickup' => array(
                                      array(
                                            'field' => 'pickup_name',
                                            'label' => 'Location Name',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[3]|max_length[50]'
                                      ),
                                      array(
                                            'field' => 'pickup_address',
                                            'label' => 'Location Address',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[3]|max_length[255]'
                                      ),
                                      array(
                                            'field' => 'contact_person',
                                            'label' => 'Contact Person',
                                            'rules' => 'required'
                                      ),   
                                      array(
                                            'field' => 'opening_hour',
                                            'label' => 'Openning Hour',
                                            'rules' => 'required'
                                      ), 
                                      array(
                                            'field' => 'closing_hour',
                                            'label' => 'Closing Hour',
                                            'rules' => 'required'
                                      )  
                ),
                'add_package' => array(
                                  array(
                                    'field' => 'package_name',
                                    'label' => 'Package Name',
                                    'rules' => 'required|trim|xss_clean|htmlspecialchars|max_length[50]'
                                  ),
                                  array(
                                    'field' => 'package_desc',
                                    'label' => 'Pakcage Description',
                                    'rules' => 'trim|xss_clean|htmlspecialchars'
                                  )
                ),
                'new_resource' => array(
                                    array(
                                            'field' => 'resource_name',
                                            'label' => 'Resource Name',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars'
                                         ),
                                    array(
                                            'field' => 'resource_description',
                                            'label' => 'Resource Description',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars'
                                         ),
                                    array(
                                            'field' => 'resource_type',
                                            'label' => 'Resource Type',
                                            'rules' => 'required'
                                         )
                                    ),
                'add_staff' => array(
                                    array(
                                            'field' => 'first_name',
                                            'label' => 'First Name',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[3]|max_length[50]'
                                         ),
                                    array(
                                            'field' => 'last_name',
                                            'label' => 'Last Name',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[3]|max_length[50]'
                                         ),
                                    array(
                                            'field' => 'email',
                                            'label' => 'E-mail',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|max_length[50]|valid_email'
                                         ),
                                    array(
                                            'field' => 'pass1',
                                            'label' => 'Password',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[2]|max_length[20]|matches[pass2]'
                                         ),
                                    array(
                                            'field' => 'pass2',
                                            'label' => 'Password again',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[2]|max_length[20]'
                                         )
                                    ),
                    'update_staff' => array(
                                    array(
                                            'field' => 'first_name',
                                            'label' => 'First Name',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[3]|max_length[50]'
                                         ),
                                    array(
                                            'field' => 'last_name',
                                            'label' => 'Last Name',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[3]|max_length[50]'
                                         ),
                                    array(
                                            'field' => 'email',
                                            'label' => 'E-mail',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|max_length[50]|valid_email'
                                         ),
                                    array(
                                            'field' => 'pass1',
                                            'label' => 'Password',
                                            'rules' => 'trim|xss_clean|htmlspecialchars|min_length[6]|max_length[20]|matches[pass2]'
                                         ),
                                    array(
                                            'field' => 'pass2',
                                            'label' => 'Password again',
                                            'rules' => 'trim|xss_clean|htmlspecialchars|min_length[6]|max_length[20]'
                                         )
                                    ),
                    'create_contact_person' => array(
                                          array(
                                            'field' => 'contact_name',
                                            'label' => 'Contact Name',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[3]|max_length[50]'
                                          ),
                                          array(
                                            'field' => 'contact_email',
                                            'label' => 'Contact E-mail',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|max_length[50]|valid_email'
                                          ),
                                          array(
                                            'field' => 'contact_position',
                                            'label' => 'Contact Position',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|max_length[50]'
                                          ),
                                          array(
                                            'field' => 'pass1',
                                            'label' => 'Password',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[2]|max_length[20]|matches[pass2]'
                                         ),
                                          array(
                                            'field' => 'pass2',
                                            'label' => 'Password again',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[2]|max_length[20]'
                                         )
                    ),
                  /*Customers Validation */                  
                  	'create_customers' => array(
                                    array(
                                            'field' => 'company_name',
                                            'label' => 'Company Name',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[3]|max_length[50]'
                                         ),
                                    array(
                                            'field' => 'company_email',
                                            'label' => 'Company E-mail',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|max_length[50]|valid_email'
                                         ),
                                    array(
                                            'field' => 'company_phone',
                                            'label' => 'Company Phone',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars'
                                         ),
                                    array(
                                            'field' => 'pass1',
                                            'label' => 'Password',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[2]|max_length[20]|matches[pass2]'
                                         ),
                                    array(
                                            'field' => 'pass2',
                                            'label' => 'Password again',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[2]|max_length[20]'
                                         )
                                    ),
                    'login_customer' => array(
                                    array(
                                            'field' => 'email',
                                            'label' => 'E-mail',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|max_length[50]|valid_email'
                                         ),
                                    array(
                                            'field' => 'password',
                                            'label' => 'Password',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[2]|max_length[20]'
                                         )
                                    ),
                  'customer_lostpassword' => array(
                                    array(
                                            'field' => 'email',
                                            'label' => 'E-mail',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|valid_email'
                                         )
                                    ),
                 'customer_change_profile' => array(
									array(
                                            'field' => 'first_name',
                                            'label' => 'First Name',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[3]|max_length[50]'
                                         ),
                                    array(
                                            'field' => 'last_name',
                                            'label' => 'Last Name',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[3]|max_length[50]'
                                         ),
									array(
                                            'field' => 'email',
                                            'label' => 'E-mail',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|max_length[20]|valid_email'
                                         )
                                    ),
          'customer_change_password' => array(
										array(
	                                            'field' => 'currentpass',
	                                            'label' => 'Current password',
	                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[2]|max_length[20]'
	                                         ),
	                                    array(
	                                            'field' => 'pass1',
	                                            'label' => 'New password',
	                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[2]|max_length[20]|matches[pass2]'
	                                         ),
										array(
	                                            'field' => 'pass2',
	                                            'label' => 'Repeat password again',
	                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[2]|max_length[20]'
	                                         )
                                         ),
         		'admin_create_customers' => array(
                                    array(
                                            'field' => 'first_name',
                                            'label' => 'First Name',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[3]|max_length[50]'
                                         ),
                                    array(
                                            'field' => 'last_name',
                                            'label' => 'Last Name',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[3]|max_length[50]'
                                         ),
                                    array(
                                            'field' => 'email',
                                            'label' => 'Email',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|max_length[50]|valid_email'
                                         ),
                                    array(
                                            'field' => 'password',
                                            'label' => 'Password',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[2]|max_length[20]'
                                         ) 
                                    ),
                     'admin_update_customers' => array(
                                    array(
                                            'field' => 'first_name',
                                            'label' => 'First Name',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[3]|max_length[50]'
                                         ),
                                    array(
                                            'field' => 'last_name',
                                            'label' => 'Last Name',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[3]|max_length[50]'
                                         ),
                                    array(
                                            'field' => 'email',
                                            'label' => 'Email',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|max_length[50]|valid_email'
                                         )  
                                    ),                                                    
				'add_customer_main_order' => array(
                                    array(
                                            'field' => 'airway_bill_no',
                                            'label' => 'Airway Bill No',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[3]|max_length[50]'
                                         ),
                                    array(
                                            'field' => 'comments',
                                            'label' => 'Comments or Notes',
                                            'rules' => 'trim|xss_clean|htmlspecialchars'
                                         ) 
                                    ),
        'admin_create_company' => array(
                                    array(
                                            'field' => 'name',
                                            'label' => 'Company Name',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[3]|max_length[50]'
                                         ),
                                    array(
                                            'field' => 'email',
                                            'label' => 'Email',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|max_length[50]|valid_email'
                                         ) 
                                    ),
        
        'add_customer_order' => array(
                                  array(
                                            'field' => 'airway_bill_no_order_form',
                                            'label' => 'Airway Bill No',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[3]|max_length[50]'
                                  ),
                                  array(
                                            'field' => 'stop_contact_person',
                                            'label' => 'Contact Person',
                                            'rules' => 'required'
                                  ),
                                  array(
                                            'field' => 'pickup_location',
                                            'label' => 'Pickup Location',
                                            'rules' => 'required'
                                  ),
                                  array(
                                            'field' => 'pickup_date',
                                            'label' => 'Pickup Date',
                                            'rules' => 'required'
                                  ),
                                  array(
                                            'field' => 'delivery_location',
                                            'label' => 'Delivery Location',
                                            'rules' => 'required'
                                  ),
                                  array(
                                            'field' => 'delivery_date',
                                            'label' => 'Delivery Date',
                                            'rules' => 'required'
                                  ),
                                  array(
                                            'field' => 'item_quantity',
                                            'label' => 'Item Quantity',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[1]|max_length[50]'
                                  ),
                                  array(
                                            'field' => 'total_weight',
                                            'label' => 'Total Weight',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[1]|max_length[50]'
                                  ),
                                  array(
                                            'field' => 'measure',
                                            'label' => 'Measurement Unit',
                                            'rules' => 'required'
                                  ),
                                  array(
                                            'field' => 'packaging_type',
                                            'label' => 'Packaging Type',
                                            'rules' => 'required'
                                  ),
                                  array(
                                            'field' => 'pickup_time',
                                            'label' => 'Pickup Time',
                                            'rules' => 'required'
                                  ),
                                  array(
                                            'field' => 'delivery_time',
                                            'label' => 'Delivery Time',
                                            'rules' => 'required'
                                  )
        ),
                'admin_update_company' => array(
                                    array(
                                            'field' => 'name',
                                            'label' => 'Company Name',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|min_length[3]|max_length[50]'
                                         ),
                                    array(
                                            'field' => 'email',
                                            'label' => 'Email',
                                            'rules' => 'required|trim|xss_clean|htmlspecialchars|max_length[50]|valid_email'
                                         ) 
                                    ),                    					
               );
?>